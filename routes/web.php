<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes([
    'login' => true,
    'register' => true,
    'reset' => false,
    ]);

    Route::middleware(['XSS'])->group(function () {

    //Installation Part

    Route::get('/install','Installer\InstallerController@install')->name('install')->middleware('installer');

    Route::get('/install/info','Installer\InstallerController@info')->name('install.info');
    Route::get('/install/check/{param}','Installer\InstallerController@check')->name('install.check');
    Route::get('pre/install','Installer\InstallerController@preinstall')->name('preinstall');
    Route::post('/install/store','Installer\InstallerController@send');

    //Frontend Part

    Route::get('/', 'SiteController@index')->name('index');
    Route::get('/hearourpoint', 'SiteController@hearourpoint')->name('hearourpoint');
    Route::get('/acc', 'SiteController@acc')->name('acc');
    Route::get('/partner', 'SiteController@partner')->name('partner');
    Route::get('/invest', 'SiteController@invest')->name('invest');
    Route::get('/apply', 'SiteController@apply')->name('apply');
    Route::get('/about', 'SiteController@about')->name('about');
    Route::get('/support', 'SiteController@support')->name('team');
    Route::get('/testimonial', 'SiteController@testimonial')->name('testimonial');
    Route::get('/blog-list', 'SiteController@bloglist')->name('blog-list');
    Route::get('/blog-grid', 'SiteController@bloggrid')->name('blog-grid');
    Route::get('/blogdetails/{id}/{slug}', 'SiteController@blogdetails');
    Route::get('/blog-search','SiteController@blogsearchpage')->name('blog.search');
    Route::get('/contact', 'SiteController@contact')->name('contact');
    Route::post('/frcontact', 'SiteController@frcontactstore')->name('frcontact.store');
    Route::post('/frpartner', 'SiteController@frpartnerstore')->name('frpartner.store');
    Route::post('/frinvest', 'SiteController@frinveststore')->name('frinvest.store');
    Route::post('/fracc', 'SiteController@fraccstore')->name('fracc.store');
    Route::post('/frsubscribe', 'SiteController@frsubscribestore')->name('frsubscribe.store');
});


Route::group(['prefix'=>'admin','middleware'=>['auth','XSS']],function(){

    //Language
    Route::get('lang/{lang}', 'Admin\LanguageController@switchLang')->name('lang.switch');
    // Dashboard
    Route::resource('dashboard', 'Admin\DashboardController');

    // Profile
    Route::resource('profile', 'Admin\ProfileController');
    Route::get('profile/destroy/{id}','Admin\ProfileController@destroy')->name('profile.destroy');

    // General
    Route::resource('general', 'Admin\GeneralController');
    Route::post('general/destroy','Admin\GeneralController@destroy')->name('general.destroy');
    Route::get('general/destroy/{id}','Admin\GeneralController@destroy')->name('general.destroy');

    // Mainslider
    Route::resource('mainslider', 'Admin\MainSliderController');
    Route::get('mainslider/destroy/{id}','Admin\MainSliderController@destroy')->name('mainslider.destroy');

    // About
    Route::resource('about', 'Admin\AboutController');
    Route::get('about/destroy/{id}','Admin\AboutController@destroy')->name('about.destroy');

    // Counter
    Route::resource('counter', 'Admin\CounterController');
    Route::get('counter/destroy/{id}','Admin\CounterController@destroy')->name('counter.destroy');

    // Service
    Route::resource('service', 'Admin\ServiceController');
    Route::get('service/destroy/{id}','Admin\ServiceController@destroy')->name('service.destroy');

    // Service content
    Route::resource('servicecontent', 'Admin\ServiceContentController');
    Route::get('servicecontent/destroy/{id}','Admin\ServiceContentController@destroy')->name('servicecontent.destroy');

    // Team
    Route::resource('team', 'Admin\TeamController');
    Route::get('team/destroy/{id}','Admin\TeamController@destroy')->name('team.destroy');

    // Team content
    Route::resource('teamcontent', 'Admin\TeamContentController');
    Route::get('teamcontent/destroy/{id}','Admin\TeamContentController@destroy')->name('teamcontent.destroy');

    // Cta
    Route::resource('cta', 'Admin\CtaController');
    Route::get('cta/destroy/{id}','Admin\CtaController@destroy')->name('cta.destroy');

    // Testimonial
    Route::resource('testimonial', 'Admin\TestimonialController');
    Route::get('testimonial/destroy/{id}','Admin\TestimonialController@destroy')->name('testimonial.destroy');

    // Testimonial content
    Route::resource('testimonialcontent', 'Admin\TestimonialContentController');
    Route::get('testimonialcontent/destroy/{id}','Admin\TestimonialContentController@destroy')->name('testimonialcontent.destroy');

    // Video
    Route::resource('video', 'Admin\VideoController');
    Route::get('video/destroy/{id}','Admin\VideoController@destroy')->name('video.destroy');

    // Video content
    Route::resource('videocontent', 'Admin\VideoContentController');
    Route::get('videocontent/destroy/{id}','Admin\VideoContentController@destroy')->name('videocontent.destroy');

    // Blog
    Route::resource('blog', 'Admin\BlogController');
    Route::get('blog/destroy/{id}','Admin\BlogController@destroy')->name('blog.destroy');

    // Blog content
    Route::resource('blogcontent', 'Admin\BlogContentController');
    Route::get('blogcontent/destroy/{id}','Admin\BlogContentController@destroy')->name('blogcontent.destroy');

    // Subscribe
    Route::resource('subscribe', 'Admin\SubscribeController');
    Route::get('subscribe/destroy/{id}','Admin\SubscribeController@destroy')->name('subscribe.destroy');

    // Subscribe Content
    Route::resource('subscribecontent', 'Admin\SubscribeContentController');
    Route::get('subscribecontent/destroy/{id}','Admin\SubscribeContentController@destroy')->name('subscribecontent.destroy');

    // Ecosystempartner
    Route::resource('client', 'Admin\ClientController');
    Route::get('client/destroy/{id}','Admin\ClientController@destroy')->name('client.destroy');

    // Widget1
    Route::resource('widget1', 'Admin\Widget1Controller');
    Route::get('widget1/destroy/{id}','Admin\Widget1Controller@destroy')->name('widget1.destroy');

    // Widget2
    Route::resource('widget2', 'Admin\Widget2Controller');
    Route::get('widget2/destroy/{id}','Admin\Widget2Controller@destroy')->name('widget2.destroy');

    // Widget3
    Route::resource('widget3', 'Admin\Widget3Controller');
    Route::get('widget3/destroy/{id}','Admin\Widget3Controller@destroy')->name('widget3.destroy');

    // Contact
    Route::resource('contact', 'Admin\ContactController');
    Route::get('contact/destroy/{id}','Admin\ContactController@destroy')->name('contact.destroy');

    // Contact Info
    Route::resource('contactinfo', 'Admin\ContactinfoController');
    Route::get('contactinfo/destroy/{id}','Admin\ContactinfoController@destroy')->name('contactinfo.destroy');

    // optimizer
    Route::get('optimizer', function() {
        Artisan::call('cache:clear');
        Artisan::call('route:clear');
        Artisan::call('config:clear');
        Artisan::call('view:clear');
        $notification = array(
            'message' => __('messages.Your Site Optimize Successfully'),
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    });

});
