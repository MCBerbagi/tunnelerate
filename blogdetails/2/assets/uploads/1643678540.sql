-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 31, 2022 at 03:09 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tunnel`
--

-- --------------------------------------------------------

--
-- Table structure for table `abouts`
--

CREATE TABLE `abouts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dab_sec_title` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `dab_sec_title_2` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dab_srt_paragraph` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dab_long_paragraph` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dab_list_info` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dab_btn` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dab_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dab_big_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dab_brand_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `abouts`
--

INSERT INTO `abouts` (`id`, `created_at`, `updated_at`, `dab_sec_title`, `dab_sec_title_2`, `dab_srt_paragraph`, `dab_long_paragraph`, `dab_list_info`, `dab_btn`, `dab_url`, `dab_big_image`, `dab_brand_image`) VALUES
(1, '2022-01-25 19:36:38', '2022-01-27 16:53:33', 'How we work', 'about <span>Tunnelerate </span> work', 'As founders, we know how hard it is to build a company from scratch. We wanted to help you realized and grow your business from ground-up during in our 10 weeks long program without any program fees', '', '', 'about us', '#', 'assets/img/about/16433276131.png', 'assets/img/about/16431680882.png');

-- --------------------------------------------------------

--
-- Table structure for table `acclerators`
--

CREATE TABLE `acclerators` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dab_sec_title` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `dab_sec_title_2` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dab_srt_paragraph` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dab_long_paragraph` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dab_list_info` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dab_btn` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dab_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dab_big_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dab_brand_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dab_big_imagee` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dab_big_imageee` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `acclerators`
--

INSERT INTO `acclerators` (`id`, `created_at`, `updated_at`, `dab_sec_title`, `dab_sec_title_2`, `dab_srt_paragraph`, `dab_long_paragraph`, `dab_list_info`, `dab_btn`, `dab_url`, `dab_big_image`, `dab_brand_image`, `dab_big_imagee`, `dab_big_imageee`) VALUES
(1, '2022-01-25 19:36:38', '2022-01-25 21:32:10', 'Tunnelerate', '<span>Tunnelerate </span> work', 'Invest early on revenue generating industries such as edutech, agritech, ecommerce, gaming and many more. Be a part of early stage startups and see their journey grow. Invest small but see a big impact. Fund promising startups during demo days or even pre program.\n\nThe Tunnelerate Accelerator Program invites industry leaders and promising startups to collaborate and share experiences. Use our sources to gain insights into emerging markets. Join our closed networking groups and build a strong relationships with our community of founders.\n\nMentors are a crucial part of our ecosystem. Share your experiences, advice, guidance and wisdom to founders, they shape the company and create lasting impact and relationships.\n\nTake this opportunity to partner with us. At Tunnelerate our vision is to help local founders to be leaders in their industry. With your help and our experienced founders we aim to give our local founders a platform to grow and succeed with their companies.', '', '', 'about us', '#', 'assets/img/about/4.jpg', 'assets/img/about/16431680882.png', 'assets/img/about/5.png', 'assets/img/about/6.png');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dblog_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dblog_tags` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dblog_paragraph` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `dblog_long_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `dblog_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `created_at`, `updated_at`, `dblog_title`, `dblog_tags`, `dblog_paragraph`, `dblog_long_content`, `dblog_image`) VALUES
(1, '2022-01-25 19:36:38', '2022-01-25 23:17:20', 'Daily Social - Tunnelerate menargetkan pendanaan untuk startup tahap awal Indonesia', 'venture capital', 'Co-Founder dan eks-CEO TaniHub Ivan Arie Sustiawan Ingin Bangkitkan \"Founder\" Startup Lokal Melalui Perusahaan Modal Ventura Tunnelerate', '<p><br></p><blockquote class=\"block-quote\"><p><span style=\"font-family: Helvetica;\">Someday I would like to give back to community</span></p><span style=\"font-family: Helvetica;\">Ivan Arie Sustiawan</span></blockquote><p><span style=\"font-family: Helvetica;\">Bicara langkah besarnya setelah mundur dari posisi CEO Tanihub dan mendirikan venture capitalnya sendiri</span></p>', 'assets/img/blog/16431762752.png'),
(2, '2022-01-25 19:36:38', '2022-01-25 23:22:09', 'Tech In Asia - Modal Ventura Milik Co-founder Tanihub Cari Startup dari Kota Kecil', 'Tunnelerate,modal venture', 'Mantan CEO Tanihub, Ivan Arie Sustiawan, mendirikan perusahaan modal ventura yang akan fokus kepada startup tahap awal (seed stage).', '<p style=\"box-sizing: border-box; line-height: 1.8; margin-bottom: 1.8em; color: rgb(23, 23, 23); font-family: Georgia, serif; font-size: 21px;\"><span style=\"font-family: Helvetica;\">Mantan CEO Tanihub, Ivan Arie Sustiawan, mendirikan perusahaan modal ventura yang akan fokus kepada&nbsp;</span><em style=\"box-sizing: border-box;\"><span style=\"font-family: Helvetica;\">startup</span><span style=\"font-family: Helvetica;\">&nbsp;</span></em><span style=\"font-family: Helvetica;\">tahap awal (</span><em style=\"box-sizing: border-box;\"><span style=\"font-family: Helvetica;\">seed stage</span></em><span style=\"font-family: Helvetica;\">). Mengusung nama Tunnelerate, Ivan mendirikan modal ventura ini bersama tiga rekannya yaitu, Bharat Ongso, Riswanto, dan Ayunda Afifa.</span></p><p style=\"box-sizing: border-box; line-height: 1.8; margin-bottom: 1.8em; color: rgb(23, 23, 23); font-family: Georgia, serif; font-size: 21px;\"><span style=\"font-family: Helvetica;\">Kepada</span><span style=\"font-family: Helvetica;\">&nbsp;</span><em style=\"box-sizing: border-box;\"><a target=\"_blank\" href=\"https://www.techinasia.com/extanihub-ceo-launches-seed-fund-accelerator-indonesian-founders\" style=\"box-sizing: border-box; color: inherit; border-bottom: 1px solid rgb(166, 166, 166);\"><span style=\"font-family: Helvetica;\">Tech in Asia</span></a></em><span style=\"font-family: Helvetica;\">, Ivan menjelaskan bahwa Tunnelerate akan fokus di sektor</span><span style=\"font-family: Helvetica;\">&nbsp;</span><em style=\"box-sizing: border-box;\"><span style=\"font-family: Helvetica;\">fintech, agritech</span></em><span style=\"font-family: Helvetica;\">, dan</span><span style=\"font-family: Helvetica;\">&nbsp;</span><em style=\"box-sizing: border-box;\"><span style=\"font-family: Helvetica;\">healthtech</span></em><span style=\"font-family: Helvetica;\">. Tunnelerate juga berencana mendukung para</span><span style=\"font-family: Helvetica;\">&nbsp;</span><em style=\"box-sizing: border-box;\"><span style=\"font-family: Helvetica;\">founder</span></em><span style=\"font-family: Helvetica;\">&nbsp;</span><span style=\"font-family: Helvetica;\">yang berada di luar kota besar di Indonesia.</span></p><p style=\"box-sizing: border-box; line-height: 1.8; margin-bottom: 1.8em; color: rgb(23, 23, 23); font-family: Georgia, serif; font-size: 21px;\"><span style=\"font-family: Helvetica;\">“Kami ingin para</span><span style=\"font-family: Helvetica;\">&nbsp;</span><em style=\"box-sizing: border-box;\"><span style=\"font-family: Helvetica;\">founder</span></em><span style=\"font-family: Helvetica;\">&nbsp;</span><span style=\"font-family: Helvetica;\">dari daerah-daerah di Indonesia memiliki kesempatan yang lebih baik untuk mewujudkan impian mereka di negara ini,” kata Sustiawan.</span></p><p style=\"box-sizing: border-box; line-height: 1.8; margin-bottom: 1.8em; color: rgb(23, 23, 23); font-family: Georgia, serif; font-size: 21px;\"><span style=\"font-family: Helvetica;\">Selain berinvestasi, perusahaan modal ventura ini juga menyelenggarakan program akselerator. Tunnelerate bakal memilih 15</span><span style=\"font-family: Helvetica;\">&nbsp;</span><em style=\"box-sizing: border-box;\"><span style=\"font-family: Helvetica;\">startup</span></em><span style=\"font-family: Helvetica;\">&nbsp;</span><span style=\"font-family: Helvetica;\">untuk berpartisipasi dalam dua</span><span style=\"font-family: Helvetica;\">&nbsp;</span><em style=\"box-sizing: border-box;\"><span style=\"font-family: Helvetica;\">cohort</span></em><span style=\"font-family: Helvetica;\">&nbsp;</span><span style=\"font-family: Helvetica;\">yang rencananya diselenggarakan pada tahun 2022.</span></p><p style=\"box-sizing: border-box; color: rgb(201, 201, 201); background-color: rgb(250, 250, 250); margin-top: 1em; margin-bottom: 2em; text-align: center; font-size: 12px; line-height: 1.8em; padding-top: 5px; padding-bottom: 5px; font-family: sans-serif; position: relative; height: auto !important;\"><span style=\"font-family: Helvetica;\">Iklan. Hapus iklan dengan</span><span style=\"font-family: Helvetica;\">&nbsp;</span><a class=\"banner-ad-top-link\" href=\"https://id.techinasia.com/idplus?ref=banner-ad-top\" style=\"box-sizing: border-box; color: inherit; text-decoration-line: underline; border-bottom: 1px solid rgb(166, 166, 166); border-top: 0px !important; border-right: 0px !important; border-left: 0px !important; border-image: initial !important;\"><span style=\"font-family: Helvetica;\">berlangganan</span></a><span style=\"font-family: Helvetica;\">.</span><p><a href=\"https://id.techinasia.com/idplus?ref=banner-ad-bottom\" style=\"box-sizing: border-box; color: inherit; border-bottom: 1px solid rgb(166, 166, 166); border-top: 0px !important; border-right: 0px !important; border-left: 0px !important; border-image: initial !important;\"><u style=\"box-sizing: border-box;\"><span style=\"font-family: Helvetica;\">Dukung</span></u><span style=\"font-family: Helvetica;\">&nbsp;</span><span style=\"font-family: Helvetica;\">jurnalisme independen &amp; berkualitas.</span></a><div class=\"artad\" style=\"box-sizing: border-box; color: rgb(201, 201, 201); background-color: rgb(250, 250, 250); margin-top: 1em; margin-bottom: 2em; text-align: center; font-size: 12px; line-height: 1.8em; padding-top: 5px; padding-bottom: 5px; font-family: sans-serif; position: relative; height: auto !important;\"></div></p></p><div class=\"artad\" style=\"box-sizing: border-box; color: rgb(201, 201, 201); background-color: rgb(250, 250, 250); margin-top: 1em; margin-bottom: 2em; text-align: center; font-size: 12px; line-height: 1.8em; padding-top: 5px; padding-bottom: 5px; font-family: sans-serif; position: relative; height: auto !important;\"></div><p style=\"box-sizing: border-box; line-height: 1.8; margin-bottom: 1.8em; color: rgb(23, 23, 23); font-family: Georgia, serif; font-size: 21px; height: auto !important;\"><span style=\"font-family: Helvetica;\">Seluruh peserta yang terpilih akan mendapatkan akses ke para mentor. Di akhir program, nantinya ada sekitar 5</span><span style=\"font-family: Helvetica;\">&nbsp;</span><em style=\"box-sizing: border-box;\"><span style=\"font-family: Helvetica;\">startup</span><span style=\"font-family: Helvetica;\">&nbsp;</span></em><span style=\"font-family: Helvetica;\">yang memperoleh investasi dari dana kelolaan Tunnelerate.</span></p><p style=\"box-sizing: border-box; line-height: 1.8; margin-bottom: 1.8em; color: rgb(23, 23, 23); font-family: Georgia, serif; font-size: 21px;\"><span style=\"font-family: Helvetica;\">Untuk dana kelolaan pertamanya, Tunnelerate berencana menghimpun US$10 juta (sekitar Rp143 miliar). Ivan menuturkan dana tersebut berasal dari</span><span style=\"font-family: Helvetica;\">&nbsp;</span><em style=\"box-sizing: border-box;\"><span style=\"font-family: Helvetica;\">angel investor</span></em><span style=\"font-family: Helvetica;\">&nbsp;</span><span style=\"font-family: Helvetica;\">yang berlatarbelakang pengusaha veteran lokal dengan pengalaman bisnis lebih dari 20 tahun.</span></p><p style=\"box-sizing: border-box; line-height: 1.8; margin-bottom: 1.8em; color: rgb(23, 23, 23); font-family: Georgia, serif; font-size: 21px;\"><span style=\"font-family: Helvetica;\">Dijelaskan, para</span><span style=\"font-family: Helvetica;\">&nbsp;</span><em style=\"box-sizing: border-box;\"><span style=\"font-family: Helvetica;\">angel investor</span><span style=\"font-family: Helvetica;\">&nbsp;</span></em><span style=\"font-family: Helvetica;\">akan menginvestasikan dana di kisaran minimal US$100 ribu (sekitar Rp1,4 miliar). Mereka pun bakal berinteraksi langsung dengan</span><span style=\"font-family: Helvetica;\">&nbsp;</span><em style=\"box-sizing: border-box;\"><span style=\"font-family: Helvetica;\">founder startup</span></em><span style=\"font-family: Helvetica;\">.</span></p>', 'assets/img/blog/16431780342.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `blog_contents`
--

CREATE TABLE `blog_contents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dtmcon_sec_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dtmcon_sec_title_2` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog_contents`
--

INSERT INTO `blog_contents` (`id`, `created_at`, `updated_at`, `dtmcon_sec_title`, `dtmcon_sec_title_2`) VALUES
(1, '2022-01-25 19:36:38', '2022-01-25 19:36:38', 'recent news', 'news & articles');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dcli_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dcli_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `created_at`, `updated_at`, `dcli_link`, `dcli_image`) VALUES
(1, '2022-01-25 19:36:38', '2022-01-25 21:54:41', 'Gojek.com', 'assets/img/client/16431728812.png'),
(2, '2022-01-25 19:36:38', '2022-01-26 00:37:12', 'carrefour.com', 'assets/img/client/16431826322.png'),
(3, '2022-01-25 19:36:38', '2022-01-25 21:55:29', 'eateroo.com', 'assets/img/client/16431729292.png'),
(4, '2022-01-25 19:36:38', '2022-01-25 21:55:48', 'efishery.com', 'assets/img/client/16431729482.png');

-- --------------------------------------------------------

--
-- Table structure for table `contactinfos`
--

CREATE TABLE `contactinfos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dci_icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dci_info` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contactinfos`
--

INSERT INTO `contactinfos` (`id`, `created_at`, `updated_at`, `dci_icon`, `dci_info`) VALUES
(1, '2022-01-25 19:36:38', '2022-01-26 17:25:36', 'assets/img/contact/1643243136.png', '<li>021-5098 2659</li>'),
(2, '2022-01-25 19:36:38', '2022-01-27 15:36:45', 'assets/img/contact/1643323005.png', 'South Quarter Building Complex\r\nJalan RA Kartini Kav 8, Cilandak, Jakarta Selatan'),
(3, '2022-01-25 19:36:38', '2022-01-25 19:36:38', 'assets/img/contact/3.png', '<li><a href=\"mailto:merox@web.com\">merox@web.com</a></li><li><a href=\"mailto:sales@website.com\">sales@website.com</a></li>');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dco_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dco_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dco_subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dco_message` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `created_at`, `updated_at`, `dco_name`, `dco_email`, `dco_subject`, `dco_message`) VALUES
(1, '2022-01-25 19:36:38', '2022-01-25 19:36:38', 'abmack man', 'aaaaaaaaa@gmail.com', 'How Can I Buy', 'There are many variations of but the majority have suffered inject’s'),
(2, '2022-01-25 19:36:38', '2022-01-25 19:36:38', 'master', 'master@gmail.com', 'What Is This', 'There are many variations of but the majority have suffered inject’s'),
(3, '2022-01-25 19:36:38', '2022-01-25 19:36:38', 'uuuuuuu mmmmmmm', 'umumum@gmail.com', 'How Can I Buy', 'There are many variations of but the majority have suffered inject’s'),
(4, '2022-01-26 18:40:36', '2022-01-26 18:40:36', 'aaa', 'mahar.catur@gmail.com', 'bismillah', 'bismillah');

-- --------------------------------------------------------

--
-- Table structure for table `counters`
--

CREATE TABLE `counters` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dc_icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dc_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dc_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `counters`
--

INSERT INTO `counters` (`id`, `created_at`, `updated_at`, `dc_icon`, `dc_number`, `dc_title`) VALUES
(1, '2022-01-25 19:36:38', '2022-01-25 19:36:38', 'assets/img/counter/1.svg', '653', 'Happy Clients'),
(2, '2022-01-25 19:36:38', '2022-01-25 19:36:38', 'assets/img/counter/2.svg', '465', 'Trusted Users'),
(3, '2022-01-25 19:36:38', '2022-01-25 19:36:38', 'assets/img/counter/3.svg', '784', 'Projects Done'),
(4, '2022-01-25 19:36:38', '2022-01-25 19:36:38', 'assets/img/counter/4.svg', '123', 'Coffe Cup');

-- --------------------------------------------------------

--
-- Table structure for table `ctas`
--

CREATE TABLE `ctas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dcta_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dcta_title_2` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dcta_btn` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dcta_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dcta_big_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ctas`
--

INSERT INTO `ctas` (`id`, `created_at`, `updated_at`, `dcta_title`, `dcta_title_2`, `dcta_btn`, `dcta_url`, `dcta_big_image`) VALUES
(1, '2022-01-25 19:36:38', '2022-01-25 21:49:51', 'Join our first cohort', 'Calling all courageous founders', 'Apply Here', '#', 'assets/img/cta/16431725911.png');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `generals`
--

CREATE TABLE `generals` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dtop_logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dfavicon_icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dsite_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dbreadcrumb_bg` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dmap_api_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dfooter_copyright` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `generals`
--

INSERT INTO `generals` (`id`, `created_at`, `updated_at`, `dtop_logo`, `dfavicon_icon`, `dsite_title`, `dbreadcrumb_bg`, `dmap_api_key`, `dfooter_copyright`) VALUES
(1, '2022-01-25 19:36:38', '2022-01-26 03:24:11', 'assets/img/1643192651.png', 'assets/img/16431926412.png', 'Accelerating Your Entrepreneurial Journey', 'assets/img/16431649383.png', '', '<p>© All Right Reserved by <a href=\"#\">Tunnelerate 2022</a></p>');

-- --------------------------------------------------------

--
-- Table structure for table `invests`
--

CREATE TABLE `invests` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dab_sec_title` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `dab_sec_title_2` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dab_srt_paragraph` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dab_long_paragraph` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dab_list_info` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dab_btn` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dab_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dab_big_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dab_brand_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invests`
--

INSERT INTO `invests` (`id`, `created_at`, `updated_at`, `dab_sec_title`, `dab_sec_title_2`, `dab_srt_paragraph`, `dab_long_paragraph`, `dab_list_info`, `dab_btn`, `dab_url`, `dab_big_image`, `dab_brand_image`) VALUES
(1, '2022-01-25 19:36:38', '2022-01-25 21:32:10', 'More than 17,500 Islands, 700 languages, 300 ethics groups to be tapped', NULL, '', 'We personally see what Indonesia could offer to the world and with this enormous potential, we want to deal with it one step at a time. But we could not do it alone, none of us can. At Tunnelerate, we have our own version of success and that is building an inclusive Indonesian startup landscape, together. Therefore, we are looking to partner with crazy ideas that may have a significant impact on society from day one.', 'We are excited to announce that we are still opening up for anyone that wants to take the step with us in order to bring inclusivity to Indonesia startup landscape and inviting individuals, institutions, and organizations to invest with us. As our fund is focused on investing in well-diversified Indonesian companies from the very beginning in order to bring immediate, sustainable, and long-lasting impact to the society.', 'about us', '#', 'assets/img/about/1.jpg', 'assets/img/16431680882.png');

-- --------------------------------------------------------

--
-- Table structure for table `main_sliders`
--

CREATE TABLE `main_sliders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dms_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dms_title2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dms_paragraph` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dms_btn1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dms_url1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dms_btn2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dms_url2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dms_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `main_sliders`
--

INSERT INTO `main_sliders` (`id`, `created_at`, `updated_at`, `dms_title`, `dms_title2`, `dms_paragraph`, `dms_btn1`, `dms_url1`, `dms_btn2`, `dms_url2`, `dms_image`) VALUES
(1, '2022-01-25 19:36:38', '2022-01-28 01:24:53', 'Discover where we can help', 'Join our program <br> with <b> Tunnelerate </b>', 'Accelerating Your Entrepreneurial Journey', 'Join our program', '#', '', '#', 'assets/img/slider/1643165564.png');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_02_11_141754_create_generals_table', 1),
(5, '2021_03_31_113145_create_contactinfos_table', 1),
(6, '2021_04_01_064402_create_contacts_table', 1),
(7, '2021_04_24_105731_create_profiles_table', 1),
(8, '2021_04_24_110948_create_main_sliders_table', 1),
(9, '2021_04_24_141635_create_abouts_table', 1),
(10, '2021_04_24_153211_create_counters_table', 1),
(11, '2021_04_24_161308_create_services_table', 1),
(12, '2021_04_24_163340_create_service_contents_table', 1),
(13, '2021_04_25_093929_create_teams_table', 1),
(14, '2021_04_25_102842_create_team_contents_table', 1),
(15, '2021_04_25_104535_create_ctas_table', 1),
(16, '2021_04_25_132725_create_testimonials_table', 1),
(17, '2021_04_25_140730_create_testimonial_contents_table', 1),
(18, '2021_04_26_082821_create_videos_table', 1),
(19, '2021_04_26_083746_create_video_contents_table', 1),
(20, '2021_04_26_085942_create_blogs_table', 1),
(21, '2021_04_26_090758_create_blog_contents_table', 1),
(22, '2021_04_26_091814_create_clients_table', 1),
(23, '2021_04_26_093450_create_widget1s_table', 1),
(24, '2021_04_26_094445_create_widget2s_table', 1),
(25, '2021_04_26_112329_create_widget3s_table', 1),
(26, '2021_04_26_140631_create_subscribes_table', 1),
(27, '2021_04_26_150028_create_subscribe_contents_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

CREATE TABLE `partners` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dab_sec_title` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `dab_sec_title_2` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dab_srt_paragraph` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dab_long_paragraph` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dab_list_info` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dab_btn` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dab_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dab_big_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dab_brand_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dab_big_imagee` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `partners`
--

INSERT INTO `partners` (`id`, `created_at`, `updated_at`, `dab_sec_title`, `dab_sec_title_2`, `dab_srt_paragraph`, `dab_long_paragraph`, `dab_list_info`, `dab_btn`, `dab_url`, `dab_big_image`, `dab_brand_image`, `dab_big_imagee`) VALUES
(1, '2022-01-25 19:36:38', '2022-01-25 21:32:10', 'Tunnelerate', '<span>Tunnelerate </span> work', 'Invest early on revenue generating industries such as edutech, agritech, ecommerce, gaming and many more. Be a part of early stage startups and see their journey grow. Invest small but see a big impact. Fund promising startups during demo days or even pre program.\n\nThe Tunnelerate Accelerator Program invites industry leaders and promising startups to collaborate and share experiences. Use our sources to gain insights into emerging markets. Join our closed networking groups and build a strong relationships with our community of founders.\n\nMentors are a crucial part of our ecosystem. Share your experiences, advice, guidance and wisdom to founders, they shape the company and create lasting impact and relationships.\n\nTake this opportunity to partner with us. At Tunnelerate our vision is to help local founders to be leaders in their industry. With your help and our experienced founders we aim to give our local founders a platform to grow and succeed with their companies.', '', '', 'about us', '#', 'assets/img/about/2.jpg', 'assets/img/about/16431680882.png', 'assets/img/about/3.png');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dpr_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dpr_mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dpr_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dpr_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `dpr_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dpr_social1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dpr_social2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `register_investments`
--

CREATE TABLE `register_investments` (
  `id_register_investement` int(11) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `company` varchar(50) DEFAULT NULL,
  `company_desc` varchar(100) DEFAULT NULL,
  `linkedin_url` varchar(50) DEFAULT NULL,
  `investor_type` varchar(50) DEFAULT NULL,
  `have_you_invested_in_venture` varchar(50) DEFAULT NULL,
  `select_what_interest_you` varchar(50) DEFAULT NULL,
  `target_investement_amount` varchar(50) DEFAULT NULL,
  `how_did_you_hear_about_us` varchar(50) DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `created_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `register_investments`
--

INSERT INTO `register_investments` (`id_register_investement`, `first_name`, `email`, `title`, `company`, `company_desc`, `linkedin_url`, `investor_type`, `have_you_invested_in_venture`, `select_what_interest_you`, `target_investement_amount`, `how_did_you_hear_about_us`, `updated_at`, `created_at`) VALUES
(1, 'cobain', NULL, 'coba', 'uhuy', 'uhuy', 'uhuy', 'Institution', 'Yes - as a Angel', NULL, 'less than $50.000', 'sadasd', NULL, NULL),
(2, NULL, NULL, NULL, NULL, NULL, NULL, 'Personal Investment', 'Yes - as a LP', NULL, 'less than $50.000', NULL, NULL, NULL),
(3, 'mahar', 'mahar.catur@gmail.com', 'akdnawkdn', 'tunnel', 'kabdaskd', 'kabdawk', 'Personal Investment', 'Yes - as a LP', 'Direct Investment and Co-Investment,Private debt', 'less than $50.000', 'sadsada', NULL, NULL),
(4, 'das', 'mahar.catur@gmail.com', 'bkab', 'uikb', NULL, 'ui', 'Education', 'Yes - as a LP', NULL, 'less than $50.000', 'kkkbn', '2022-01-27', '2022-01-27'),
(5, 'mahar', 'mahar.catur@gmail.com', 'askjd', 'tunnel', NULL, 'KBKAB', 'Corporation', 'Yes - as a Angel', NULL, '$50.001 to $100.000', 'askjdad', '2022-01-30', '2022-01-30');

-- --------------------------------------------------------

--
-- Table structure for table `register_partnerships`
--

CREATE TABLE `register_partnerships` (
  `id_partner` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `job_title` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mobile_num` varchar(20) DEFAULT NULL,
  `company_web` varchar(100) DEFAULT NULL,
  `linked_profile_url` varchar(100) DEFAULT NULL,
  `type_company` varchar(100) DEFAULT NULL,
  `company_operating_in` varchar(100) DEFAULT NULL,
  `Which_type_of_partnership_below_that_suits_your_interest` varchar(100) DEFAULT NULL,
  `Have_you_invested_in_a_venture_before` varchar(100) DEFAULT NULL,
  `Have_you_mentored_any_startup_before` varchar(255) DEFAULT NULL,
  `How_did_you_hear_about_Tunnelerate` varchar(255) DEFAULT NULL,
  `How_do_you_prefer_to_be_contacted` varchar(255) DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `created_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `register_partnerships`
--

INSERT INTO `register_partnerships` (`id_partner`, `name`, `job_title`, `company`, `email`, `mobile_num`, `company_web`, `linked_profile_url`, `type_company`, `company_operating_in`, `Which_type_of_partnership_below_that_suits_your_interest`, `Have_you_invested_in_a_venture_before`, `Have_you_mentored_any_startup_before`, `How_did_you_hear_about_Tunnelerate`, `How_do_you_prefer_to_be_contacted`, `updated_at`, `created_at`) VALUES
(1, '', '', 'tunnel', 'mahar.catur@gmail.com', '', 'tunnel', '', '', 'Indonesia', '', '', '', '', '', '2022-01-23', '2022-01-23'),
(2, '', '', 'tunnel', 'mahar.catur@gmail.com', '', 'tunnel', '', '', 'Indonesia', '', '', '', '', '', '2022-01-23', '2022-01-23'),
(3, '', '', 'tunnel', 'mahar.catur@gmail.com', '', 'tunnel', '', '', 'Indonesia', '', '', '', '', '', '2022-01-23', '2022-01-23'),
(4, '', '', 'it', 'mahar@tunnel', '', 'it', '', '', 'Singapore', '', '', '', '', '', '2022-01-23', '2022-01-23'),
(5, '', '', 'tunnel', 'mahar.catur@gmail.com', '', 'it', '', '', 'Indonesia', '', '', '', '', '', '2022-01-23', '2022-01-23'),
(6, 'mahar', '', 'tunnel', 'mahar.catur@gmail.com', '', 'tunnel', '', '', 'Indonesia', '', '', '', '', '', '2022-01-23', '2022-01-23'),
(7, NULL, 'it', 'tunnel', 'mahar.catur@gmail.com', '0818270498', 'tunnel', 'linkedin.com/mhrctrfrz', NULL, 'Malaysia', 'cobainn', 'Yes - as a LP', 'No', 'cobainn', NULL, NULL, NULL),
(8, 'aukdna', 'ibasdka', 'onadoawn', 'mahar.catur@istn.ac.id', '9128712', 'ajdbaj', 'ad aw', 'Education,Government', 'Indonesia', 'ajdsbaw', 'Yes - as a LP', 'Yes', 'jasdas', NULL, NULL, NULL),
(9, 'adsas', 'sad', 'tunnel', 'mahar.catur@gmail.com', '0818270498', NULL, 'asd', 'Education', 'Indonesia', NULL, 'Yes', NULL, 'asda', 'asdas', '2022-01-27', '2022-01-27'),
(10, 'asda', 'asda', 'tunnel', 'mahar.catur@gmail.com', '0818270498', NULL, 'asda', 'Education', 'Indonesia', NULL, 'Yes - as a LP', 'Yes', 'asda', 'asda', '2022-01-27', '2022-01-27'),
(11, 'Mahar', 'asda', 'asd', 'mahar.catur@gmail.com', '0818270498', NULL, 'asd', 'Education', 'Indonesia', 'Indonesia', 'Yes - as a LP', 'Yes', 'asda', 'sad', '2022-01-27', '2022-01-27'),
(12, 'kajdbas', 'ajds', 'tunnel', 'mahar.catur@gmail.com', '0818270498', NULL, 'lsandl', 'Education', 'Laos', 'Educational Institution', 'No', 'No', 'kbasd', 'jkasdbask', '2022-01-30', '2022-01-30'),
(13, 'asjkdbas', 'ajkds', 'onadoawn', 'mahar.catur@istn.ac.id', '9128712', NULL, 'sada', 'Education', 'Laos', 'Overseas Partner', 'No', 'No', 'kbsakd', 'jaskd', '2022-01-30', '2022-01-30');

-- --------------------------------------------------------

--
-- Table structure for table `register_startups`
--

CREATE TABLE `register_startups` (
  `id_startup` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `linkedin` varchar(255) NOT NULL,
  `other_co_founder_linkedin` varchar(255) NOT NULL,
  `company_based` varchar(255) NOT NULL,
  `company_website` varchar(255) NOT NULL,
  `company_description` varchar(255) NOT NULL,
  `pitch` varchar(255) NOT NULL,
  `demo` varchar(255) NOT NULL,
  `prob_solv` varchar(255) NOT NULL,
  `solution` varchar(255) NOT NULL,
  `product_works` varchar(255) NOT NULL,
  `product_for` varchar(100) NOT NULL,
  `competitor` varchar(255) NOT NULL,
  `incorporated` varchar(255) NOT NULL,
  `long_working` varchar(50) NOT NULL,
  `participated` varchar(100) NOT NULL,
  `received` varchar(100) NOT NULL,
  `biggest_prob` varchar(255) NOT NULL,
  `need` varchar(255) NOT NULL,
  `anything` varchar(255) NOT NULL,
  `updated_at` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `register_startups`
--

INSERT INTO `register_startups` (`id_startup`, `name`, `email`, `linkedin`, `other_co_founder_linkedin`, `company_based`, `company_website`, `company_description`, `pitch`, `demo`, `prob_solv`, `solution`, `product_works`, `product_for`, `competitor`, `incorporated`, `long_working`, `participated`, `received`, `biggest_prob`, `need`, `anything`, `updated_at`, `created_at`) VALUES
(1, 'zXmbxKBSAD', 'mahar.catur@gmail.com', 'KSABDK', 'KSABDK', 'KSABDK', 'BKSADB', 'KBSAKDB', '1643594944.png', 'MASDBSA', 'MBSAMDASB', 'bsadmab', 'bsamdabb', 'mbsamdba', 'mbsamdb', ',bsad,db', ',basmsb', 'asjdb', 'a,dba', 'basmd', 'adjkb', 'jasb', '2022-01-31 02:09:04', '2022-01-31 02:09:04');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dser_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dser_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dser_paragraph` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `created_at`, `updated_at`, `dser_image`, `dser_title`, `dser_paragraph`) VALUES
(1, '2022-01-25 19:36:38', '2022-01-25 21:34:44', 'assets/img/services/1.png', 'Helping you grow', 'As founders, we know how hard it is to build a company from scratch. We wanted to help you realized and grow your business from ground-up during in our 10 weeks long program without any program fees'),
(2, '2022-01-25 19:36:38', '2022-01-25 21:35:58', 'assets/img/services/2.png', 'Industry expertise', 'We are a set of ex-founders that wanted to give back to the community. Thus, we want to help other startup founders in anyway possible such as operation, strategy, finance, legal, human capital, marketing, and so forth.'),
(3, '2022-01-25 19:36:38', '2022-01-25 21:38:17', 'assets/img/services/3.png', 'Here for the long-run', 'As part of our commitment with you, we invest up to $100,000. We will continue to support you during and after the program as we wanted to be your go-to partner for the long term.');

-- --------------------------------------------------------

--
-- Table structure for table `service_contents`
--

CREATE TABLE `service_contents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dserc_sec_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dserc_sec_title_2` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dserc_big_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service_contents`
--

INSERT INTO `service_contents` (`id`, `created_at`, `updated_at`, `dserc_sec_title`, `dserc_sec_title_2`, `dserc_big_image`) VALUES
(1, '2022-01-25 19:36:38', '2022-01-25 21:39:12', 'Tunnelerate', 'HOW WE WORK', 'assets/img/services/bg.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `subscribes`
--

CREATE TABLE `subscribes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ds_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subscribes`
--

INSERT INTO `subscribes` (`id`, `created_at`, `updated_at`, `ds_email`) VALUES
(1, '2022-01-25 19:36:38', '2022-01-25 19:36:38', 'hhhhhhhhhh@gmail.com'),
(2, '2022-01-25 19:36:38', '2022-01-25 19:36:38', 'aaaaaaaa@gmail.com'),
(3, '2022-01-25 19:36:38', '2022-01-25 19:36:38', 'sssssssssss@gmail.com'),
(4, '2022-01-25 19:36:38', '2022-01-25 19:36:38', 'dddddddd@gmail.com'),
(5, '2022-01-25 19:36:38', '2022-01-25 19:36:38', 'fffffffffff@gmail.com'),
(6, '2022-01-25 19:36:38', '2022-01-25 19:36:38', 'ggggggggg@gmail.com'),
(7, '2022-01-25 19:36:38', '2022-01-25 19:36:38', 'jjjjjjjj@gmail.com'),
(8, '2022-01-25 19:36:38', '2022-01-25 19:36:38', 'kkkkkkkkkk@gmail.com'),
(9, '2022-01-25 19:36:38', '2022-01-25 19:36:38', 'lllllllllll@gmail.com'),
(10, '2022-01-25 19:36:38', '2022-01-25 19:36:38', 'qqqqqqqqq@gmail.com'),
(11, '2022-01-25 21:08:48', '2022-01-25 21:08:48', 'mahar.catur@istn.ac.id');

-- --------------------------------------------------------

--
-- Table structure for table `subscribe_contents`
--

CREATE TABLE `subscribe_contents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dsubscribeco_title` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dsubscribeco_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dsubscribeco_btn_title` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subscribe_contents`
--

INSERT INTO `subscribe_contents` (`id`, `created_at`, `updated_at`, `dsubscribeco_title`, `dsubscribeco_image`, `dsubscribeco_btn_title`) VALUES
(1, '2022-01-25 19:36:38', '2022-01-26 00:35:35', 'subscribe our newsletter<br> to get more update', 'assets/img/subscribe/16431825351.png', 'Subscribe');

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dtm_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dtm_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dtm_designation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dtm_social_icon1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dtm_social_icon_url1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dtm_social_icon2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dtm_social_icon_url2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dtm_social_icon3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dtm_social_icon_url3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dtm_social_icon4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dtm_social_icon_url4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `created_at`, `updated_at`, `dtm_image`, `dtm_title`, `dtm_designation`, `dtm_social_icon1`, `dtm_social_icon_url1`, `dtm_social_icon2`, `dtm_social_icon_url2`, `dtm_social_icon3`, `dtm_social_icon_url3`, `dtm_social_icon4`, `dtm_social_icon_url4`) VALUES
(1, '2022-01-25 19:36:38', '2022-01-28 01:13:00', 'assets/img/team/1643191248.png', 'Ivan Arie Sustiawan', 'Founding & Managing Partner', 'fa fa-linkedin', 'https://www.linkedin.com/in/ivanariesustiawan/', 'fa fa-twitter', '#', 'fa fa-instagram', '', '', ''),
(2, '2022-01-25 19:36:38', '2022-01-28 01:12:12', 'assets/img/team/1643191301.png', 'Ayunda Afifa', 'Partner', 'fa fa-linkedin', 'https://www.linkedin.com/in/ayundaafifa/', 'fa fa-twitter', '#', 'fa fa-instagram', '#', '', ''),
(3, '2022-01-25 19:36:38', '2022-01-28 01:15:11', 'assets/img/team/1643357711.png', 'Bharat Ongso', 'Founding Partner', 'fa fa-linkedin', 'https://www.linkedin.com/in/bharat-ongso-33a40a53/', 'fa fa-twitter', '#', 'fa fa-instagram', '#', '', ''),
(4, '2022-01-25 19:36:38', '2022-01-28 01:16:09', 'assets/img/team/1643357761.png', 'Riswanto', 'Founding Partner', 'fa fa-linkedin', 'https://www.linkedin.com/in/rriswant/', 'fa fa-twitter', '#', 'fa fa-instagram', '#', 'fa fa-vk', '#'),
(5, '2022-01-28 01:17:32', '2022-01-28 01:17:42', 'assets/img/team/1643357852.png', 'Faisal Fahmi', 'Investment Manager', 'fa fa-linkedin', 'https://www.linkedin.com/in/muhammadfaisalfahmi/', 'fa fa-twitter', '#', 'fa fa-instagram', '#', '', ''),
(6, '2022-01-28 01:19:37', '2022-01-28 01:19:37', 'assets/img/team/1643357977.png', 'Jatin Raisinghani', 'Strategic Partnership Manager', 'fa fa-linkedin', 'https://www.linkedin.com/in/jatin-raisinghani-b82258147/', 'fa fa-twitter', '#', 'fa fa-instagram', '#', '', ''),
(7, '2022-01-28 01:20:51', '2022-01-28 01:20:51', 'assets/img/team/1643358051.png', 'Mahar Catur Ferniza', 'Full Stack Enginner', 'fa fa-linkedin', 'https://www.linkedin.com/in/mhrctrfrnz/', 'fa fa-twitter', '#', 'fa fa-instagram', '#', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `team_contents`
--

CREATE TABLE `team_contents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dtmcon_sec_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dtmcon_sec_title_2` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `team_contents`
--

INSERT INTO `team_contents` (`id`, `created_at`, `updated_at`, `dtmcon_sec_title`, `dtmcon_sec_title_2`) VALUES
(1, '2022-01-25 19:36:38', '2022-01-28 01:11:18', 'Tunnelerate Team', 'experience team');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dts_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dts_desig` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dts_paragraph` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dts_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `created_at`, `updated_at`, `dts_title`, `dts_desig`, `dts_paragraph`, `dts_image`) VALUES
(1, '2022-01-25 19:36:38', '2022-01-25 21:44:32', 'Jaring Pangan', '', 'Indonesian AgriTech startup that enables farmers sell directly to consumers.', 'assets/img/testimonial/16431722722.png'),
(2, '2022-01-25 19:36:38', '2022-01-25 21:45:45', 'Era Tani', '', 'Tech-based agriculture micro financing, supply chain, and commerce solutions for paddy farmers.', 'assets/img/testimonial/16431723452.png');

-- --------------------------------------------------------

--
-- Table structure for table `testimonial_contents`
--

CREATE TABLE `testimonial_contents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dtmcon_sec_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dtmcon_sec_title_2` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `testimonial_contents`
--

INSERT INTO `testimonial_contents` (`id`, `created_at`, `updated_at`, `dtmcon_sec_title`, `dtmcon_sec_title_2`) VALUES
(1, '2022-01-25 19:36:38', '2022-01-25 21:42:56', 'Selected companies', 'within our ecosystem');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'theme_group@gmail.com', NULL, '$2y$10$E9CHqkWRrJPJte4Hn4knguJyTzXeNhACjermERMzsjePziD1t1XbS', NULL, '2022-01-25 19:36:38', '2022-01-25 19:36:38');

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dv_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dv_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `created_at`, `updated_at`, `dv_image`, `dv_url`) VALUES
(1, '2022-01-25 19:36:38', '2022-01-25 21:52:38', 'assets/img/video/1643172758.png', 'https://www.youtube.com/watch?v=Sp9_JVC5Tjo');

-- --------------------------------------------------------

--
-- Table structure for table `video_contents`
--

CREATE TABLE `video_contents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dvideoc_sec_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dvideoc_sec_title_2` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dvideoc_big_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `video_contents`
--

INSERT INTO `video_contents` (`id`, `created_at`, `updated_at`, `dvideoc_sec_title`, `dvideoc_sec_title_2`, `dvideoc_big_image`) VALUES
(1, '2022-01-25 19:36:38', '2022-01-25 21:53:47', 'Tunnelerate Videos', 'Videos', 'assets/img/video/16431728001.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `widget1s`
--

CREATE TABLE `widget1s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dfw1_logo_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dfw1_paragraph` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dfw1_social_icon_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dfw1_social_icon_1_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dfw1_social_icon_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dfw1_social_icon_2_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dfw1_social_icon_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dfw1_social_icon_3_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dfw1_social_icon_4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dfw1_social_icon_4_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `widget1s`
--

INSERT INTO `widget1s` (`id`, `created_at`, `updated_at`, `dfw1_logo_image`, `dfw1_paragraph`, `dfw1_social_icon_1`, `dfw1_social_icon_1_link`, `dfw1_social_icon_2`, `dfw1_social_icon_2_link`, `dfw1_social_icon_3`, `dfw1_social_icon_3_link`, `dfw1_social_icon_4`, `dfw1_social_icon_4_link`) VALUES
(1, '2022-01-25 19:36:38', '2022-01-26 17:22:10', 'assets/img/1643173502.png', '', 'fa fa-youtube', 'https://www.youtube.com/channel/UCk-o5LFqtHmez_-txTDRUfw', 'fa fa-twitter', 'https://twitter.com/tunnelerate', 'fa fa-instagram', 'https://www.instagram.com/tunnelerate/', 'fa fa-spotify', 'https://anchor.fm/tunnelerate');

-- --------------------------------------------------------

--
-- Table structure for table `widget2s`
--

CREATE TABLE `widget2s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dwidget2_text` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dwidget2_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `widget2s`
--

INSERT INTO `widget2s` (`id`, `created_at`, `updated_at`, `dwidget2_text`, `dwidget2_link`) VALUES
(1, '2022-01-25 19:36:38', '2022-01-25 19:36:38', 'Privacy Policy', '#'),
(2, '2022-01-25 19:36:38', '2022-01-25 19:36:38', 'Support Policy', '#'),
(3, '2022-01-25 19:36:38', '2022-01-25 19:36:38', 'Terms & Conditions', '#'),
(4, '2022-01-25 19:36:38', '2022-01-25 19:36:38', 'About Us', '#'),
(5, '2022-01-25 19:36:38', '2022-01-25 19:36:38', 'Contact', '#');

-- --------------------------------------------------------

--
-- Table structure for table `widget3s`
--

CREATE TABLE `widget3s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dwidget3_icon_text` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dwidget3_info` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `widget3s`
--

INSERT INTO `widget3s` (`id`, `created_at`, `updated_at`, `dwidget3_icon_text`, `dwidget3_info`) VALUES
(1, '2022-01-25 19:36:38', '2022-01-27 15:38:34', 'fa fa-map-marker', 'South Quarter Building Complex\r\nJalan RA Kartini Kav 8, Cilandak, Jakarta Selatan'),
(2, '2022-01-25 19:36:38', '2022-01-27 15:38:57', 'fa fa-headphones', '021-5098 2659'),
(4, '2022-01-27 15:39:36', '2022-01-27 15:40:38', 'fa fa-envelope', 'admin@tunnelerate.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abouts`
--
ALTER TABLE `abouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `acclerators`
--
ALTER TABLE `acclerators`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_contents`
--
ALTER TABLE `blog_contents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contactinfos`
--
ALTER TABLE `contactinfos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `counters`
--
ALTER TABLE `counters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ctas`
--
ALTER TABLE `ctas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `generals`
--
ALTER TABLE `generals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invests`
--
ALTER TABLE `invests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `main_sliders`
--
ALTER TABLE `main_sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `register_investments`
--
ALTER TABLE `register_investments`
  ADD PRIMARY KEY (`id_register_investement`);

--
-- Indexes for table `register_partnerships`
--
ALTER TABLE `register_partnerships`
  ADD PRIMARY KEY (`id_partner`);

--
-- Indexes for table `register_startups`
--
ALTER TABLE `register_startups`
  ADD PRIMARY KEY (`id_startup`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_contents`
--
ALTER TABLE `service_contents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribes`
--
ALTER TABLE `subscribes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribe_contents`
--
ALTER TABLE `subscribe_contents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `team_contents`
--
ALTER TABLE `team_contents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonial_contents`
--
ALTER TABLE `testimonial_contents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `video_contents`
--
ALTER TABLE `video_contents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `widget1s`
--
ALTER TABLE `widget1s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `widget2s`
--
ALTER TABLE `widget2s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `widget3s`
--
ALTER TABLE `widget3s`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abouts`
--
ALTER TABLE `abouts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `acclerators`
--
ALTER TABLE `acclerators`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `blog_contents`
--
ALTER TABLE `blog_contents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `contactinfos`
--
ALTER TABLE `contactinfos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `counters`
--
ALTER TABLE `counters`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ctas`
--
ALTER TABLE `ctas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `generals`
--
ALTER TABLE `generals`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `invests`
--
ALTER TABLE `invests`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `main_sliders`
--
ALTER TABLE `main_sliders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `partners`
--
ALTER TABLE `partners`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `register_investments`
--
ALTER TABLE `register_investments`
  MODIFY `id_register_investement` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `register_partnerships`
--
ALTER TABLE `register_partnerships`
  MODIFY `id_partner` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `register_startups`
--
ALTER TABLE `register_startups`
  MODIFY `id_startup` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `service_contents`
--
ALTER TABLE `service_contents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `subscribes`
--
ALTER TABLE `subscribes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `subscribe_contents`
--
ALTER TABLE `subscribe_contents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `team_contents`
--
ALTER TABLE `team_contents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `testimonial_contents`
--
ALTER TABLE `testimonial_contents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `video_contents`
--
ALTER TABLE `video_contents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `widget1s`
--
ALTER TABLE `widget1s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `widget2s`
--
ALTER TABLE `widget2s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `widget3s`
--
ALTER TABLE `widget3s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
