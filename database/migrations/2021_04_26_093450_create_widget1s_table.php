<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWidget1sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('widget1s', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('dfw1_logo_image');
            $table->longText('dfw1_paragraph')->nullable();
            $table->string('dfw1_social_icon_1')->nullable();
            $table->string('dfw1_social_icon_1_link')->nullable();
            $table->string('dfw1_social_icon_2')->nullable();
            $table->string('dfw1_social_icon_2_link')->nullable();
            $table->string('dfw1_social_icon_3')->nullable();
            $table->string('dfw1_social_icon_3_link')->nullable();
            $table->string('dfw1_social_icon_4')->nullable();
            $table->string('dfw1_social_icon_4_link')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('widget1s');
    }
}
