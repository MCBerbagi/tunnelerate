<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('dtm_image');
            $table->string('dtm_title');
            $table->string('dtm_designation')->nullable();
            $table->string('dtm_social_icon1')->nullable();
            $table->string('dtm_social_icon_url1')->nullable();
            $table->string('dtm_social_icon2')->nullable();
            $table->string('dtm_social_icon_url2')->nullable();
            $table->string('dtm_social_icon3')->nullable();
            $table->string('dtm_social_icon_url3')->nullable();
            $table->string('dtm_social_icon4')->nullable();
            $table->string('dtm_social_icon_url4')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams');
    }
}
