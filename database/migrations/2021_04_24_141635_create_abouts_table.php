<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAboutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('abouts', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->longText('dab_sec_title');
            $table->longText('dab_sec_title_2')->nullable();
            $table->longText('dab_srt_paragraph')->nullable();
            $table->longText('dab_long_paragraph')->nullable();
            $table->longText('dab_list_info')->nullable();
            $table->string('dab_btn')->nullable();
            $table->string('dab_url')->nullable();
            $table->string('dab_big_image');
            $table->string('dab_brand_image')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('abouts');
    }
}
