<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('dpr_title')->nullable();
            $table->string('dpr_mobile')->nullable();
            $table->string('dpr_email')->nullable();
            $table->longText('dpr_content');
            $table->string('dpr_image');
            $table->string('dpr_social1')->nullable();
            $table->string('dpr_social2')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
