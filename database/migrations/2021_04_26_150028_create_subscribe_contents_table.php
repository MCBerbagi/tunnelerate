<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscribeContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscribe_contents', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->longText('dsubscribeco_title')->nullable();
            $table->string('dsubscribeco_image');
            $table->longText('dsubscribeco_btn_title')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscribe_contents');
    }
}
