<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMainSlidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('main_sliders', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('dms_title');
            $table->string('dms_title2');
            $table->longText('dms_paragraph')->nullable();
            $table->string('dms_btn1')->nullable();
            $table->string('dms_url1')->nullable();
            $table->string('dms_btn2')->nullable();
            $table->string('dms_url2')->nullable();
            $table->string('dms_image');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('main_sliders');
    }
}
