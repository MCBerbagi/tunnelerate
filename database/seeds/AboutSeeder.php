<?php

use App\Models\About;
use Illuminate\Database\Seeder;

class AboutSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        About::create([
            'id'=>'1',
            'dab_sec_title'=>'About Us',
            'dab_sec_title_2'=>'about <span>merox</span> pro',
            'dab_srt_paragraph'=>"There are many variations of passages of Lorem Ipsum but the majority have suffered alteration in some form injected, or randomised words which don't look even slightly. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything",
            'dab_long_paragraph'=>"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever when an unknown printer took a galley of Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever when an unknown printer took a galley of",
            'dab_list_info'=>"<li><i class='fa fa-check'></i>There are many variations of passages of Lorem </li><li><i class='fa fa-check'></i>Majority have suffered alteration in some </li><li><i class='fa fa-check'></i>Sure there isn't anything embarrassing hidden in the </li><li><i class='fa fa-check'></i>All the Lorem Ipsum generators on the Internet tend to </li>",
            'dab_btn'=>'about us',
            'dab_url'=>'#',
            'dab_big_image'=>'assets/img/about/about-1.jpg',
            'dab_brand_image'=>'assets/img/about/about.png',
        ]);
    }
}
