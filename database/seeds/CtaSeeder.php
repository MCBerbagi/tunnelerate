<?php

use App\Models\Cta;
use Illuminate\Database\Seeder;

class CtaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Cta::create([
            'id'=>'1',
            'dcta_title'=>'Do You Have Any Project ?',
            'dcta_title_2'=>'let’s talk about business soluations with us',
            'dcta_btn'=>'join with us',
            'dcta_url'=>'#',
            'dcta_big_image'=>'assets/img/cta/cta-bg.jpg',
        ]);
    }
}
