<?php

use App\Models\Subscribe;
use Illuminate\Database\Seeder;

class SubscribeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Subscribe::create([
            'id'=>'1',
            'ds_email'=>'hhhhhhhhhh@gmail.com',
        ]);
        Subscribe::create([
            'id'=>'2',
            'ds_email'=>'aaaaaaaa@gmail.com',
        ]);
        Subscribe::create([
            'id'=>'3',
            'ds_email'=>'sssssssssss@gmail.com',
        ]);
        Subscribe::create([
            'id'=>'4',
            'ds_email'=>'dddddddd@gmail.com',
        ]);
        Subscribe::create([
            'id'=>'5',
            'ds_email'=>'fffffffffff@gmail.com',
        ]);
        Subscribe::create([
            'id'=>'6',
            'ds_email'=>'ggggggggg@gmail.com',
        ]);
        Subscribe::create([
            'id'=>'7',
            'ds_email'=>'jjjjjjjj@gmail.com',
        ]);
        Subscribe::create([
            'id'=>'8',
            'ds_email'=>'kkkkkkkkkk@gmail.com',
        ]);
        Subscribe::create([
            'id'=>'9',
            'ds_email'=>'lllllllllll@gmail.com',
        ]);
        Subscribe::create([
            'id'=>'10',
            'ds_email'=>'qqqqqqqqq@gmail.com',
        ]);
    }
}
