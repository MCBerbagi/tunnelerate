<?php

use App\Models\Team;
use Illuminate\Database\Seeder;

class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Team::create([
            'id'=>'1',
            'dtm_image'=>'assets/img/team/1.jpg',
            'dtm_title'=>'david walillams',
            'dtm_designation'=>'consultant',
            'dtm_social_icon1'=>'fa fa-facebook',
            'dtm_social_icon_url1'=>'#',
            'dtm_social_icon2'=>'fa fa-twitter',
            'dtm_social_icon_url2'=>'#',
            'dtm_social_icon3'=>'fa fa-instagram',
            'dtm_social_icon_url3'=>'#',
            'dtm_social_icon4'=>'fa fa-vk',
            'dtm_social_icon_url4'=>'#',
        ]);
        Team::create([
            'id'=>'2',
            'dtm_image'=>'assets/img/team/2.jpg',
            'dtm_title'=>'benjamin mark',
            'dtm_designation'=>'director',
            'dtm_social_icon1'=>'fa fa-facebook',
            'dtm_social_icon_url1'=>'#',
            'dtm_social_icon2'=>'fa fa-twitter',
            'dtm_social_icon_url2'=>'#',
            'dtm_social_icon3'=>'fa fa-instagram',
            'dtm_social_icon_url3'=>'#',
            'dtm_social_icon4'=>'fa fa-vk',
            'dtm_social_icon_url4'=>'#',
        ]);
        Team::create([
            'id'=>'3',
            'dtm_image'=>'assets/img/team/3.jpg',
            'dtm_title'=>'steve louis',
            'dtm_designation'=>'developer',
            'dtm_social_icon1'=>'fa fa-facebook',
            'dtm_social_icon_url1'=>'#',
            'dtm_social_icon2'=>'fa fa-twitter',
            'dtm_social_icon_url2'=>'#',
            'dtm_social_icon3'=>'fa fa-instagram',
            'dtm_social_icon_url3'=>'#',
            'dtm_social_icon4'=>'fa fa-vk',
            'dtm_social_icon_url4'=>'#',
        ]);
        Team::create([
            'id'=>'4',
            'dtm_image'=>'assets/img/team/4.jpg',
            'dtm_title'=>'shawn philip',
            'dtm_designation'=>'designer',
            'dtm_social_icon1'=>'fa fa-facebook',
            'dtm_social_icon_url1'=>'#',
            'dtm_social_icon2'=>'fa fa-twitter',
            'dtm_social_icon_url2'=>'#',
            'dtm_social_icon3'=>'fa fa-instagram',
            'dtm_social_icon_url3'=>'#',
            'dtm_social_icon4'=>'fa fa-vk',
            'dtm_social_icon_url4'=>'#',
        ]);
    }
}
