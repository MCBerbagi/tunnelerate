<?php

use App\Models\Widget2;
use Illuminate\Database\Seeder;

class Widget2Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Widget2::create([
            'id'=>'1',
            'dwidget2_text'=>'Privacy Policy',
            'dwidget2_link'=>'#',
        ]);
        Widget2::create([
            'id'=>'2',
            'dwidget2_text'=>'Support Policy',
            'dwidget2_link'=>'#',
        ]);
        Widget2::create([
            'id'=>'3',
            'dwidget2_text'=>'Terms & Conditions',
            'dwidget2_link'=>'#',
        ]);
        Widget2::create([
            'id'=>'4',
            'dwidget2_text'=>'About Us',
            'dwidget2_link'=>'#',
        ]);
        Widget2::create([
            'id'=>'5',
            'dwidget2_text'=>'Contact',
            'dwidget2_link'=>'#',
        ]);
    }
}
