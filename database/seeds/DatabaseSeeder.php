<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(GeneralSeeder::class);
         $this->call(MainsliderSeeder::class);
         $this->call(AboutSeeder::class);
         $this->call(CounterSeeder::class);
         $this->call(ServicecontentSeeder::class);
         $this->call(ServiceSeeder::class);
         $this->call(TeamcontentSeeder::class);
         $this->call(TeamSeeder::class);
         $this->call(CtaSeeder::class);
         $this->call(TestimonialcontentSeeder::class);
         $this->call(TestimonialSeeder::class);
         $this->call(VideocontentSeeder::class);
         $this->call(VideoSeeder::class);
         $this->call(BlogcontentSeeder::class);
         $this->call(BlogSeeder::class);
         $this->call(SubscribeSeeder::class);
         $this->call(SubscribecontentSeeder::class);
         $this->call(ClientSeeder::class);
         $this->call(Widget1Seeder::class);
         $this->call(Widget2Seeder::class);
         $this->call(Widget3Seeder::class);
         $this->call(ContactSeeder::class);
         $this->call(ContactinfoSeeder::class);
         $this->call(UserSeeder::class);
    }
}
