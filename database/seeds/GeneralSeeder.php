<?php

use App\Models\General;
use Illuminate\Database\Seeder;

class GeneralSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        General::create([
            'id'=>'1',
            'dsite_title'=>'Merox - Business Management System',
            'dtop_logo'=>'assets/img/logo-1.png',
            'dfavicon_icon'=>'assets/img/favicon.png',
            'dbreadcrumb_bg'=>'assets/img/breadcrumb.jpg',
            'dmap_api_key'=>'',
            'dfooter_copyright'=>'<p>© All Right Reserved by <a href="#">theme_group</a></p>',
        ]);
    }
}
