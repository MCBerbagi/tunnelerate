<?php

use App\Models\Counter;
use Illuminate\Database\Seeder;

class CounterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Counter::create([
            'id'=>'1',
            'dc_icon'=>'assets/img/counter/1.svg',
            'dc_number'=>'653',
            'dc_title'=>'Happy Clients',
        ]);
        Counter::create([
            'id'=>'2',
            'dc_icon'=>'assets/img/counter/2.svg',
            'dc_number'=>'465',
            'dc_title'=>'Trusted Users',
        ]);
        Counter::create([
            'id'=>'3',
            'dc_icon'=>'assets/img/counter/3.svg',
            'dc_number'=>'784',
            'dc_title'=>'Projects Done',
        ]);
        Counter::create([
            'id'=>'4',
            'dc_icon'=>'assets/img/counter/4.svg',
            'dc_number'=>'123',
            'dc_title'=>'Coffe Cup',
        ]);
    }
}
