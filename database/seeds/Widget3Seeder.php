<?php

use App\Models\Widget3;
use Illuminate\Database\Seeder;

class Widget3Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Widget3::create([
            'id'=>'1',
            'dwidget3_icon_text'=>'fa fa-map-marker',
            'dwidget3_info'=>'address: 679 grand avenu, central parl, nyc, ny',
        ]);
        Widget3::create([
            'id'=>'2',
            'dwidget3_icon_text'=>'fa fa-headphones',
            'dwidget3_info'=>'Custom Support & Sale: +012-345-678',
        ]);
        Widget3::create([
            'id'=>'3',
            'dwidget3_icon_text'=>'fa fa-clock-o',
            'dwidget3_info'=>'Working Time: Mon-Sat: 9 Am – 5 Pm',
        ]);
    }
}
