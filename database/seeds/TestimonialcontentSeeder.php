<?php

use App\Models\TestimonialContent;
use Illuminate\Database\Seeder;

class TestimonialcontentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TestimonialContent::create([
            'id'=>'1',
            'dtmcon_sec_title'=>'testimonials',
            'dtmcon_sec_title_2'=>'client’s say',
        ]);
    }
}
