<?php

use App\Models\MainSlider;
use Illuminate\Database\Seeder;

class MainsliderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MainSlider::create([
            'id'=>'1',
            'dms_title'=>'always top rated',
            'dms_title2'=>'we are coming <br> with <b>merox</b> business pro.',
            'dms_paragraph'=>'There are many variations of passages of Lorem Ipsum , but the majority have suffered alteration in',
            'dms_btn1'=>'get started',
            'dms_url1'=>'#',
            'dms_btn2'=>'learn more',
            'dms_url2'=>'#',
            'dms_image'=>'assets/img/slider/slider-1.jpg',
        ]);
        MainSlider::create([
            'id'=>'2',
            'dms_title'=>'always top rated',
            'dms_title2'=>'we are coming <br> with <b>merox</b> business pro.',
            'dms_paragraph'=>'There are many variations of passages of Lorem Ipsum , but the majority have suffered alteration in',
            'dms_btn1'=>'get started',
            'dms_url1'=>'#',
            'dms_btn2'=>'learn more',
            'dms_url2'=>'#',
            'dms_image'=>'assets/img/slider/slider-1.jpg',
        ]);
    }
}
