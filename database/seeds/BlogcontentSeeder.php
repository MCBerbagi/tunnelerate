<?php

use App\Models\BlogContent;
use Illuminate\Database\Seeder;

class BlogcontentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BlogContent::create([
            'id'=>'1',
            'dtmcon_sec_title'=>'recent news',
            'dtmcon_sec_title_2'=>'news & articles',
        ]);
    }
}
