<?php

use App\Models\Testimonial;
use Illuminate\Database\Seeder;

class TestimonialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Testimonial::create([
            'id'=>'1',
            'dts_title'=>'daniel wells',
            'dts_desig'=>'developer',
            'dts_paragraph'=>"dummy text of the printing and type setting and industry. Lorem Ipsum has been the industry's",
            'dts_image'=>'assets/img/testimonial/client-1.png',
        ]);
        Testimonial::create([
            'id'=>'2',
            'dts_title'=>'morgan liu',
            'dts_desig'=>'expert',
            'dts_paragraph'=>"dummy text of the printing and type setting and industry. Lorem Ipsum has been the industry's",
            'dts_image'=>'assets/img/testimonial/client-2.png',
        ]);
        Testimonial::create([
            'id'=>'3',
            'dts_title'=>'mark man',
            'dts_desig'=>'writer',
            'dts_paragraph'=>"dummy text of the printing and type setting and industry. Lorem Ipsum has been the industry's",
            'dts_image'=>'assets/img/testimonial/client-3.png',
        ]);
    }
}
