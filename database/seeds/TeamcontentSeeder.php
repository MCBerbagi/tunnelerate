<?php

use App\Models\TeamContent;
use Illuminate\Database\Seeder;

class TeamcontentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TeamContent::create([
            'id'=>'1',
            'dtmcon_sec_title'=>'merox team',
            'dtmcon_sec_title_2'=>'experience team',
        ]);
    }
}
