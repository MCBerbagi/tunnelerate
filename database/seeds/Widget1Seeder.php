<?php

use App\Models\Widget1;
use Illuminate\Database\Seeder;

class Widget1Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Widget1::create([
            'id'=>'1',
            'dfw1_logo_image'=>'assets/img/logo-1.png',
            'dfw1_paragraph'=>"There are many variations of but the majority have suffered inject’s humour, or randomised word and don't look even slightly believable. If you are going to use",
            'dfw1_social_icon_1'=>'fa fa-facebook',
            'dfw1_social_icon_1_link'=>'#',
            'dfw1_social_icon_2'=>'fa fa-twitter',
            'dfw1_social_icon_2_link'=>'#',
            'dfw1_social_icon_3'=>'fa fa-instagram',
            'dfw1_social_icon_3_link'=>'#',
            'dfw1_social_icon_4'=>'fa fa-vk',
            'dfw1_social_icon_4_link'=>'#',
        ]);
    }
}
