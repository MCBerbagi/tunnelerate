<?php

use App\Models\Contactinfo;
use Illuminate\Database\Seeder;

class ContactinfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Contactinfo::create([
            'id'=>'1',
            'dci_icon'=>'assets/img/contact/1.png',
            'dci_info'=>'<li>+1-234-567-8991</li><li>+1-234-567-8991</li>',
        ]);
        Contactinfo::create([
            'id'=>'2',
            'dci_icon'=>'assets/img/contact/2.png',
            'dci_info'=>'<li>189 Matex Street, Badly</li><li>New York, NY-12548</li>',
        ]);
        Contactinfo::create([
            'id'=>'3',
            'dci_icon'=>'assets/img/contact/3.png',
            'dci_info'=>'<li><a href="mailto:merox@web.com">merox@web.com</a></li><li><a href="mailto:sales@website.com">sales@website.com</a></li>',
        ]);
    }
}
