<?php

use App\Models\Service;
use Illuminate\Database\Seeder;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Service::create([
            'id'=>'1',
            'dser_title'=>'easy customize',
            'dser_paragraph'=>'mext of the printing and has been type setting industry. Lorem bee the',
            'dser_image'=>'assets/img/services/1.png',
        ]);
        Service::create([
            'id'=>'2',
            'dser_title'=>'responsive ready',
            'dser_paragraph'=>'mext of the printing and has been type setting industry. Lorem bee the',
            'dser_image'=>'assets/img/services/2.png',
        ]);
        Service::create([
            'id'=>'3',
            'dser_title'=>'cloud storage',
            'dser_paragraph'=>'mext of the printing and has been type setting industry. Lorem bee the',
            'dser_image'=>'assets/img/services/3.png',
        ]);
    }
}
