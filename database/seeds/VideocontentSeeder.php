<?php

use App\Models\VideoContent;
use Illuminate\Database\Seeder;

class VideocontentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        VideoContent::create([
            'id'=>'1',
            'dvideoc_sec_title'=>'merox videos',
            'dvideoc_sec_title_2'=>'videos',
            'dvideoc_big_image'=>'assets/img/video/video-bg.jpg',
        ]);
    }
}
