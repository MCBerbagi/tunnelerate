<?php

use App\Models\Video;
use Illuminate\Database\Seeder;

class VideoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Video::create([
            'id'=>'1',
            'dv_image'=>'assets/img/video/video-1.jpg',
            'dv_url'=>'https://www.youtube.com/watch?v=snvzakfcTmY',
        ]);
    }
}
