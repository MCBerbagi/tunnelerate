<?php

use App\Models\Ecosystempartner;
use Illuminate\Database\Seeder;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Ecosystempartner::create([
            'id'=>'1',
            'dcli_link'=>'#',
            'dcli_image'=>'assets/img/client/1.png',
        ]);
        Ecosystempartner::create([
            'id'=>'2',
            'dcli_link'=>'#',
            'dcli_image'=>'assets/img/client/2.png',
        ]);
        Ecosystempartner::create([
            'id'=>'3',
            'dcli_link'=>'#',
            'dcli_image'=>'assets/img/client/3.png',
        ]);
        Ecosystempartner::create([
            'id'=>'4',
            'dcli_link'=>'#',
            'dcli_image'=>'assets/img/client/4.png',
        ]);
    }
}
