<?php

use App\Models\Contact;
use Illuminate\Database\Seeder;

class ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Contact::create([
            'id'=>'1',
            'dco_name'=>'abmack man',
            'dco_email'=>'aaaaaaaaa@gmail.com',
            'dco_subject'=>'How Can I Buy',
            'dco_message'=>'There are many variations of but the majority have suffered inject’s',
        ]);
        Contact::create([
            'id'=>'2',
            'dco_name'=>'master',
            'dco_email'=>'master@gmail.com',
            'dco_subject'=>'What Is This',
            'dco_message'=>'There are many variations of but the majority have suffered inject’s',
        ]);
        Contact::create([
            'id'=>'3',
            'dco_name'=>'uuuuuuu mmmmmmm',
            'dco_email'=>'umumum@gmail.com',
            'dco_subject'=>'How Can I Buy',
            'dco_message'=>'There are many variations of but the majority have suffered inject’s',
        ]);
    }
}
