<?php

use App\Models\ServiceContent;
use Illuminate\Database\Seeder;

class ServicecontentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ServiceContent::create([
            'id'=>'1',
            'dserc_sec_title'=>'merox services',
            'dserc_sec_title_2'=>'services',
            'dserc_big_image'=>'assets/img/services/bg.jpg',
        ]);
    }
}
