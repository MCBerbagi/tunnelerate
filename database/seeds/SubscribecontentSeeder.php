<?php

use App\Models\SubscribeContent;
use Illuminate\Database\Seeder;

class SubscribecontentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SubscribeContent::create([
            'id'=>'1',
            'dsubscribeco_title'=>'subscribe our newsletter<br> to get more update',
            'dsubscribeco_image'=>'assets/img/subscribe/subscribe-bg.jpg',
            'dsubscribeco_btn_title'=>'Subscribe',
        ]);
    }
}
