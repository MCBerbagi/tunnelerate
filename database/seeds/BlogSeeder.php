<?php

use App\Models\Blog;
use Illuminate\Database\Seeder;

class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Blog::create([
            'id'=>'1',
            'dblog_title'=>'in spring dumont opens mixed reality on experience popular belief',
            'dblog_tags'=>'blog,appnox,recent',
            'dblog_paragraph'=>"There are many variations of passages of Lorem Ipsum available, but the majority have suffered teration some form, by injected humour, or randomised words which don't look even slightly believable.",
            'dblog_long_content'=>"<p>It is a long established fact that a reader will be distracted by the readable content of a page when this looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years</p><br><h3>we’re fortunate to work with worldwide clients</h3><br><p>It is a long established fact that a reader will be distracted by the readable of a page when this looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters</p><p>It is a long established fact that a reader will be distracted by the readable of a page when this looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p><blockquote class='block-quote'><p>industry.has been the industry's standard dummy text ever since the 1500s, when an unk nown printer took a galley of type and and scrales</p><cite>mr. robul islam</cite></blockquote><p>It is a long established fact that a reader will be distracted by the readable of a page when this looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters</p><p>It is a long established fact that a reader will be distracted by the readable of a page when this looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>",
            'dblog_image'=>'assets/img/blog/1.jpg',
        ]);
        Blog::create([
            'id'=>'2',
            'dblog_title'=>'there are many variations of passages of but',
            'dblog_tags'=>'merox,awesome,blog',
            'dblog_paragraph'=>"There are many variations of passages of Lorem Ipsum available, but the majority have suffered teration some form, by injected humour, or randomised words which don't look even slightly believable.",
            'dblog_long_content'=>"<p>It is a long established fact that a reader will be distracted by the readable content of a page when this looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years</p><br><h3>we’re fortunate to work with worldwide clients</h3><br><p>It is a long established fact that a reader will be distracted by the readable of a page when this looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters</p><p>It is a long established fact that a reader will be distracted by the readable of a page when this looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p><blockquote class='block-quote'><p>industry.has been the industry's standard dummy text ever since the 1500s, when an unk nown printer took a galley of type and and scrales</p><cite>mr. robul islam</cite></blockquote><p>It is a long established fact that a reader will be distracted by the readable of a page when this looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters</p><p>It is a long established fact that a reader will be distracted by the readable of a page when this looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>",
            'dblog_image'=>'assets/img/blog/2.jpg',
        ]);
        Blog::create([
            'id'=>'3',
            'dblog_title'=>'not simply random text. it ha roots in a piece of',
            'dblog_tags'=>'blog,appnox,recent',
            'dblog_paragraph'=>"There are many variations of passages of Lorem Ipsum available, but the majority have suffered teration some form, by injected humour, or randomised words which don't look even slightly believable.",
            'dblog_long_content'=>"<p>It is a long established fact that a reader will be distracted by the readable content of a page when this looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years</p><br><h3>we’re fortunate to work with worldwide clients</h3><br><p>It is a long established fact that a reader will be distracted by the readable of a page when this looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters</p><p>It is a long established fact that a reader will be distracted by the readable of a page when this looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p><blockquote class='block-quote'><p>industry.has been the industry's standard dummy text ever since the 1500s, when an unk nown printer took a galley of type and and scrales</p><cite>mr. robul islam</cite></blockquote><p>It is a long established fact that a reader will be distracted by the readable of a page when this looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters</p><p>It is a long established fact that a reader will be distracted by the readable of a page when this looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>",
            'dblog_image'=>'assets/img/blog/3.jpg',
        ]);
    }
}
