<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\Widget2;
use App\Models\Profile;
use Illuminate\Http\Request;

class Widget2Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $widget2=Widget2::orderBy('id', 'asc')->latest()->get();
        $profile=Profile::first();
        return view('admin.footer.widget2', compact('widget2','profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)

    {
        $validatedData = $request->validate([

            'fwidget2_text' =>'required',
        ]);


        $widget2=new Widget2();
        $widget2->dwidget2_text=$request->fwidget2_text;
        $widget2->dwidget2_link=$request->fwidget2_link;
        $widget2->save();
        $notification = array(
            'message' => 'Widget2 Create Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Widget2  $widget2
     * @return \Illuminate\Http\Response
     */
    public function show(Widget2 $widget2)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Widget2  $widget2
     * @return \Illuminate\Http\Response
     */
    public function edit(Widget2 $widget2)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Widget2  $widget2
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)


    {

        $widget2=Widget2::find($id);
        $widget2->dwidget2_text=$request->fwidget2_text;
        $widget2->dwidget2_link=$request->fwidget2_link;
        $widget2->save();
        $notification = array(
            'message' => 'Updated Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Widget2  $widget2
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Widget2::destroy($id);
       $notification = array(
            'message' => 'Content Delete Successfully',
            'alert-type' => 'success'
        );
        return back()->with($notification);

    }
}
