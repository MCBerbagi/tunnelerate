<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\TestimonialContent;
use App\Models\Profile;
use Illuminate\Http\Request;

class TestimonialContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $testimonialcontent=TestimonialContent::first();
        $profile=Profile::first();
        return view('admin.testimonial.testimonialcontent', compact('testimonialcontent','profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)

    {
        $validatedData = $request->validate([

            'dtmcon_sec_title' =>'nullable',
            'dtmcon_sec_title_2' =>'nullable',
        ]);


        $testimonialcontent=new TestimonialContent();
        $testimonialcontent->dtmcon_sec_title=$request->ftmcon_sec_title;
        $testimonialcontent->dtmcon_sec_title_2=$request->ftmcon_sec_title_2;
        $testimonialcontent->save();
        $notification = array(
            'message' => 'TestimonialContent Create Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\TestimonialContent  $testimonialcontent
     * @return \Illuminate\Http\Response
     */
    public function show(TestimonialContent $testimonialcontent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TestimonialContent  $testimonialcontent
     * @return \Illuminate\Http\Response
     */
    public function edit(TestimonialContent $testimonialcontent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TestimonialContent  $testimonialcontent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)


    {
        $validatedData = $request->validate([

            'dtmcon_sec_title' =>'nullable',
            'dtmcon_sec_title_2' =>'nullable',
        ]);

        $testimonialcontent=TestimonialContent::find($id);
        $testimonialcontent->dtmcon_sec_title=$request->ftmcon_sec_title;
        $testimonialcontent->dtmcon_sec_title_2=$request->ftmcon_sec_title_2;
        $testimonialcontent->save();
        $notification = array(
            'message' => 'Updated Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TestimonialContent  $testimonialcontent
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        TestimonialContent::destroy($id);
       $notification = array(
            'message' => 'Content Delete Successfully',
            'alert-type' => 'success'
        );
        return back()->with($notification);

    }
}
