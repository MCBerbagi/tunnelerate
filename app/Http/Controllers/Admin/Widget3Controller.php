<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\Widget3;
use App\Models\Profile;
use Illuminate\Http\Request;

class Widget3Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $widget3=Widget3::all();
        $profile=Profile::first();
        return view('admin.footer.widget3', compact('widget3','profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)

    {
        $validatedData = $request->validate([
            'fwidget3_icon_text' =>'required',
            'fwidget3_info' =>'nullable',
        ]);


        $widget3=new Widget3();
        $widget3->dwidget3_icon_text=$request->fwidget3_icon_text;
        $widget3->dwidget3_info=$request->fwidget3_info;
        $widget3->save();
        $notification = array(
            'message' => 'Widget3 Create Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Widget3  $widget3
     * @return \Illuminate\Http\Response
     */
    public function show(Widget3 $widget3)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Widget3  $widget3
     * @return \Illuminate\Http\Response
     */
    public function edit(Widget3 $widget3)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Widget3  $widget3
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)


    {
        $validatedData = $request->validate([
            'fwidget3_icon_text' =>'required',
            'fwidget3_info' =>'nullable',
        ]);

        $widget3=Widget3::find($id);
        $widget3->dwidget3_icon_text=$request->fwidget3_icon_text;
        $widget3->dwidget3_info=$request->fwidget3_info;
        $widget3->save();
        $notification = array(
            'message' => 'Updated Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Widget3  $widget3
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Widget3::destroy($id);
       $notification = array(
            'message' => 'Content Delete Successfully',
            'alert-type' => 'success'
        );
        return back()->with($notification);

    }
}
