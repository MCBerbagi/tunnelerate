<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\Cta;
use App\Models\Profile;
use Illuminate\Http\Request;

class CtaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $cta=Cta::first();
        $profile=Profile::first();
        return view('admin.cta.cta', compact('cta','profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)

    {
        $validatedData = $request->validate([

            'fcta_big_image' =>'required|image|mimes:png,jpeg,jpg',
            'fcta_title' =>'required',
            'fcta_title_2' =>'nullable',
            'fcta_btn' =>'nullable',
            'fcta_url' =>'nullable',
        ]);


        //Cta Big Image

        if ($request->fcta_big_image) {

            $imageName = time().'1.'.request()->fcta_big_image->getClientOriginalExtension();
            request()->fcta_big_image->move('assets/img/cta/',$imageName);
            $fcta_big_image='assets/img/cta/'.$imageName;
        }

        $cta=new Cta();
        $cta->dcta_title=$request->fcta_title;
        $cta->dcta_title_2=$request->fcta_title_2;
        $cta->dcta_btn=$request->fcta_btn;
        $cta->dcta_url=$request->fcta_url;
        $cta->dcta_big_image=$fcta_big_image;
        $cta->save();
        $notification = array(
            'message' => 'Cta Create Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Cta  $cta
     * @return \Illuminate\Http\Response
     */
    public function show(Cta $cta)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cta  $cta
     * @return \Illuminate\Http\Response
     */
    public function edit(Cta $cta)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cta  $cta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)


    {
        $validatedData = $request->validate([
            'fcta_big_image' =>'image|mimes:png,jpeg,jpg',
            'fcta_title' =>'required',
            'fcta_title_2' =>'nullable',
            'fcta_btn' =>'nullable',
            'fcta_url' =>'nullable',
        ]);

        $cta=Cta::find($id);


        //Cta Big Image

        if ($request->hasFile('fcta_big_image')) {

            $imageName = time().'1.'.request()->fcta_big_image->getClientOriginalExtension();
            request()->fcta_big_image->move('assets/img/cta/',$imageName);
            $fcta_big_image='assets/img/cta/'.$imageName;

            if (file_exists($cta->dcta_big_image)) {
                unlink($cta->dcta_big_image);
                }
                $cta->dcta_big_image=$fcta_big_image;
        }

        $cta->dcta_title=$request->fcta_title;
        $cta->dcta_title_2=$request->fcta_title_2;
        $cta->dcta_btn=$request->fcta_btn;
        $cta->dcta_url=$request->fcta_url;
        $cta->save();
        $notification = array(
            'message' => 'Updated Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cta  $cta
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cta=Cta::find($id);
        if (file_exists($cta->dcta_big_image)) {
            unlink($cta->dcta_big_image);
        }

        Cta::destroy($id);
       $notification = array(
            'message' => 'Content Delete Successfully',
            'alert-type' => 'success'
        );
        return back()->with($notification);

    }
}
