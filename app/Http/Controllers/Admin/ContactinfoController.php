<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\Profile;
use App\Models\Contactinfo;
use Illuminate\Http\Request;

class ContactinfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contactinfo=Contactinfo::orderBy('id', 'asc')->latest()->get();
        $profile=Profile::first();
        return view('admin.contact.contactinfo', compact('contactinfo','profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)

    {
        $validatedData = $request->validate([
            'fci_icon' => 'required|image|mimes:png,jpeg,jpg',
            'fci_info' => 'required',
        ]);


        //Contactinfo icon

        if ($request->fci_icon) {

            $imageName = time().'.'.request()->fci_icon->getClientOriginalExtension();
            request()->fci_icon->move('assets/img/contact/',$imageName);
            $fci_icon='assets/img/contact/'.$imageName;
        }

        $contactinfo=new Contactinfo;
        $contactinfo->dci_icon=$fci_icon;
        $contactinfo->dci_info=$request->fci_info;
        $contactinfo->save();
        $notification = array(
            'message' => 'Contactinfo Created Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contactinfo  $contactinfo
     * @return \Illuminate\Http\Response
     */
    public function show(Contactinfo $contactinfo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contactinfo  $contactinfo
     * @return \Illuminate\Http\Response
     */
    public function edit(Contactinfo $contactinfo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contactinfo  $contactinfo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)

    {
        $validatedData = $request->validate([
            'fci_icon' => 'image|mimes:png,jpeg,jpg',
            'fci_info' => 'required',
        ]);

        $contactinfo=Contactinfo::find($id);

        //contactinfo Icon
        if ($request->hasFile('fci_icon')) {

            $imageName = time().'.'.request()->fci_icon->getClientOriginalExtension();
            request()->fci_icon->move('assets/img/contact/',$imageName);
            $fci_icon='assets/img/contact/'.$imageName;

            if (file_exists($contactinfo->dci_icon)) {
                unlink($contactinfo->dci_icon);
             }
             $contactinfo->dci_icon=$fci_icon;

        }

        $contactinfo->dci_info=$request->fci_info;
        $contactinfo->save();
        $notification = array(
            'message' => 'Contactinfo Updated Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contactinfo  $contactinfo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $info=Contactinfo::find($id);
        if (file_exists($info->dci_icon)) {
            unlink($info->dci_icon);
        }

        Contactinfo::destroy($id);
       $notification = array(
            'message' => 'Contactinfo Delete Successfully',
            'alert-type' => 'success'
        );
        return back()->with($notification);
    }

}
