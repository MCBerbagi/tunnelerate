<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\Profile;
use App\Models\communitypartner;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $client=communitypartner::latest()->get();
        $profile=Profile::first();
        return view('admin.client.client', compact('client','profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)

    {
        $validatedData = $request->validate([
            'fcli_link' => 'nullable',
            'fcli_image' => 'required|image|mimes:png,jpeg,jpg',
        ]);

        //communitypartner Style Image

        if ($request->fcli_image) {

            $imageName = time().'2.'.request()->fcli_image->getClientOriginalExtension();
            request()->fcli_image->move('assets/img/client/',$imageName);
            $fcli_image='assets/img/client/'.$imageName;
        }

        $client=new communitypartner;
        $client->dcli_link=$request->fcli_link;
        $client->dcli_image=$fcli_image;
        $client->save();
        $notification = array(
            'message' => 'communitypartner Created Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(communitypartner $client)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(communitypartner $client)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)

    {
        $validatedData = $request->validate([
            'fcli_link' => 'nullable',
            'fcli_image' => 'image|mimes:png,jpeg,jpg',
        ]);


        $client=communitypartner::find($id);

        //client Style Icon
        if ($request->hasFile('fcli_image')) {

            $imageName = time().'2.'.request()->fcli_image->getClientOriginalExtension();
            request()->fcli_image->move('assets/img/client/',$imageName);
            $fcli_image='assets/img/client/'.$imageName;

            if (file_exists($client->dcli_image)) {
                unlink($client->dcli_image);
             }
             $client->dcli_image=$fcli_image;

        }


        $client->dcli_link=$request->fcli_link;
        $client->save();
        $notification = array(
            'message' => 'communitypartner Updated Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $info=communitypartner::find($id);

        if (file_exists($info->dcli_image)) {
            unlink($info->dcli_image);
        }

        communitypartner::destroy($id);
       $notification = array(
            'message' => 'communitypartner Delete Successfully',
            'alert-type' => 'success'
        );
        return back()->with($notification);
    }

}
