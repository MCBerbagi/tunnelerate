<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\ServiceContent;
use App\Models\Profile;
use Illuminate\Http\Request;

class ServiceContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $servicecontent=ServiceContent::first();
        $profile=Profile::first();
        return view('admin.service.servicecontent', compact('servicecontent','profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)

    {
        $validatedData = $request->validate([

            'fserc_big_image' =>'required|image|mimes:png,jpeg,jpg',
            'dserc_sec_title_2' =>'nullable',
            'fserc_sec_title' =>'required',
        ]);


        //ServiceContent Big Image

        if ($request->fserc_big_image) {

            $imageName = time().'1.'.request()->fserc_big_image->getClientOriginalExtension();
            request()->fserc_big_image->move('assets/img/services/',$imageName);
            $fserc_big_image='assets/img/services/'.$imageName;
        }


        $servicecontent=new ServiceContent();
        $servicecontent->dserc_sec_title=$request->fserc_sec_title;
        $servicecontent->dserc_sec_title_2=$request->fserc_sec_title_2;
        $servicecontent->dserc_big_image=$fserc_big_image;
        $servicecontent->save();
        $notification = array(
            'message' => 'ServiceContent Create Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\ServiceContent  $servicecontent
     * @return \Illuminate\Http\Response
     */
    public function show(ServiceContent $servicecontent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ServiceContent  $servicecontent
     * @return \Illuminate\Http\Response
     */
    public function edit(ServiceContent $servicecontent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ServiceContent  $servicecontent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)


    {
        $validatedData = $request->validate([

            'fserc_big_image' =>'image|mimes:png,jpeg,jpg',
            'dserc_sec_title_2' =>'nullable',
            'fserc_sec_title' =>'required',
        ]);

        $servicecontent=ServiceContent::find($id);


        //ServiceContent Big Image

        if ($request->hasFile('fserc_big_image')) {

            $imageName = time().'1.'.request()->fserc_big_image->getClientOriginalExtension();
            request()->fserc_big_image->move('assets/img/services/',$imageName);
            $fserc_big_image='assets/img/services/'.$imageName;

            if (file_exists($servicecontent->dserc_big_image)) {
                unlink($servicecontent->dserc_big_image);
                }
                $servicecontent->dserc_big_image=$fserc_big_image;
        }


        $servicecontent->dserc_sec_title=$request->fserc_sec_title;
        $servicecontent->dserc_sec_title_2=$request->fserc_sec_title_2;
        $servicecontent->save();
        $notification = array(
            'message' => 'Updated Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ServiceContent  $servicecontent
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $servicecontent=ServiceContent::find($id);

        if (file_exists($servicecontent->dserc_big_image)) {
            unlink($servicecontent->dserc_big_image);
        }

        ServiceContent::destroy($id);
       $notification = array(
            'message' => 'Content Delete Successfully',
            'alert-type' => 'success'
        );
        return back()->with($notification);

    }
}
