<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\About;
use App\Models\Profile;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $about=About::first();
        $profile=Profile::first();
        return view('admin.about.about', compact('about','profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)

    {
        $validatedData = $request->validate([

            'fab_sec_title' =>'required',
            'fab_sec_title_2' =>'nullable',
            'fab_srt_paragraph' =>'nullable',
            'fab_long_paragraph' =>'nullable',
            'fab_list_info' =>'nullable',
            'fab_btn' =>'nullable',
            'fab_url' =>'nullable',
            'fab_big_image' =>'required|image|mimes:png,jpeg,jpg',
            'fab_brand_image' =>'nullable|image|mimes:png,jpeg,jpg',
        ]);


        $about=new About();

        //About Big Image

        if ($request->fab_big_image) {

            $imageName = time().'1.'.request()->fab_big_image->getClientOriginalExtension();
            request()->fab_big_image->move('assets/img/about/',$imageName);
            $fab_big_image='assets/img/about/'.$imageName;
        }

        //About Icon Image

        if ($request->fab_brand_image) {

            $imageName = time().'2.'.request()->fab_brand_image->getClientOriginalExtension();
            request()->fab_brand_image->move('assets/img/about/',$imageName);
            $fab_brand_image='assets/img/about/'.$imageName;
            $about->dab_brand_image=$fab_brand_image;
        }


        $about->dab_sec_title=$request->fab_sec_title;
        $about->dab_sec_title_2=$request->fab_sec_title_2;
        $about->dab_srt_paragraph=$request->fab_srt_paragraph;
        $about->dab_long_paragraph=$request->fab_long_paragraph;
        $about->dab_list_info=$request->fab_list_info;
        $about->dab_btn=$request->fab_btn;
        $about->dab_url=$request->fab_url;
        $about->dab_big_image=$fab_big_image;
        $about->save();
        $notification = array(
            'message' => 'About Create Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\About  $about
     * @return \Illuminate\Http\Response
     */
    public function show(About $about)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\About  $about
     * @return \Illuminate\Http\Response
     */
    public function edit(About $about)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\About  $about
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)


    {
        $validatedData = $request->validate([

            'fab_sec_title' =>'required',
            'fab_sec_title_2' =>'nullable',
            'fab_srt_paragraph' =>'nullable',
            'fab_long_paragraph' =>'nullable',
            'fab_list_info' =>'nullable',
            'fab_btn' =>'nullable',
            'fab_url' =>'nullable',
            'fab_big_image' =>'image|mimes:png,jpeg,jpg',
            'fab_brand_image' =>'image|mimes:png,jpeg,jpg',
        ]);

        $about=About::find($id);


        //About Big Image

        if ($request->hasFile('fab_big_image')) {

            $imageName = time().'1.'.request()->fab_big_image->getClientOriginalExtension();
            request()->fab_big_image->move('assets/img/about/',$imageName);
            $fab_big_image='assets/img/about/'.$imageName;

            if (file_exists($about->dab_big_image)) {
                unlink($about->dab_big_image);
                }
                $about->dab_big_image=$fab_big_image;
        }

        //About Icon Image

        if ($request->hasFile('fab_brand_image')) {

            $imageName = time().'2.'.request()->fab_brand_image->getClientOriginalExtension();
            request()->fab_brand_image->move('assets/img/about/',$imageName);
            $fab_brand_image='assets/img/about/'.$imageName;

            if (file_exists($about->dab_brand_image)) {
                unlink($about->dab_brand_image);
                }
                $about->dab_brand_image=$fab_brand_image;
        }


        $about->dab_sec_title=$request->fab_sec_title;
        $about->dab_sec_title_2=$request->fab_sec_title_2;
        $about->dab_srt_paragraph=$request->fab_srt_paragraph;
        $about->dab_long_paragraph=$request->fab_long_paragraph;
        $about->dab_list_info=$request->fab_list_info;
        $about->dab_btn=$request->fab_btn;
        $about->dab_url=$request->fab_url;
        $about->save();
        $notification = array(
            'message' => 'Updated Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\About  $about
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $about=About::find($id);

        if (file_exists($about->dab_big_image)) {
            unlink($about->dab_big_image);
        }

        if (file_exists($about->dab_brand_image)) {
            unlink($about->dab_brand_image);
        }

        About::destroy($id);
       $notification = array(
            'message' => 'Content Delete Successfully',
            'alert-type' => 'success'
        );
        return back()->with($notification);

    }
}
