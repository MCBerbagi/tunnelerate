<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\General;
use App\Models\Profile;
use Illuminate\Http\Request;
class GeneralController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $general=General::first();
        $profile=Profile::first();
        return view('admin.general.general', compact('general','profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'ftop_logo' => 'nullable|image|mimes:png,jpeg,jpg',
            'ffavicon_icon' => 'nullable|image|mimes:png,jpeg,jpg',
            'fbreadcrumb_bg' => 'nullable|image|mimes:png,jpeg,jpg',
            'fsite_title' => 'nullable',
            'fmap_api_key' => 'nullable',
            'ffooter_copyright' => 'nullable',

        ]);

        $general=new General();

        //Logo

        if ($request->ftop_logo) {

            $imageName = time().'.'.request()->ftop_logo->getClientOriginalExtension();
            request()->ftop_logo->move('assets/img/',$imageName);
            $ftop_logo='assets/img/'.$imageName;
            $general->dtop_logo=$ftop_logo;

        }

        //Favicon

        if ($request->ffavicon_icon) {

            $imageName = time().'2.'.request()->ffavicon_icon->getClientOriginalExtension();
            request()->ffavicon_icon->move('assets/img/',$imageName);
            $ffavicon_icon='assets/img/'.$imageName;
            $general->dfavicon_icon=$ffavicon_icon;

        }

        //Fbreadcrumb

        if ($request->fbreadcrumb_bg) {

            $imageName = time().'3.'.request()->fbreadcrumb_bg->getClientOriginalExtension();
            request()->fbreadcrumb_bg->move('assets/img/',$imageName);
            $fbreadcrumb_bg='assets/img/'.$imageName;
            $general->dbreadcrumb_bg=$fbreadcrumb_bg;

        }

        $general->dsite_title=$request->fsite_title;
        $general->dmap_api_key=$request->fmap_api_key;
        $general->dfooter_copyright=$request->ffooter_copyright;
        $general->save();
        $notification = array(
            'message' => 'Content Create Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\General  $general
     * @return \Illuminate\Http\Response
     */
    public function show(General $general)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\General  $general
     * @return \Illuminate\Http\Response
     */
    public function edit(General $general)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\General  $general
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)


    {
        $validatedData = $request->validate([
            'ftop_logo' => 'image|mimes:png,jpeg,jpg',
            'ffavicon_icon' => 'image|mimes:png,jpeg,jpg',
            'fbreadcrumb_bg' => 'image|mimes:png,jpeg,jpg',
            'fsite_title' => 'nullable',
            'fmap_api_key' => 'nullable',
            'ffooter_copyright' => 'nullable',
        ]);

        $general=General::find($id);


        //Logo

        if ($request->hasFile('ftop_logo')) {

            $imageName = time().'.'.request()->ftop_logo->getClientOriginalExtension();
            request()->ftop_logo->move('assets/img/',$imageName);
            $ftop_logo='assets/img/'.$imageName;

            if (file_exists($general->dtop_logo)) {
                unlink($general->dtop_logo);
                }
                $general->dtop_logo=$ftop_logo;
        }

        //Favicon

        if ($request->hasFile('ffavicon_icon')) {

            $imageName = time().'2.'.request()->ffavicon_icon->getClientOriginalExtension();
            request()->ffavicon_icon->move('assets/img/',$imageName);
            $ffavicon_icon='assets/img/'.$imageName;

            if (file_exists($general->dfavicon_icon)) {
                unlink($general->dfavicon_icon);
                }
                $general->dfavicon_icon=$ffavicon_icon;
        }

        //Breadcrumb

        if ($request->hasFile('fbreadcrumb_bg')) {

            $imageName = time().'3.'.request()->fbreadcrumb_bg->getClientOriginalExtension();
            request()->fbreadcrumb_bg->move('assets/img/',$imageName);
            $fbreadcrumb_bg='assets/img/'.$imageName;

            if (file_exists($general->dbreadcrumb_bg)) {
                unlink($general->dbreadcrumb_bg);
                }
                $general->dbreadcrumb_bg=$fbreadcrumb_bg;
        }

        $general->dsite_title=$request->fsite_title;
        $general->dmap_api_key=$request->fmap_api_key;
        $general->dfooter_copyright=$request->ffooter_copyright;
        $general->save();
        $notification = array(
            'message' => 'Content Updated Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\General  $general
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        $info=General::find($id);

        //Logo
        if (file_exists($info->dtop_logo)) {
            unlink($info->dtop_logo);
        }

        //Favicon
        if (file_exists($info->dfavicon_icon)) {
            unlink($info->dfavicon_icon);
        }

        //Breadcrumb_bg
        if (file_exists($info->dbreadcrumb_bg)) {
            unlink($info->dbreadcrumb_bg);
        }

        General::destroy($id);
       $notification = array(
            'message' => 'Content Delete Successfully',
            'alert-type' => 'success'
        );
        return back()->with($notification);

    }


}
