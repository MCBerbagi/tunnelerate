<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\Team;
use App\Models\Profile;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $team=Team::orderBy('id', 'asc')->latest()->get();
        $profile=Profile::first();
        return view('admin.team.team', compact('team','profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'ftm_image' => 'required|image|mimes:png,jpeg,jpg',
            'ftm_title' => 'required',
            'ftm_designation' => 'nullable',
            'ftm_social_icon1' => 'nullable',
            'ftm_social_icon_url1' => 'nullable',
            'ftm_social_icon2' => 'nullable',
            'ftm_social_icon_url2' => 'nullable',
            'ftm_social_icon3' => 'nullable',
            'ftm_social_icon_url3' => 'nullable',
            'ftm_social_icon4' => 'nullable',
            'ftm_social_icon_url4' => 'nullable',
        ]);

        //Team Icon

        if ($request->ftm_image) {

            $imageName = time().'.'.request()->ftm_image->getClientOriginalExtension();
            request()->ftm_image->move('assets/img/team/',$imageName);
            $ftm_image='assets/img/team/'.$imageName;
        }

        $team=new Team();
        $team->dtm_designation=$request->ftm_designation;
        $team->dtm_title=$request->ftm_title;
        $team->dtm_social_icon1=$request->ftm_social_icon1;
        $team->dtm_social_icon_url1=$request->ftm_social_icon_url1;
        $team->dtm_social_icon2=$request->ftm_social_icon2;
        $team->dtm_social_icon_url2=$request->ftm_social_icon_url2;
        $team->dtm_social_icon3=$request->ftm_social_icon3;
        $team->dtm_social_icon_url3=$request->ftm_social_icon_url3;
        $team->dtm_social_icon4=$request->ftm_social_icon4;
        $team->dtm_social_icon_url4=$request->ftm_social_icon_url4;
        $team->dtm_image=$ftm_image;
        $team->save();
        $notification = array(
            'message' => 'Team Create Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function show(Team $team)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function edit(Team $team)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)

    {
        $validatedData = $request->validate([
            'ftm_image' => 'image|mimes:png,jpeg,jpg',
            'ftm_title' => 'required',
            'ftm_designation' => 'nullable',
            'ftm_social_icon1' => 'nullable',
            'ftm_social_icon_url1' => 'nullable',
            'ftm_social_icon2' => 'nullable',
            'ftm_social_icon_url2' => 'nullable',
            'ftm_social_icon3' => 'nullable',
            'ftm_social_icon_url3' => 'nullable',
            'ftm_social_icon4' => 'nullable',
            'ftm_social_icon_url4' => 'nullable',
        ]);

        $team=Team::find($id);

        //team Icon
        if ($request->hasFile('ftm_image')) {

            $imageName = time().'.'.request()->ftm_image->getClientOriginalExtension();
            request()->ftm_image->move('assets/img/team/',$imageName);
            $ftm_image='assets/img/team/'.$imageName;

            if (file_exists($team->dtm_image)) {
                unlink($team->dtm_image);
             }
             $team->dtm_image=$ftm_image;

        }

        $team->dtm_designation=$request->ftm_designation;
        $team->dtm_title=$request->ftm_title;
        $team->dtm_social_icon1=$request->ftm_social_icon1;
        $team->dtm_social_icon_url1=$request->ftm_social_icon_url1;
        $team->dtm_social_icon2=$request->ftm_social_icon2;
        $team->dtm_social_icon_url2=$request->ftm_social_icon_url2;
        $team->dtm_social_icon3=$request->ftm_social_icon3;
        $team->dtm_social_icon_url3=$request->ftm_social_icon_url3;
        $team->dtm_social_icon4=$request->ftm_social_icon4;
        $team->dtm_social_icon_url4=$request->ftm_social_icon_url4;
        $team->save();
        $notification = array(
            'message' => 'Team Updated Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $team=Team::find($id);
        if (file_exists($team->dtm_image)) {
            unlink($team->dtm_image);
        }

        Team::destroy($id);
       $notification = array(
            'message' => 'Team Delete Successfully',
            'alert-type' => 'success'
        );
        return back()->with($notification);
    }
}
