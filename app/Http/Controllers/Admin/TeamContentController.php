<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\TeamContent;
use App\Models\Profile;
use Illuminate\Http\Request;

class TeamContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $teamcontent=TeamContent::first();
        $profile=Profile::first();
        return view('admin.team.teamcontent', compact('teamcontent','profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)

    {
        $validatedData = $request->validate([

            'ftmcon_sec_title' =>'nullable',
            'dtmcon_sec_title_2' =>'nullable',
        ]);


        $teamcontent=new TeamContent();
        $teamcontent->dtmcon_sec_title=$request->ftmcon_sec_title;
        $teamcontent->dtmcon_sec_title_2=$request->ftmcon_sec_title_2;
        $teamcontent->save();
        $notification = array(
            'message' => 'TeamContent Create Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\TeamContent  $teamcontent
     * @return \Illuminate\Http\Response
     */
    public function show(TeamContent $teamcontent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TeamContent  $teamcontent
     * @return \Illuminate\Http\Response
     */
    public function edit(TeamContent $teamcontent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TeamContent  $teamcontent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)


    {
        $validatedData = $request->validate([

            'ftmcon_sec_title' =>'nullable',
            'dtmcon_sec_title_2' =>'nullable',
        ]);


        $teamcontent=TeamContent::find($id);
        $teamcontent->dtmcon_sec_title=$request->ftmcon_sec_title;
        $teamcontent->dtmcon_sec_title_2=$request->ftmcon_sec_title_2;
        $teamcontent->save();
        $notification = array(
            'message' => 'Updated Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TeamContent  $teamcontent
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        TeamContent::destroy($id);
       $notification = array(
            'message' => 'Content Delete Successfully',
            'alert-type' => 'success'
        );
        return back()->with($notification);

    }
}
