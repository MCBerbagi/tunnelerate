<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\Video;
use App\Models\Profile;
use Illuminate\Http\Request;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $video=Video::first();
        $profile=Profile::first();
        return view('admin.video.video', compact('video','profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)

    {
        $validatedData = $request->validate([
            'fv_image' =>'required|image|mimes:png,jpeg,jpg',
            'fv_url' =>'nullable',
        ]);


        //Video Big Image

        if ($request->fv_image) {

            $imageName = time().'.'.request()->fv_image->getClientOriginalExtension();
            request()->fv_image->move('assets/img/video/',$imageName);
            $fv_image='assets/img/video/'.$imageName;
        }

        $video=new Video();
        $video->dv_url=$request->fv_url;
        $video->dv_image=$fv_image;
        $video->save();
        $notification = array(
            'message' => 'Video Create Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function show(Video $video)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function edit(Video $video)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)


    {
        $validatedData = $request->validate([
            'fv_image' =>'image|mimes:png,jpeg,jpg',
            'fv_url' =>'nullable',
        ]);

        $video=Video::find($id);


        //Video Big Image

        if ($request->hasFile('fv_image')) {

            $imageName = time().'.'.request()->fv_image->getClientOriginalExtension();
            request()->fv_image->move('assets/img/video/',$imageName);
            $fv_image='assets/img/video/'.$imageName;

            if (file_exists($video->dv_image)) {
                unlink($video->dv_image);
                }
                $video->dv_image=$fv_image;
        }

        $video->dv_url=$request->fv_url;
        $video->save();
        $notification = array(
            'message' => 'Updated Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $info=Video::find($id);

        if (file_exists($info->dv_image)) {
            unlink($info->dv_image);
        }

        Video::destroy($id);
       $notification = array(
            'message' => 'Content Delete Successfully',
            'alert-type' => 'success'
        );
        return back()->with($notification);

    }
}
