<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\Widget1;
use App\Models\Profile;
use Illuminate\Http\Request;

class Widget1Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $widget1=Widget1::first();
        $profile=Profile::first();
        return view('admin.footer.widget1', compact('widget1','profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)

    {
        $validatedData = $request->validate([

            'ffw1_logo_image' =>'required|image|mimes:png,jpeg,jpg',
            'ffw1_paragraph' =>'nullable',
            'ffw1_social_icon_1' =>'nullable',
            'ffw1_social_icon_1_link' =>'nullable',
            'ffw1_social_icon_2' =>'nullable',
            'ffw1_social_icon_2_link' =>'nullable',
            'ffw1_social_icon_3' =>'nullable',
            'ffw1_social_icon_3_link' =>'nullable',
            'ffw1_social_icon_4' =>'nullable',
            'ffw1_social_icon_4_link' =>'nullable',
        ]);


        //Widget1 Big Image

        if ($request->ffw1_logo_image) {

            $imageName = time().'.'.request()->ffw1_logo_image->getClientOriginalExtension();
            request()->ffw1_logo_image->move('assets/img/',$imageName);
            $ffw1_logo_image='assets/img/'.$imageName;
        }

        $widget1=new Widget1();
        $widget1->dfw1_logo_image=$ffw1_logo_image;
        $widget1->dfw1_paragraph=$request->ffw1_paragraph;
        $widget1->dfw1_social_icon_1=$request->ffw1_social_icon_1;
        $widget1->dfw1_social_icon_1_link=$request->ffw1_social_icon_1_link;
        $widget1->dfw1_social_icon_2=$request->ffw1_social_icon_2;
        $widget1->dfw1_social_icon_2_link=$request->ffw1_social_icon_2_link;
        $widget1->dfw1_social_icon_3=$request->ffw1_social_icon_3;
        $widget1->dfw1_social_icon_3_link=$request->ffw1_social_icon_3_link;
        $widget1->dfw1_social_icon_4=$request->ffw1_social_icon_4;
        $widget1->dfw1_social_icon_4_link=$request->ffw1_social_icon_4_link;
        $widget1->save();
        $notification = array(
            'message' => 'Widget1 Create Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Widget1  $widget1
     * @return \Illuminate\Http\Response
     */
    public function show(Widget1 $widget1)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Widget1  $widget1
     * @return \Illuminate\Http\Response
     */
    public function edit(Widget1 $widget1)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Widget1  $widget1
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)


    {
        $validatedData = $request->validate([

            'ffw1_logo_image' =>'image|mimes:png,jpeg,jpg',
            'ffw1_paragraph' =>'nullable',
            'ffw1_social_icon_1' =>'nullable',
            'ffw1_social_icon_1_link' =>'nullable',
            'ffw1_social_icon_2' =>'nullable',
            'ffw1_social_icon_2_link' =>'nullable',
            'ffw1_social_icon_3' =>'nullable',
            'ffw1_social_icon_3_link' =>'nullable',
            'ffw1_social_icon_4' =>'nullable',
            'ffw1_social_icon_4_link' =>'nullable',
        ]);

        $widget1=Widget1::find($id);


        //Widget1 Big Image

        if ($request->hasFile('ffw1_logo_image')) {

            $imageName = time().'.'.request()->ffw1_logo_image->getClientOriginalExtension();
            request()->ffw1_logo_image->move('assets/img/',$imageName);
            $ffw1_logo_image='assets/img/'.$imageName;

            if (file_exists($widget1->dfw1_logo_image)) {
                unlink($widget1->dfw1_logo_image);
                }
                $widget1->dfw1_logo_image=$ffw1_logo_image;
        }

        $widget1->dfw1_paragraph=$request->ffw1_paragraph;
        $widget1->dfw1_social_icon_1=$request->ffw1_social_icon_1;
        $widget1->dfw1_social_icon_1_link=$request->ffw1_social_icon_1_link;
        $widget1->dfw1_social_icon_2=$request->ffw1_social_icon_2;
        $widget1->dfw1_social_icon_2_link=$request->ffw1_social_icon_2_link;
        $widget1->dfw1_social_icon_3=$request->ffw1_social_icon_3;
        $widget1->dfw1_social_icon_3_link=$request->ffw1_social_icon_3_link;
        $widget1->dfw1_social_icon_4=$request->ffw1_social_icon_4;
        $widget1->dfw1_social_icon_4_link=$request->ffw1_social_icon_4_link;
        $widget1->save();
        $notification = array(
            'message' => 'Updated Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Widget1  $widget1
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $info=Widget1::find($id);

        if (file_exists($info->dfw1_logo_image)) {
            unlink($info->dfw1_logo_image);
        }

        Widget1::destroy($id);
       $notification = array(
            'message' => 'Content Delete Successfully',
            'alert-type' => 'success'
        );
        return back()->with($notification);

    }
}
