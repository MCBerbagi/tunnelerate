<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\Profile;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $profile=Profile::first();
        return view('admin.profile.profile', compact('profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)

    {
        $validatedData = $request->validate([
            'fpr_title'=> 'nullable',
            'fpr_mobile'=> 'nullable',
            'fpr_email'=> 'nullable',
            'fpr_content'=> 'required',
            'fpr_image'=> 'required|image|mimes:png,jpeg,jpg',
            'fpr_social1'=> 'nullable',
            'fpr_social2'=> 'nullable',

        ]);


        //logo

        if ($request->fpr_image) {

            $imageName = time().'.'.request()->fpr_image->getClientOriginalExtension();
            request()->fpr_image->move('assets/img/profile/',$imageName);
            $fpr_image='assets/img/profile/'.$imageName;


        }


        $profile=new Profile();
        $profile->dpr_title=$request->fpr_title;
        $profile->dpr_mobile=$request->fpr_mobile;
        $profile->dpr_email=$request->fpr_email;
        $profile->dpr_content=$request->fpr_content;
        $profile->dpr_image=$request->fpr_image;
        $profile->dpr_social1=$request->fpr_social1;
        $profile->dpr_social2=$request->fpr_social2;
        $profile->dpr_image=$fpr_image;
        $profile->save();
        $notification = array(
            'message' => 'Create Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function show(Profile $profile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function edit(Profile $profile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)


    {
        $validatedData = $request->validate([
            'fpr_title'=> 'nullable',
            'fpr_mobile'=> 'nullable',
            'fpr_email'=> 'nullable',
            'fpr_content'=> 'required',
            'fpr_image'=> 'image|mimes:png,jpeg,jpg',
            'fpr_social1'=> 'nullable',
            'fpr_social2'=> 'nullable',

        ]);

        $profile=Profile::find($id);

        //logo
                if ($request->hasFile('fpr_image')) {

                    $imageName = time().'.'.request()->fpr_image->getClientOriginalExtension();
                    request()->fpr_image->move('assets/img/profile/',$imageName);
                    $fpr_image='assets/img/profile/'.$imageName;

                    if (file_exists($profile->dpr_image)) {
                        unlink($profile->dpr_image);
                     }
                     $profile->dpr_image=$fpr_image;

                }

        $profile->dpr_title=$request->fpr_title;
        $profile->dpr_mobile=$request->fpr_mobile;
        $profile->dpr_email=$request->fpr_email;
        $profile->dpr_content=$request->fpr_content;
        $profile->dpr_social1=$request->fpr_social1;
        $profile->dpr_social2=$request->fpr_social2;
        $profile->save();
        $notification = array(
            'message' => 'Updated Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
   public function destroy($id)
    {
        $info=Profile::find($id);

        if (file_exists($info->dpr_image)) {
            unlink($info->dpr_image);
        }

        Profile::destroy($id);
       $notification = array(
            'message' => 'Content Delete Successfully',
            'alert-type' => 'success'
        );
        return back()->with($notification);

    }
}
