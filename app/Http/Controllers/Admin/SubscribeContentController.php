<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\SubscribeContent;
use App\Models\Profile;
use Illuminate\Http\Request;

class SubscribeContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $subscribecontent=SubscribeContent::first();
        $profile=Profile::first();
        return view('admin.subscribe.subscribecontent', compact('subscribecontent','profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)

    {
        $validatedData = $request->validate([
            'fsubscribeco_title' =>'nullable',
            'fsubscribeco_image' =>'required|image|mimes:png,jpeg,jpg',
            'fsubscribeco_btn_title' =>'nullable',
        ]);


        //SubscribeContent Big Image

        if ($request->fsubscribeco_image) {

            $imageName = time().'1.'.request()->fsubscribeco_image->getClientOriginalExtension();
            request()->fsubscribeco_image->move('assets/img/subscribe/',$imageName);
            $fsubscribeco_image='assets/img/subscribe/'.$imageName;
        }


        $subscribecontent=new SubscribeContent();
        $subscribecontent->dsubscribeco_title=$request->fsubscribeco_title;
        $subscribecontent->dsubscribeco_image=$fsubscribeco_image;
        $subscribecontent->dsubscribeco_btn_title=$request->fsubscribeco_btn_title;

        $subscribecontent->save();
        $notification = array(
            'message' => 'SubscribeContent Create Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\SubscribeContent  $subscribecontent
     * @return \Illuminate\Http\Response
     */
    public function show(SubscribeContent $subscribecontent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SubscribeContent  $subscribecontent
     * @return \Illuminate\Http\Response
     */
    public function edit(SubscribeContent $subscribecontent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SubscribeContent  $subscribecontent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)


    {
        $validatedData = $request->validate([
            'fsubscribeco_title' =>'nullable',
            'fsubscribeco_image' =>'image|mimes:png,jpeg,jpg',
            'fsubscribeco_btn_title' =>'nullable',
        ]);

        $subscribecontent=SubscribeContent::find($id);


        //SubscribeContent Big Image

        if ($request->hasFile('fsubscribeco_image')) {

            $imageName = time().'1.'.request()->fsubscribeco_image->getClientOriginalExtension();
            request()->fsubscribeco_image->move('assets/img/subscribe/',$imageName);
            $fsubscribeco_image='assets/img/subscribe/'.$imageName;

            if (file_exists($subscribecontent->dsubscribeco_image)) {
                unlink($subscribecontent->dsubscribeco_image);
                }
                $subscribecontent->dsubscribeco_image=$fsubscribeco_image;
        }


        $subscribecontent->dsubscribeco_title=$request->fsubscribeco_title;
        $subscribecontent->dsubscribeco_btn_title=$request->fsubscribeco_btn_title;
        $subscribecontent->save();
        $notification = array(
            'message' => 'Updated Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SubscribeContent  $subscribecontent
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subscribecontent=SubscribeContent::find($id);

        if (file_exists($subscribecontent->dsubscribeco_image)) {
            unlink($subscribecontent->dsubscribeco_image);
        }

        SubscribeContent::destroy($id);
       $notification = array(
            'message' => 'Content Delete Successfully',
            'alert-type' => 'success'
        );
        return back()->with($notification);

    }
}
