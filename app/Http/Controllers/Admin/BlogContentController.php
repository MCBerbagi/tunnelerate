<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\BlogContent;
use App\Models\Profile;
use Illuminate\Http\Request;

class BlogContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $blogcontent=BlogContent::first();
        $profile=Profile::first();
        return view('admin.blog.blogcontent', compact('blogcontent','profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)

    {
        $validatedData = $request->validate([

            'ftmcon_sec_title' =>'required',
        ]);


        $blogcontent=new BlogContent();
        $blogcontent->dtmcon_sec_title=$request->ftmcon_sec_title;
        $blogcontent->dtmcon_sec_title_2=$request->ftmcon_sec_title_2;
        $blogcontent->save();
        $notification = array(
            'message' => 'BlogContent Create Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\BlogContent  $blogcontent
     * @return \Illuminate\Http\Response
     */
    public function show(BlogContent $blogcontent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BlogContent  $blogcontent
     * @return \Illuminate\Http\Response
     */
    public function edit(BlogContent $blogcontent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BlogContent  $blogcontent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)


    {

        $blogcontent=BlogContent::find($id);
        $blogcontent->dtmcon_sec_title=$request->ftmcon_sec_title;
        $blogcontent->dtmcon_sec_title_2=$request->ftmcon_sec_title_2;
        $blogcontent->save();
        $notification = array(
            'message' => 'Updated Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BlogContent  $blogcontent
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        BlogContent::destroy($id);
       $notification = array(
            'message' => 'Content Delete Successfully',
            'alert-type' => 'success'
        );
        return back()->with($notification);

    }
}
