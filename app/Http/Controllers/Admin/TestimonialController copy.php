<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\Profile;
use App\Models\Testimonial;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;

class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $testimonial=Testimonial::latest()->get();

        $profile=Profile::first();
        return view('admin.testimonial.testimonial', compact('testimonial','profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(Request $request)

    {
        $validatedData = $request->validate([
            'fts_image' => 'sometimes|nullable|image',
            'fts_title' => 'required',
        ]);


        $testimonial = $request->all();

        //Testimonial Image
        if($request->hasFile('fts_image')){

            // Get image file
            $fts_image = $request->file('fts_image');

            // Folder path
            $folder ='assets/img/testimonial/';

            // Make image name
            $image_name =  time().'-'.$fts_image->getClientOriginalName();

            // Upload image
            $fts_image->move($folder, $image_name);

            // Set input
            $testimonial['fts_image']= $image_name;

        }  else {
            // Set input
            $testimonial['fts_image']= null;
        }

        Testimonial::firstOrCreate([
            'dts_title' => $testimonial['fts_title'],
            'dts_desig' => $testimonial['fts_desig'],
            'dts_paragraph' => $testimonial['fts_paragraph'],
            'dts_image' => $testimonial['fts_image']
        ]);
        $notification = array(
            'message' => 'Testimonial Created Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }





    /**
     * Display the specified resource.
     *
     * @param  \App\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function show(Testimonial $testimonial)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function edit(Testimonial $testimonial)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)

    {
        $validatedData = $request->validate([
            'fts_image' => 'image|mimes:jpeg,jpg,png|',
        ]);


        $testimonial=Testimonial::find($id);

            // Get All Request
            $input = $request->all();

            if($request->hasFile('fts_image')){

                // Get image file
                $fts_image = $request->file('fts_image');

                // Folder path FileStorage
                $folder = 'assets/img/testimonial/';

                // Make image name
                $image_name =  time().'-'.$fts_image->getClientOriginalName();

                // Delete Image
                File::delete(public_path($folder.$testimonial->fts_image));

                // Original size upload file
                $fts_image->move($folder, $image_name);

                // Set input FileStorage
                $input['fts_image']= $image_name;

            }


        // Update to database
        $testimonial->save();

        $notification = array(
            'message' => 'Testimonial Updated Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $info=Testimonial::find($id);

        if (file_exists($info->dts_image)) {
            unlink($info->dts_image);
        }

        Testimonial::destroy($id);
       $notification = array(
            'message' => 'Testimonial Delete Successfully',
            'alert-type' => 'success'
        );
        return back()->with($notification);
    }

}
