<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\Profile;
use App\Models\Testimonial;
use Illuminate\Http\Request;

class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $testimonial=Testimonial::orderBy('id', 'asc')->latest()->get();
        $profile=Profile::first();
        return view('admin.testimonial.testimonial', compact('testimonial','profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)

    {
        $validatedData = $request->validate([
            'fts_title' => 'required',
            'fts_desig' => 'nullable',
            'fts_paragraph' => 'nullable',
            'fts_image' => 'required|image|mimes:png,jpeg,jpg',
        ]);


        $testimonial=new Testimonial;

        //Testimonial Image

            if ($request->hasFile('fts_image')) {

            $imageName = time().'2.'.request()->fts_image->getClientOriginalExtension();
            request()->fts_image->move('assets/img/testimonial/',$imageName);
            $fts_image='assets/img/testimonial/'.$imageName;
            $testimonial->dts_image=$fts_image;
        }

        $testimonial->dts_title=$request->fts_title;
        $testimonial->dts_desig=$request->fts_desig;
        $testimonial->dts_paragraph=$request->fts_paragraph;
        $testimonial->save();
        $notification = array(
            'message' => 'Testimonial Created Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function show(Testimonial $testimonial)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function edit(Testimonial $testimonial)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)

    {
        $validatedData = $request->validate([
            'fts_title' => 'required',
            'fts_desig' => 'nullable',
            'fts_paragraph' => 'nullable',
            'fts_image' => 'image|mimes:png,jpeg,jpg',
        ]);


        $testimonial=Testimonial::find($id);


        //testimonial Image

        if ($request->hasFile('fts_image')) {

            $imageName = time().'2.'.request()->fts_image->getClientOriginalExtension();
            request()->fts_image->move('assets/img/testimonial/',$imageName);
            $fts_image='assets/img/testimonial/'.$imageName;

            if (file_exists($testimonial->dts_image)) {
                unlink($testimonial->dts_image);
             }
             $testimonial->dts_image=$fts_image;
        }


        $testimonial->dts_title=$request->fts_title;
        $testimonial->dts_desig=$request->fts_desig;
        $testimonial->dts_paragraph=$request->fts_paragraph;
        $testimonial->save();
        $notification = array(
            'message' => 'Testimonial Updated Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $info=Testimonial::find($id);

        if (file_exists($info->dts_image)) {
            unlink($info->dts_image);
        }

        Testimonial::destroy($id);
       $notification = array(
            'message' => 'Testimonial Delete Successfully',
            'alert-type' => 'success'
        );
        return back()->with($notification);
    }

}
