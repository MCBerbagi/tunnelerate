<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\Service;
use App\Models\Profile;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $service=Service::all();
        $profile=Profile::first();
        return view('admin.service.service', compact('service','profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'fser_image'=>'required|image|mimes:png,jpeg,jpg',
            'fser_title' => 'required',
            'fser_paragraph' => 'nullable',
        ]);

        //Service Icon

        if ($request->fser_image) {

            $imageName = time().'.'.request()->fser_image->getClientOriginalExtension();
            request()->fser_image->move('assets/img/services/',$imageName);
            $fser_image='assets/img/services/'.$imageName;
        }

        $service=new Service();
        $service->dser_paragraph=$request->fser_paragraph;
        $service->dser_title=$request->fser_title;
        $service->dser_image=$fser_image;
        $service->save();
        $notification = array(
            'message' => 'Service Create Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)

    {
        $validatedData = $request->validate([
            'fser_image'=>'image|mimes:png,jpeg,jpg',
            'fser_title' => 'required',
            'fser_paragraph' => 'nullable',
        ]);

        $service=Service::find($id);

        //service Icon
        if ($request->hasFile('fser_image')) {

            $imageName = time().'.'.request()->fser_image->getClientOriginalExtension();
            request()->fser_image->move('assets/img/services/',$imageName);
            $fser_image='assets/img/services/'.$imageName;

            if (file_exists($service->dser_image)) {
                unlink($service->dser_image);
             }
             $service->dser_image=$fser_image;

        }

        $service->dser_paragraph=$request->fser_paragraph;
        $service->dser_title=$request->fser_title;
        $service->save();
        $notification = array(
            'message' => 'Service Updated Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $service=Service::find($id);
        if (file_exists($service->dser_image)) {
            unlink($service->dser_image);
        }

        Service::destroy($id);
       $notification = array(
            'message' => 'Service Delete Successfully',
            'alert-type' => 'success'
        );
        return back()->with($notification);
    }
}
