<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\Profile;
use App\Models\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blog=Blog::orderBy('id', 'asc')->latest()->get();
        $profile=Profile::first();
        return view('admin.blog.blog', compact('blog','profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)

    {
        $validatedData = $request->validate([
            'fblog_title' => 'required',
            'fblog_paragraph' => 'required',
            'fblog_long_content' => 'required',
            'fblog_tags' => 'required',
            'fblog_image' => 'required|image|mimes:png,jpeg,jpg',
        ]);

        //Blog Style Image

        if ($request->fblog_image) {

            $imageName = time().'2.'.request()->fblog_image->getClientOriginalExtension();
            request()->fblog_image->move('assets/img/blog/',$imageName);
            $fblog_image='assets/img/blog/'.$imageName;
        }

        $blog=new Blog;
        $blog->dblog_title=$request->fblog_title;
        $blog->dblog_paragraph=$request->fblog_paragraph;
        $blog->dblog_long_content=$request->fblog_long_content;
        $blog->dblog_tags=$request->fblog_tags;
        $blog->dblog_image=$fblog_image;
        $blog->save();
        $notification = array(
            'message' => 'Blog Created Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)

    {
        $validatedData = $request->validate([
            'fblog_image' => 'image|mimes:png,jpeg,jpg',
        ]);


        $blog=Blog::find($id);

        //blog Style Icon
        if ($request->hasFile('fblog_image')) {

            $imageName = time().'2.'.request()->fblog_image->getClientOriginalExtension();
            request()->fblog_image->move('assets/img/blog/',$imageName);
            $fblog_image='assets/img/blog/'.$imageName;

            if (file_exists($blog->dblog_image)) {
                unlink($blog->dblog_image);
             }
             $blog->dblog_image=$fblog_image;

        }


        $blog->dblog_title=$request->fblog_title;
        $blog->dblog_paragraph=$request->fblog_paragraph;
        $blog->dblog_long_content=$request->fblog_long_content;
        $blog->dblog_tags=$request->fblog_tags;
        $blog->save();
        $notification = array(
            'message' => 'Blog Updated Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $info=Blog::find($id);

        if (file_exists($info->dblog_image)) {
            unlink($info->dblog_image);
        }

        Blog::destroy($id);
       $notification = array(
            'message' => 'Blog Delete Successfully',
            'alert-type' => 'success'
        );
        return back()->with($notification);
    }

}
