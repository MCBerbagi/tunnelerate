<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\Counter;
use App\Models\Profile;
use Illuminate\Http\Request;

class CounterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $counter=Counter::orderBy('id', 'asc')->latest()->get();
        $profile=Profile::first();
        return view('admin.counter.counter', compact('counter','profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'fc_icon'=>'required|image|mimes:png,jpeg,jpg',
            'fc_number' => 'required',
            'fc_title' => 'nullable',
        ]);

        //Counter Icon

        if ($request->fc_icon) {

            $imageName = time().'.'.request()->fc_icon->getClientOriginalExtension();
            request()->fc_icon->move('assets/img/counter/',$imageName);
            $fc_icon='assets/img/counter/'.$imageName;
        }

        $counter=new Counter();
        $counter->dc_title=$request->fc_title;
        $counter->dc_number=$request->fc_number;
        $counter->dc_icon=$fc_icon;
        $counter->save();
        $notification = array(
            'message' => 'Counter Create Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Counter  $counter
     * @return \Illuminate\Http\Response
     */
    public function show(Counter $counter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Counter  $counter
     * @return \Illuminate\Http\Response
     */
    public function edit(Counter $counter)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Counter  $counter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)

    {
        $validatedData = $request->validate([
            'fc_icon'=>'image|mimes:png,jpeg,jpg',
            'fc_number' => 'required',
            'fc_title' => 'nullable',
        ]);

        $counter=Counter::find($id);

        //counter Icon
        if ($request->hasFile('fc_icon')) {

            $imageName = time().'.'.request()->fc_icon->getClientOriginalExtension();
            request()->fc_icon->move('assets/img/counter/',$imageName);
            $fc_icon='assets/img/counter/'.$imageName;

            if (file_exists($counter->dc_icon)) {
                unlink($counter->dc_icon);
             }
             $counter->dc_icon=$fc_icon;

        }

        $counter->dc_title=$request->fc_title;
        $counter->dc_number=$request->fc_number;
        $counter->save();
        $notification = array(
            'message' => 'Counter Updated Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Counter  $counter
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $counter=Counter::find($id);
        if (file_exists($counter->dc_icon)) {
            unlink($counter->dc_icon);
        }

        Counter::destroy($id);
       $notification = array(
            'message' => 'Counter Delete Successfully',
            'alert-type' => 'success'
        );
        return back()->with($notification);
    }
}
