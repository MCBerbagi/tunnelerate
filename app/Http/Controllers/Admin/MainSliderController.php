<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\Profile;
use App\Models\MainSlider;
use Illuminate\Http\Request;

class MainSliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mainslider=MainSlider::all();
        $profile=Profile::first();
        return view('admin.mainslider.mainslider', compact('mainslider','profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)

    {
        $validatedData = $request->validate([
            'fms_title'=>'required',
            'fms_title2'=>'required',
            'fms_image'=>'required|image|mimes:png,jpeg,jpg',
            'fms_paragraph'=>'nullable',
            'fms_btn1'=>'nullable',
            'fms_url1'=>'nullable',
            'fms_btn2'=>'nullable',
            'fms_url2'=>'nullable',
        ]);


        //MainSlider Image

        if ($request->fms_image) {

            $imageName = time().'.'.request()->fms_image->getClientOriginalExtension();
            request()->fms_image->move('assets/img/slider/',$imageName);
            $fms_image='assets/img/slider/'.$imageName;
        }

        $mainslider=new MainSlider;
        $mainslider->dms_title=$request->fms_title;
        $mainslider->dms_title2=$request->fms_title2;
        $mainslider->dms_paragraph=$request->fms_paragraph;
        $mainslider->dms_btn1=$request->fms_btn1;
        $mainslider->dms_url1=$request->fms_url1;
        $mainslider->dms_btn2=$request->fms_btn2;
        $mainslider->dms_url2=$request->fms_url2;
        $mainslider->dms_image=$fms_image;
        $mainslider->save();
        $notification = array(
            'message' => 'Slider Created Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MainSlider  $mainslider
     * @return \Illuminate\Http\Response
     */
    public function show(MainSlider $mainslider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MainSlider  $mainslider
     * @return \Illuminate\Http\Response
     */
    public function edit(MainSlider $mainslider)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MainSlider  $mainslider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)

    {
        $validatedData = $request->validate([
            'fms_title'=>'required',
            'fms_title2'=>'required',
            'fms_image'=>'image|mimes:png,jpeg,jpg',
            'fms_paragraph'=>'nullable',
            'fms_btn1'=>'nullable',
            'fms_url1'=>'nullable',
            'fms_btn2'=>'nullable',
            'fms_url2'=>'nullable',
        ]);


        $mainslider=MainSlider::find($id);

        //mainslider Icon
        if ($request->hasFile('fms_image')) {

            $imageName = time().'.'.request()->fms_image->getClientOriginalExtension();
            request()->fms_image->move('assets/img/slider/',$imageName);
            $fms_image='assets/img/slider/'.$imageName;

            if (file_exists($mainslider->dms_image)) {
                unlink($mainslider->dms_image);
             }
             $mainslider->dms_image=$fms_image;

        }

        $mainslider->dms_title=$request->fms_title;
        $mainslider->dms_title2=$request->fms_title2;
        $mainslider->dms_paragraph=$request->fms_paragraph;
        $mainslider->dms_btn1=$request->fms_btn1;
        $mainslider->dms_url1=$request->fms_url1;
        $mainslider->dms_btn2=$request->fms_btn2;
        $mainslider->dms_url2=$request->fms_url2;
        $mainslider->save();
        $notification = array(
            'message' => 'Slider Updated Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MainSlider  $mainslider
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mainslider=MainSlider::find($id);
        if (file_exists($mainslider->dms_image)) {
            unlink($mainslider->dms_image);
        }

        MainSlider::destroy($id);
       $notification = array(
            'message' => 'Slider Delete Successfully',
            'alert-type' => 'success'
        );
        return back()->with($notification);
    }

}
