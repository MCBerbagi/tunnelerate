<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\VideoContent;
use App\Models\Profile;
use Illuminate\Http\Request;

class VideoContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $videocontent=VideoContent::first();
        $profile=Profile::first();
        return view('admin.video.videocontent', compact('videocontent','profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)

    {
        $validatedData = $request->validate([

            'fvideoc_big_image' =>'required|image|mimes:png,jpeg,jpg',
            'fvideoc_sec_title' =>'nullable',
            'fvideoc_sec_title_2' =>'nullable',
        ]);


        //VideoContent Big Image

        if ($request->fvideoc_big_image) {

            $imageName = time().'1.'.request()->fvideoc_big_image->getClientOriginalExtension();
            request()->fvideoc_big_image->move('assets/img/video/',$imageName);
            $fvideoc_big_image='assets/img/video/'.$imageName;
        }


        $videocontent=new VideoContent();
        $videocontent->dvideoc_sec_title=$request->fvideoc_sec_title;
        $videocontent->dvideoc_sec_title_2=$request->fvideoc_sec_title_2;
        $videocontent->dvideoc_big_image=$fvideoc_big_image;
        $videocontent->save();
        $notification = array(
            'message' => 'VideoContent Create Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\VideoContent  $videocontent
     * @return \Illuminate\Http\Response
     */
    public function show(VideoContent $videocontent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\VideoContent  $videocontent
     * @return \Illuminate\Http\Response
     */
    public function edit(VideoContent $videocontent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\VideoContent  $videocontent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)


    {
        $validatedData = $request->validate([
            'fvideoc_big_image' =>'image|mimes:png,jpeg,jpg',
            'fvideoc_sec_title' =>'nullable',
            'fvideoc_sec_title_2' =>'nullable',
        ]);

        $videocontent=VideoContent::find($id);


        //VideoContent Big Image

        if ($request->hasFile('fvideoc_big_image')) {

            $imageName = time().'1.'.request()->fvideoc_big_image->getClientOriginalExtension();
            request()->fvideoc_big_image->move('assets/img/video/',$imageName);
            $fvideoc_big_image='assets/img/video/'.$imageName;

            if (file_exists($videocontent->dvideoc_big_image)) {
                unlink($videocontent->dvideoc_big_image);
                }
                $videocontent->dvideoc_big_image=$fvideoc_big_image;
        }


        $videocontent->dvideoc_sec_title=$request->fvideoc_sec_title;
        $videocontent->dvideoc_sec_title_2=$request->fvideoc_sec_title_2;
        $videocontent->save();
        $notification = array(
            'message' => 'Updated Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\VideoContent  $videocontent
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $videocontent=VideoContent::find($id);

        if (file_exists($videocontent->dvideoc_big_image)) {
            unlink($videocontent->dvideoc_big_image);
        }

        VideoContent::destroy($id);
       $notification = array(
            'message' => 'Content Delete Successfully',
            'alert-type' => 'success'
        );
        return back()->with($notification);

    }
}
