<?php

namespace App\Http\Controllers;

use App\Models\Accelerator;
use App\Models\EmailPendaftar;
use App\Models\General;
use App\Models\Invest;
use App\Models\MainSlider;
use App\Models\About;
use App\Models\Counter;
use App\Models\partner;
use App\Models\Register_investment;
use App\Models\Register_partnership;
use App\Models\Register_startup;
use App\Models\Service;
use App\Models\ServiceContent;
use App\Models\Team;
use App\Models\TeamContent;
use App\Models\Cta;
use App\Models\Testimonial;
use App\Models\TestimonialContent;
use App\Models\Video;
use App\Models\VideoContent;
use App\Models\Blog;
use App\Models\BlogContent;
use App\Models\SubscribeContent;
use App\Models\communitypartner;
use App\Models\Contact;
use App\Models\Contactinfo;
use App\Models\Subscribe;
use App\Models\Widget1;
use App\Models\Widget2;
use App\Models\Widget3;
use App\Models\mediapartner;
use App\Models\vcpartner;
use Illuminate\Http\Request;
use App\Models\Email;
use Illuminate\Support\Facades\Mail;

class SiteController extends Controller
{

    public function index()
    {
        $general=General::latest()->get();
        $mainSlider=MainSlider::latest()->get();
        $about=About::first();
        $counter=Counter::orderBy('id', 'asc')->latest()->get();
        $service=Service::orderBy('id', 'asc')->latest()->get();
        $servicecontent=ServiceContent::latest()->get();
        $team=Team::orderBy('id', 'asc')->latest()->get();
        $teamcontent=TeamContent::latest()->get();
        $cta=Cta::first();
        $testimonial=Testimonial::orderBy('id', 'asc')->latest()->get();
        $testimonialcontent=TestimonialContent::latest()->get();
        $video=Video::latest()->get();
        $videocontent=VideoContent::latest()->get();
        $blog=Blog::orderBy('id', 'asc')->latest()->paginate(6);
        $blogcontent=BlogContent::latest()->get();
        $subscribecontent=SubscribeContent::first();
        $communitypartner=communitypartner::latest()->get();
        $mediapartner=mediapartner::latest()->get();
        $vcpartner=vcpartner::latest()->get();

        $widget1=Widget1::latest()->get();
        $widget2=Widget2::orderBy('id', 'asc')->latest()->get();
        $footerblog=Blog::orderBy('id', 'asc')->latest()->paginate(3);
        $widget3=Widget3::orderBy('id', 'asc')->latest()->get();

        return view('website.pages.index', compact('general','vcpartner','mediapartner','mainSlider','about','counter','service','servicecontent','team','teamcontent','cta','testimonial','testimonialcontent','video','videocontent','blog','blogcontent','subscribecontent','communitypartner','widget1','widget2','footerblog','widget3'));
    }


    public function hearourpoint()
    {
        return view('website.index_hearourpoint');
    }

    public function about()
    {
        $general=General::latest()->get();
        $about=About::first();
        $counter=Counter::orderBy('id', 'asc')->latest()->get();
        $widget1=Widget1::latest()->get();
        $widget2=Widget2::orderBy('id', 'asc')->latest()->get();
        $footerblog=Blog::orderBy('id', 'asc')->latest()->paginate(3);
        $widget3=Widget3::orderBy('id', 'asc')->latest()->get();

        return view('website.pages.about', compact('general','about','counter','widget1','widget2','footerblog','widget3'));
    }
    public function apply()
    {
        $general=General::latest()->get();
        $about=About::first();
        $counter=Counter::orderBy('id', 'asc')->latest()->get();
        $widget1=Widget1::latest()->get();
        $widget2=Widget2::orderBy('id', 'asc')->latest()->get();
        $footerblog=Blog::orderBy('id', 'asc')->latest()->paginate(3);
        $widget3=Widget3::orderBy('id', 'asc')->latest()->get();

        return view('website.pages.apply', compact('general','about','counter','widget1','widget2','footerblog','widget3'));
    }


    public function support()
    {
        $general=General::latest()->get();
        $team=Team::orderBy('id', 'asc')->latest()->get();
        $teamcontent=TeamContent::latest()->get();
        $widget1=Widget1::latest()->get();
        $widget2=Widget2::orderBy('id', 'asc')->latest()->get();
        $client=communitypartner::latest()->get();
        $footerblog=Blog::orderBy('id', 'asc')->latest()->paginate(3);
        $widget3=Widget3::orderBy('id', 'asc')->latest()->get();

        return view('website.pages.support', compact('general','team','teamcontent','widget1','widget2','client','footerblog','widget3'));
    }
    public function testimonial()
    {
        $general=General::latest()->get();
        $team=Team::orderBy('id', 'asc')->latest()->get();
        $testimonial=Testimonial::orderBy('id', 'asc')->latest()->get();
        $testimonialcontent=TestimonialContent::latest()->get();
        $widget1=Widget1::latest()->get();
        $widget2=Widget2::orderBy('id', 'asc')->latest()->get();
        $footerblog=Blog::orderBy('id', 'asc')->latest()->paginate(3);
        $widget3=Widget3::orderBy('id', 'asc')->latest()->get();

        return view('website.pages.testimonial', compact('general','team','testimonial','testimonialcontent','widget1','widget2','footerblog','widget3'));
    }
    public function bloglist()
    {
        $general=General::latest()->get();
        $blog=Blog::orderBy('id', 'asc')->latest()->paginate(3);
        $blog_relat_sidebar=Blog::latest()->paginate(3);
        $widget1=Widget1::latest()->get();
        $widget2=Widget2::orderBy('id', 'asc')->latest()->get();
        $footerblog=Blog::orderBy('id', 'asc')->latest()->paginate(3);
        $widget3=Widget3::orderBy('id', 'asc')->latest()->get();

        return view('website.pages.blog-list', compact('general','blog','blog_relat_sidebar','widget1','widget2','footerblog','widget3'));
    }
    public function bloggrid()
    {
        $general=General::latest()->get();
        $blog=Blog::orderBy('id', 'asc')->latest()->paginate(4);
        $widget1=Widget1::latest()->get();
        $widget2=Widget2::orderBy('id', 'asc')->latest()->get();
        $footerblog=Blog::orderBy('id', 'asc')->latest()->paginate(3);
        $widget3=Widget3::orderBy('id', 'asc')->latest()->get();

        return view('website.pages.blog-grid', compact('general','blog','widget1','widget2','footerblog','widget3'));
    }
    public function partner() {
        $general=General::latest()->get();
        $partnerr=partner::first();
        $team=Team::orderBy('id', 'asc')->latest()->get();
        $teamcontent=TeamContent::latest()->get();
        $widget1=Widget1::latest()->get();
        $client=communitypartner::latest()->get();
        $widget2=Widget2::orderBy('id', 'asc')->latest()->get();
        $footerblog=Blog::orderBy('id', 'asc')->latest()->paginate(3);
        $widget3=Widget3::orderBy('id', 'asc')->latest()->get();

        return view('website.pages.partner', compact('general','partnerr','team','teamcontent','client','widget1','widget2','footerblog','widget3'));
    }
    public function invest() {
        $general=General::latest()->get();
        $investt=Invest::first();
        $widget1=Widget1::latest()->get();
        $widget2=Widget2::orderBy('id', 'asc')->latest()->get();
        $footerblog=Blog::orderBy('id', 'asc')->latest()->paginate(3);
        $widget3=Widget3::orderBy('id', 'asc')->latest()->get();

        return view('website.pages.invest', compact('general','investt','widget1','widget2','footerblog','widget3'));
    }
    public function acc() {
        $general=General::latest()->get();
        $accleratorss=Accelerator::first();
        $cta=Cta::first();
        $widget1=Widget1::latest()->get();
        $widget2=Widget2::orderBy('id', 'asc')->latest()->get();
        $footerblog=Blog::orderBy('id', 'asc')->latest()->paginate(3);
        $widget3=Widget3::orderBy('id', 'asc')->latest()->get();

        return view('website.pages.acc', compact('general','accleratorss','cta','widget1','widget2','footerblog','widget3'));
    }
    public function contact()
    {
        $general=General::latest()->get();
        $contactinfo=Contactinfo::orderBy('id', 'asc')->latest()->paginate(4);
        $widget1=Widget1::latest()->get();
        $widget2=Widget2::orderBy('id', 'asc')->latest()->get();
        $footerblog=Blog::orderBy('id', 'asc')->latest()->paginate(3);
        $widget3=Widget3::orderBy('id', 'asc')->latest()->get();

        return view('website.pages.contact', compact('general','contactinfo','widget1','widget2','footerblog','widget3'));
    }
    public function blogdetails($id)
    {

        $general=General::latest()->get();
        $blog = Blog::find($id);
        $post_tags = Blog::where('id', $id)->first();
        $blog_relat_sidebar=Blog::latest()->paginate(3);
        $widget1=Widget1::latest()->get();
        $widget2=Widget2::orderBy('id', 'asc')->latest()->get();
        $footerblog=Blog::orderBy('id', 'asc')->latest()->paginate(3);
        $widget3=Widget3::orderBy('id', 'asc')->latest()->get();

        return view('website.pages.blog-details', compact('general','blog','post_tags','blog_relat_sidebar','widget1','widget2','footerblog','widget3'));
    }
    public function blogsearchpage(Request $request)

    {
        $general=General::latest()->get();
        $search = $request->input('search');
        $search_blog =Blog::Where('dblog_title', 'LIKE', "%{$search}%")->orWhere('dblog_paragraph', 'LIKE', "%{$search}%")->orderBy('id', 'desc')->get();
        $blog_relat_sidebar=Blog::latest()->paginate(3);
        $widget1=Widget1::latest()->get();
        $widget2=Widget2::orderBy('id', 'asc')->latest()->get();
        $footerblog=Blog::orderBy('id', 'asc')->latest()->paginate(3);
        $widget3=Widget3::orderBy('id', 'asc')->latest()->get();

        return view('website.pages.blog-searchpage', compact('general','search','search_blog','blog_relat_sidebar','widget1','widget2','footerblog','widget3'));
    }


    public function frsubscribestore(Request $request)
    {
        $general=General::latest()->get();
        $mainSlider=MainSlider::latest()->get();
        $about=About::first();
        $counter=Counter::orderBy('id', 'asc')->latest()->get();
        $service=Service::orderBy('id', 'asc')->latest()->get();
        $servicecontent=ServiceContent::latest()->get();
        $team=Team::orderBy('id', 'asc')->latest()->get();
        $teamcontent=TeamContent::latest()->get();
        $cta=Cta::first();
        $testimonial=Testimonial::orderBy('id', 'asc')->latest()->get();
        $testimonialcontent=TestimonialContent::latest()->get();
        $video=Video::latest()->get();
        $videocontent=VideoContent::latest()->get();
        $blog=Blog::orderBy('id', 'asc')->latest()->paginate(2);
        $blogcontent=BlogContent::latest()->get();
        $subscribecontent=SubscribeContent::first();
        $client=communitypartner::latest()->get();
        $widget1=Widget1::latest()->get();
        $widget2=Widget2::orderBy('id', 'asc')->latest()->get();
        $footerblog=Blog::orderBy('id', 'asc')->latest()->paginate(3);
        $widget3=Widget3::orderBy('id', 'asc')->latest()->get();
//        $validatedData = $request->validate([
//            'email' => 'required|email',
//            'nama' => 'required|nama',
//        ],
//        [
//            'email.required' => __('Email is required'),
//            'nama.required' => __('Email is required'),
//        ]);

        $subscribe=new Subscribe;
        $subscribe->email=$request->email;
        $subscribe->nama=$request->nama;
        $subscribe->save();
        Mail::to($request->email)->send(new Email);
        return view('website.pages.index', compact('general','mainSlider','about','counter','service','servicecontent','team','teamcontent','cta','testimonial','testimonialcontent','video','videocontent','blog','blogcontent','subscribecontent','client','widget1','widget2','footerblog','widget3'))->with('success','Thanks for subscribe');
    }
    public function frpartnerstore(Request $request)
    {
//        $validatedData = $request->validate([
//            'fco_name_partner'           => 'required',
//            'fco_email'          => 'required|email',
//            'fco_job'        => 'required',
//            'fco_num'        => 'required',
//            'fco_company'        => 'required',
//            'fco_linkedin_profile'        => 'required',
//            'fco_type'        => 'required',
//            'fco_company_operating_in'        => 'required',
//            'fco_investor_type'        => 'required',
//            'fco_have_you_invested_in_venture'=> 'required',
//            'fco_have_you_mentored_any_startup_before'        => 'required',
//            'fco_how_do_you'        => 'required',
//            'fco_how_did'        => 'required',
//        ],
//            [
//                'fco_name_partner.required' => __('Name is required'),
//                'fco_email.required' => __('Email is required'),
//                'fco_job.required' => __('Job is required'),
//                'fco_num.required' => __('Number is required'),
//                'fco_company.required' => __('Company is required'),
//                'fco_linkedin_profile.required' => __('Linkedin is required'),
//                'fco_type.required' => __('Company Type is required'),
//                'fco_company_operating_in.required' => __('Company operating is required'),
//                'fco_investor_type.required' => __('Message is required'),
//                'fco_have_you_invested_in_venture.required' => __('Have you invested in venture is required'),
//                'fco_have_you_mentored_any_startup_before.required' => __('Mentored is required'),
//                'fco_how_do_you.required' => __('Message is required'),
//                'fco_how_did.required' => __('Message is required'),
//            ]);

        $partner=new Register_partnership;
        $partner->name=$request->name;
        $partner->email=$request->email;
        $partner->job_title=$request->job;
        $partner->mobile_num=$request->num;
        $partner->company=$request->company;
        $partner->company_web=$request->company_webb;
        $partner->linked_profile_url=$request->linkedin;
        $partner->type_company=$request->type;
        $partner->company_operating_in=$request->compoin;
        $partner->Which_type_of_partnership_below_that_suits_your_interest=$request->typepart;
        $partner->Have_you_invested_in_a_venture_before=$request->hyii;
        $partner->Have_you_mentored_any_startup_before=$request->hyii;
        $partner->How_do_you_prefer_to_be_contacted=$request->hdyh;
        $partner->How_did_you_hear_about_Tunnelerate=$request->hwcc;
        $partner->save();
        Mail::to($request->email)->send(new EmailPendaftar);
        return redirect('/partner')->with('success','Thanks for submitting!');
    }
    public function frinveststore(Request $request)
    {
//        $validatedData = $request->validate([
//
//            'fco_email'          => 'required|email',
//            'fco_job'        => 'required',
//            'fco_company'        => 'required',
//            'fco_linkedin_profile'        => 'required',
//            'fco_type'        => 'required',
//            'fco_target_invest'        => 'required',
//            'fco_have_you_invested_in_venture'=> 'required',
//            'fco_how_did'        => 'required',
//        ],
//            [
//                'fco_name_invest.required' => __('Name is required'),
//                'fco_email.required' => __('Email is required'),
//                'fco_job.required' => __('Job is required'),
//                'fco_company.required' => __('Company is required'),
//                'fco_linkedin_profile.required' => __('Linkedin is required'),
//                'fco_type.required' => __('Company Type is required'),
//
//                'fco_target_invest.required' => __('Message is required'),
//                'fco_have_you_invested_in_venture.required' => __('Have you invested in venture is required'),
//                'fco_how_did.required' => __('Message is required'),
//            ]);

        $invest=new Register_investment;
        $invest->first_name=$request->name;
        $invest->email=$request->email;
        $invest->title=$request->job;
        $invest->company=$request->company;
        $invest->company_desc=$request->desc;
        $invest->linkedin_url=$request->linkedin;
        $invest->investor_type=$request->investor;
        $invest->select_what_interest_you=$request->swiy;
        $invest->target_investement_amount=$request->target;
        $invest->have_you_invested_in_venture=$request->hyii;
        $invest->how_did_you_hear_about_us=$request->hdyh;
        $invest->save();
        Mail::to($request->email)->send(new EmailPendaftar);
        return redirect('/invest')->with('success','Thanks for submitting!');
    }
    public function fraccstore(Request $request)
    {


        if ($request->hasFile('pitch')) {
            $image = $request->file('pitch');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = base_path('assets/uploads/');
            $image->move($destinationPath, $imageName);
        $acc = new Register_startup;
        $acc->name = $request->name;
        $acc->email = $request->email;
        $acc->linkedin = $request->linkedin;
        $acc->other_co_founder_linkedin = $request->other;
        $acc->company_name = $request->company_name;
        $acc->company_based = $request->based;
        $acc->company_website = $request->web;
        $acc->company_description = $request->desc;
        $acc->pitch = $imageName;
        $acc->demo = $request->demo;
        $acc->prob_solv = $request->solved;
        $acc->solution = $request->solusi;
        $acc->product_works = $request->works;
        $acc->product_for = $request->for;
        $acc->competitor = $request->comp;
        $acc->incorporated = $request->incor;
        $acc->long_working = $request->long;
        $acc->participated = $request->par;
        $acc->received = $request->receiv;
        $acc->biggest_prob = $request->biggest;
        $acc->need = $request->need;
        $acc->anything = $request->any;
        $acc->save();
            Mail::to($request->email)->send(new EmailPendaftar);
        return redirect('/apply')->with('success', 'Thanks for submitting!');
    }
    else {
        return redirect('/apply')->with('failed', 'Sorry your file is oversized and doesnt fit pdf format!');
    }
    }

    public function frcontactstore(Request $request)
    {
        $validatedData = $request->validate([
            'fco_name'           => 'required',
            'fco_email'          => 'required|email',
            'fco_subject'        => 'required',
            'fco_message'        => 'required',
        ],
        [
            'fco_name.required' => __('Name is required'),
            'fco_email.required' => __('Email is required'),
            'fco_subject.required' => __('Subject is required'),
            'fco_message.required' => __('Message is required'),
        ]);

        $contact=new Contact;
        $contact->dco_name=$request->fco_name;
        $contact->dco_email=$request->fco_email;
        $contact->dco_subject=$request->fco_subject;
        $contact->dco_message=$request->fco_message;
        $contact->save();

    return response()->json([ 'success'=> __('Your Message Is Successfully Submitting!')]);
    }


}
