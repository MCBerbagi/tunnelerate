<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubscribeContent extends Model
{
    protected $table="subscribe_contents";
}
