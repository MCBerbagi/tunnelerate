<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TestimonialContent extends Model
{
    protected $table="testimonial_contents";
}
