<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>{{ __('Merox - Business Management System') }}</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">


		<link rel="shortcut icon" type="image/x-icon" href="{{ asset('uploads/favicon.ico') }}">
        <!-- Place favicon.ico in the root directory -->

	<!-- CSS here -->
	<link rel="stylesheet" href="{{ asset('assets/website/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/website/css/fontawesome-all.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/website/css/default.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/website/css/style.css') }}">

    </head>
    <body class="bg-color-greay">
        <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

        <!-- requirments-section-start -->
        <section class="mt-50 mb-50">
            <div class="requirments-section">
                <div class="content-requirments d-flex justify-content-center">
                    <div class="requirments-main-content">
                        <div class="installer-header text-center">
                            <h2>{{ __('Configuration') }}</h2>
                            <p>{{ __('Please enter your database connection details') }}</p>
                        </div>
                        @if (session('status'))
	                        <div class="alert alert-success" role="alert">
	                            {{ session('status') }}
	                        </div>
	                    @endif
                        <form action="{{ url('install/store') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="app_name">{{ __('App Name') }}</label>
                                    <input type="text" class="form-control" id="app_name" name="app_name" required placeholder="App Name without space">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="app_url">{{ __('App Url') }}</label>
                                    <input type="text" value="{{ url('/') }}" class="form-control" id="app_url" name="app_url" required placeholder="App Url">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="db_host">{{ __('Database Host') }}</label>
                                    <input type="text" value="localhost" class="form-control" id="db_host" name="db_host" required placeholder="Database Host">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="db_name">{{ __('Database Name') }}</label>
                                    <input type="text" class="form-control" id="db_name" name="db_name" required placeholder="Database Name">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="db_user">{{ __('Database Username') }}</label>
                                    <input type="text" class="form-control" id="db_user" name="db_user" required placeholder="Database Username">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="db_pass">{{ __('Database Password') }}</label>
                                    <input type="text" class="form-control" id="db_pass" name="db_pass" placeholder="Database Password">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="mail_driver">{{ __('Mail Driver') }}</label>
                                    <input type="text" class="form-control" value="smtp" id="mail_driver" name="mail_driver" required placeholder="Mail Driver">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="mail_host">{{ __('Mail Host') }}</label>
                                    <input type="text" class="form-control" value="smtp.mailtrap.io" id="mail_host" name="mail_host" required placeholder="Mail Host">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="mail_user">{{ __('Mail Username') }}</label>
                                    <input value="mailusername" type="text" class="form-control" id="mail_user" name="mail_user" required required placeholder="Mail Username">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="mail_pass">{{ __('Mail Password') }}</label>
                                    <input value="mailpassword" type="text" class="form-control" id="mail_pass" name="mail_pass" required placeholder="Mail Password">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="mail_port">{{ __('Mail Port') }}</label>
                                    <input type="text" value="2525" class="form-control" id="mail_port" name="mail_port" required required placeholder="Mail Port">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="mail_address">{{ __('Mail Address') }}</label>
                                    <input type="text" value="mailaddress" class="form-control" id="mail_address" name="mail_address" required placeholder="Mail Address">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="mail_enc">{{ __('Mail Encryption') }}</label>
                                    <select class="form-control" id="mail_enc" name="mail_enc">
                                      <option value="TLS">{{ __('TLS') }}</option>
                                      <option value="SSL">{{ __('SSL') }}</option>
                                      <option value="STARTTLS">{{ __('STARTTLS') }}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="mail_form">{{ __('Mail Form Name') }}</label>
                                    <input type="text" value="mailformname" class="form-control" id="mail_form" name="mail_form" required placeholder="Mail Form Name">
                                </div>
                            </div>
                            <div class="col-lg-12 mb-20">
                                <div class="form-group">
                                    <label for="mail_form">{{ __('Select Install Method') }}</label>
                                    <select class="form-control" name="method">
                                        <option value="install">{{ __('I want to install full system') }}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
						<a href="{{ url('install') }}" class="btn btn-type-2 float-left"><i class="fa fa-angle-double-left"></i> {{ __('Back') }}</a>
                        <button type="submit" class="btn btn-type-2 float-right">{{ __('Install') }} <i class="fa fa-angle-double-right"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <!-- requirments-section-end -->
    </body>
</html>
