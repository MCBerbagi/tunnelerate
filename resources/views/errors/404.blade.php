<!doctype html>
<html class="no-js" lang="en-us">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    @if(App\Models\General::count() > 0)
        @foreach(App\Models\General::latest()->get() as $item)

            @if(!empty( $item->dsite_title ))

                <title>{{ $item->dsite_title }} </title>
            @else

                <title>{{ __('messages.Merox - Business Management System') }}</title>
            @endif
        @endforeach
    @else
        <title>{{ __('messages.Merox - Business Management System') }}</title>
    @endif
    <meta name="keywords" content="Corporate Business Shopse HTML5 Css3 Template App Product laravel script" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">

    <!-- css here -->
    <link rel="stylesheet" href="{{ asset('assets/website/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/website/css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/website/css/fontawesome-all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/website/css/default.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/website/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/website/css/responsive.css') }}">
</head>

<body>

    <!-- main area start -->
    <main>
        <!-- merox error area start -->
        <div id="merox-error-area" class="merox-error-area text-center mt-100 mb-100">
            <div class="container">
                <div class="row">
                    <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 mx-auto">
                        <div class="merox-content-box">
                            <h2>{{ __('messages.404') }}</h2>
                            <h3>{{ __('messages.oops! that page can not be found.') }}</h3>
                            <p>{{ __('messages.it looks like nothing was found at this location. maybe try a search?') }}
                            </p>
                            <a href="{{ url('/') }}"
                                class="btn btn-type-1">{{ __('messages.go to home') }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- merox error area end -->
    </main>
    <!-- main area end -->

    <!-- js here -->
    <script src="{{ asset('assets/website/js/modernizr-3.5.0.min.js') }}"></script>
    <script src="{{ asset('assets/website/js/jquery-3.6.0.min.js') }}"></script>
    <script src="{{ asset('assets/website/js/popper.min.js') }}"></script>
    <script src="{{ asset('assets/website/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/website/js/wow.min.js') }}"></script>
    <script src="{{ asset('assets/website/js/plugins.js') }}"></script>
    <script>
        (function ($) {
            "use strict";

            new WOW().init();

        })(jQuery);

    </script>

</body>

</html>
