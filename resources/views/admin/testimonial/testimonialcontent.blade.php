@extends('admin.master')
@section('title')
{{ __('messages.Testimonial Content') }}
@endsection
@section('admincontent')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">{{ __('messages.Content Section') }}</h4>

                    <!-- Error Start -->
                    @if($errors->any())
                        <ul class="alert alert-danger alert-dismissible fade show mt-5">
                            @foreach($errors->all() as $error)
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    <!-- Error End -->

                    <div class="row">
                        <div class="col-md-12">

                            @if(isset($testimonialcontent))

                                <form
                                    action="{{ route('testimonialcontent.update',$testimonialcontent->id) }}"
                                    class="testimonialcontent-section-wraper-form" method="POST">

                                    @csrf
                                    @method('Put')

                                    <div class="form-group">
                                        <label for="ftmcon_sec_title">{{ __('messages.Section Title') }}</label>
                                        <input id="ftmcon_sec_title" type="text" name="ftmcon_sec_title"
                                            class="form-control" value="{{ $testimonialcontent->dtmcon_sec_title }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="ftmcon_sec_title_2">{{ __('messages.Section Title 2') }}</label>
                                        <textarea id="ftmcon_sec_title_2" type="text" name="ftmcon_sec_title_2"
                                            class="form-control"
                                            rows="4">{{ $testimonialcontent->dtmcon_sec_title_2 }}</textarea>
                                    </div>

                                    <button type="submit" class="btn btn-primary">{{ __('messages.Update') }}</button>
                                    <a href="{{ route('testimonialcontent.destroy',$testimonialcontent->id ) }}"
                                        class="btn mb-1 btn-danger">{{ __('messages.Delete') }}</a>

                                </form>
                            @else

                                <form action="{{ route('testimonialcontent.store') }}"
                                    class="testimonialcontent-section-wraper-form" method="POST">
                                    @csrf

                                    <div class="form-group">
                                        <label for="ftmcon_sec_title">{{ __('messages.Section Title') }}</label>
                                        <input id="ftmcon_sec_title" type="text" name="ftmcon_sec_title"
                                            class="form-control" placeholder="{{ __('messages.Your Title Here') }} ">
                                    </div>

                                    <div class="form-group">
                                        <label for="ftmcon_sec_title_2">{{ __('messages.Section Title 2') }}</label>
                                        <textarea id="ftmcon_sec_title_2" type="text" name="ftmcon_sec_title_2"
                                            class="form-control" rows="4">{{ __('messages.Your Second Title Here') }}</textarea>
                                    </div>

                                    <button type="submit" class="btn btn-primary">{{ __('messages.Create') }}</button>

                                </form>

                            @endif

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
