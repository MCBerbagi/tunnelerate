@extends('admin.master')
@section('title')
{{ __('messages.Testimonial') }}
@endsection
@section('admincontent')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">
                        {{ __('messages.Testimonial Table') }}
                        <button type="button" class="btn btn-primary float-right" data-toggle="modal"
                            data-target="#testimonialmodal">{{ __('messages.Create Testimonial') }}</button>
                    </h4>
                    <!-- modal start -->
                    <!-- Modal -->
                    <div class="modal fade" id="testimonialmodal" tabindex="-1" role="dialog"
                        aria-labelledby="modaltitle" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="modaltitle">{{ __('messages.Create Testimonial') }}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">

                                    <!-- Modal Body Contant Start -->
                                    <form action="{{ route('testimonial.store') }}" method="POST"
                                        class="create-testimonial-form-wraper" enctype="multipart/form-data">
                                        @csrf

                                        <div class="form-group">
                                            <label for="fts_title">{{ __('messages.Title') }}</label>
                                            <input id="fts_title" type="text" name="fts_title" class="form-control"
                                                placeholder="{{ __('messages.Title Here') }}" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="fts_desig">{{ __('messages.Designation') }}</label>
                                            <input id="fts_desig" type="text" name="fts_desig" class="form-control"
                                                placeholder="{{ __('messages.Designation') }}">
                                        </div>

                                        <div class="form-group">
                                            <label for="fts_paragraph">{{ __('messages.Paragraph') }}</label>
                                            <textarea id="fts_paragraph" type="text" name="fts_paragraph"
                                                class="form-control"
                                                rows="4">{{ __("messages.dummy text of the printing and type setting and industry. Lorem Ipsum has been the industry's") }}</textarea>
                                        </div>

                                        <div class="form-group">
                                            <label for="fts_image">{{ __('messages.Upload Image') }}</label>
                                            <input id="fts_image" type="file" name="fts_image" class="form-control"
                                                required>
                                        </div>


                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-secondary"
                                                data-dismiss="modal">{{ __('messages.Close') }}</button>
                                            <button type="submit" class="btn btn-primary">{{ __('messages.Save') }}</button>
                                        </div>

                                    </form>
                                    <!-- Modal Body Contant End -->
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- modal end -->
                    <!-- Error Start -->
                    @if($errors->any())
                        <ul class="alert alert-danger alert-dismissible fade show mt-5">
                            @foreach($errors->all() as $error)
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    <!-- Error End -->
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered zero-configuration">
                            <thead>
                                <tr>
                                    <th>{{ __('messages.No') }}</th>
                                    <th>{{ __('messages.Name') }}</th>
                                    <th>{{ __('messages.Designation') }}</th>
                                    <th>{{ __('messages.Image') }}</th>
                                    <th scope="col">{{ __('messages.Action') }}</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($testimonial as $key => $item)

                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $item->dts_title }}</td>
                                        <td>{{ $item->dts_desig }}</td>
                                        <td><img style="width: 50px" src="{{ asset($item->dts_image) }}" alt="img-alt" /></td>
                                        <td>
                                            <span>

                                                <button type="button" class="btn mb-1 btn-primary modal-title"
                                                    data-toggle="modal" data-target="#TestimonialEditModal{{ $key }}">
                                                    <i class="fa fa-pencil color-muted m-r-5"></i>
                                                </button>


                                                <a href="{{ route('testimonial.destroy',$item->id) }}"
                                                    class="btn mb-1 btn-danger"><i
                                                        class="fa fa-close color-danger"></i></a>

                                            </span>
                                        </td>
                                    </tr>

                                    <!-- Hero Edit Modal -->
                                    <div class="modal fade" id="TestimonialEditModal{{ $key }}" tabindex="-1"
                                        role="dialog" aria-labelledby="editmodaltitle" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="editmodaltitle">{{ __('messages.Edit Testimonial') }}</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <!-- Modal Body Contant Start -->
                                                    <form class="create-slider-form-wraper" method="POST"
                                                        action="{{ route('testimonial.update',$item->id) }}"
                                                        enctype="multipart/form-data">
                                                        @csrf
                                                        @method('PUT')

                                                        <div class="form-group">
                                                            <label for="fts_title">{{ __('messages.Title') }}</label>
                                                            <input id="fts_title" type="text" name="fts_title"
                                                                class="form-control" placeholder="{{ __('messages.Title Here') }}"
                                                                value="{{ $item->dts_title }}">
                                                        </div>


                                                        <div class="form-group">
                                                            <label for="fts_desig">{{ __('messages.Designation') }}</label>
                                                            <input id="fts_desig" type="text" name="fts_desig"
                                                                class="form-control" value="{{ $item->dts_desig }}">
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="fts_paragraph">{{ __('messages.Paragraph') }}</label>
                                                            <textarea id="fts_paragraph" type="text"
                                                                name="fts_paragraph" class="form-control"
                                                                rows="4">{{ $item->dts_paragraph }}</textarea>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="fts_image">{{ __('messages.Upload Image') }}</label>
                                                            <input id="fts_image" type="file" name="fts_image"
                                                                class="form-control" value="{{ $item->dts_image }}">
                                                        </div>


                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-secondary"
                                                                data-dismiss="modal">{{ __('messages.Close') }}</button>
                                                            <button type="submit" class="btn btn-primary">{{ __('messages.Save') }}</button>
                                                        </div>

                                                    </form>
                                                    <!-- Modal Body Contant End -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Hero Edit Modal -->

                                @endforeach

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>{{ __('messages.No') }}</th>
                                    <th>{{ __('messages.Name') }}</th>
                                    <th>{{ __('messages.Designation') }}</th>
                                    <th>{{ __('messages.Image') }}</th>
                                    <th scope="col">{{ __('messages.Action') }}</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
