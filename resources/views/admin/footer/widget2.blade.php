@extends('admin.master')
@section('title')
{{ __('messages.Quick Link') }}
@endsection
@section('admincontent')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <!-- Error Start -->
                    @if($errors->any())
                        <ul class="alert alert-danger alert-dismissible fade show mt-5">
                            @foreach($errors->all() as $error)
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    <!-- Error End -->

                    <h4 class="card-title">
                        {{ __('messages.Link Table') }}
                        <button type="button" class="btn btn-primary float-right" data-toggle="modal"
                            data-target="#widget2modal">{{ __('messages.Create Link') }}</button>
                    </h4>
                    <!-- modal start -->
                    <!-- Modal -->
                    <div class="modal fade" id="widget2modal" tabindex="-1" role="dialog" aria-labelledby="modaltitle"
                        aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="modaltitle">{{ __('messages.Create Link') }}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <!-- Modal Body Contant Start -->
                                    <form action="{{ route('widget2.store') }}"
                                        class="create-widget2-form-wraper" method="POST" enctype="multipart/form-data">
                                        @csrf

                                        <div class="form-group">
                                            <label for="fwidget2_text">{{ __('messages.Title') }}</label>
                                            <input id="fwidget2_text" type="text" name="fwidget2_text"
                                                class="form-control" placeholder="{{ __('messages.Your Title Here') }}" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="fwidget2_link">{{ __('messages.Link') }}</label>
                                            <input id="fwidget2_link" type="text" name="fwidget2_link"
                                                class="form-control" placeholder="{{ __('messages.www.example.com') }}">
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal">{{ __('messages.Close') }}</button>
                                            <button type="submit" class="btn btn-primary">{{ __('messages.Save') }}</button>
                                        </div>
                                    </form>

                                    <!-- Modal Body Contant End -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- modal end -->
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered zero-configuration">
                            <thead>
                                <tr>
                                    <th>{{ __('messages.No') }}</th>
                                    <th>{{ __('messages.Title') }}</th>
                                    <th>{{ __('messages.Link') }}</th>
                                    <th>{{ __('messages.Action') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($widget2 as $key=> $item)

                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $item->dwidget2_text }}</td>
                                        <td>{{ $item->dwidget2_link }}</td>
                                        <td>
                                            <span>

                                                <button type="button" class="btn mb-1 btn-primary modal-title"
                                                    data-toggle="modal" data-target="#Widget2ditModal{{ $key }}">
                                                    <i class="fa fa-pencil color-muted m-r-5"></i>
                                                </button>


                                                <a href="{{ route('widget2.destroy',$item->id) }}"
                                                    class="btn mb-1 btn-danger"><i
                                                        class="fa fa-close color-danger"></i></a>
                                            </span>
                                        </td>
                                    </tr>

                                    <!-- widget2 Edit Modal -->
                                    <div class="modal fade" id="Widget2ditModal{{ $key }}" tabindex="-1" role="dialog"
                                        aria-labelledby="editmodaltitle" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="editmodaltitle">{{ __('messages.Edit widget2') }}</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <!-- Modal Body Contant Start -->
                                                    <form class="create-widget2-form-wraper" method="POST"
                                                        action="{{ route('widget2.update',$item->id) }}"
                                                        enctype="multipart/form-data" enctype="multipart/form-data">
                                                        @csrf
                                                        @method('PUT')

                                                        <div class="form-group">
                                                            <label for="fwidget2_text">{{ __('messages.Title') }}</label>
                                                            <input id="fwidget2_text" type="text" name="fwidget2_text"
                                                                class="form-control" value="{{ $item->dwidget2_text }}">
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="fwidget2_link">{{ __('messages.Link') }}</label>
                                                            <input id="fwidget2_link" type="text" name="fwidget2_link"
                                                                class="form-control"
                                                                value="{{ $item->dwidget2_link }}">
                                                        </div>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                data-dismiss="modal">{{ __('messages.Close') }}</button>
                                                            <button type="submit" class="btn btn-primary">{{ __('messages.Save') }}</button>
                                                        </div>
                                                    </form>
                                                    <!-- Modal Body Contant End -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- widget2 Edit Modal -->
                                @endforeach

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>{{ __('messages.No') }}</th>
                                    <th>{{ __('messages.Title') }}</th>
                                    <th>{{ __('messages.Link') }}</th>
                                    <th>{{ __('messages.Action') }}</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
