@extends('admin.master')
@section('title')
{{ __('messages.Widget 3') }}
@endsection
@section('admincontent')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">
                        {{ __('messages.Widget 3 Table') }}
                        <button type="button" class="btn btn-primary float-right" data-toggle="modal"
                            data-target="#widget3modal">{{ __('messages.Create Information') }}</button>
                    </h4>
                    <!-- modal start -->
                    <!-- Modal -->
                    <div class="modal fade" id="widget3modal" tabindex="-1" role="dialog" aria-labelledby="modaltitle"
                        aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="modaltitle">{{ __('messages.Create Information') }}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">

                                    <!-- Modal Body Contant Start -->
                                    <form action="{{ route('widget3.store') }}" method="POST"
                                        class="create-pricing-form-wraper">
                                        @csrf

                                        <div class="form-group">
                                            <label for="fwidget3_icon_text">{{ __('messages.Icon') }}</label>
                                            <input id="fwidget3_icon_text" type="text" name="fwidget3_icon_text"
                                                class="form-control" placeholder="{{ __('messages.fa fa-map-marker') }}" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="fwidget3_info">Info</label>
                                            <textarea name="fwidget3_info" id="fwidget3_info" cols="1" rows="2"
                                                class="form-control">{{ __('messages.Type Your Information Here') }}</textarea>
                                        </div>

                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-secondary"
                                                data-dismiss="modal">{{ __('messages.Close') }}</button>
                                            <button type="submit" class="btn btn-primary">{{ __('messages.Save') }}</button>
                                        </div>

                                    </form>
                                    <!-- Modal Body Contant End -->
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- modal end -->
                    <!-- Error Start -->
                    @if($errors->any())
                        <ul class="alert alert-danger alert-dismissible fade show mt-5">
                            @foreach($errors->all() as $error)
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    <!-- Error End -->
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered zero-configuration">
                            <thead>
                                <tr>
                                    <th>{{ __('messages.No') }}</th>
                                    <th>{{ __('messages.Icon') }}</th>
                                    <th>{{ __('messages.Information') }}</th>
                                    <th scope="col">{{ __('messages.Action') }}</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($widget3 as $key => $item)

                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $item->dwidget3_icon_text }}</td>
                                        <td>{{ $item->dwidget3_info }}</td>
                                        <td>
                                            <span>

                                                <button type="button" class="btn mb-1 btn-primary modal-title"
                                                    data-toggle="modal" data-target="#PricingEditModal{{ $key }}">
                                                    <i class="fa fa-pencil color-muted m-r-5"></i>
                                                </button>


                                                <a href="{{ route('widget3.destroy',$item->id) }}"
                                                    class="btn mb-1 btn-danger"><i
                                                        class="fa fa-close color-danger"></i></a>
                                            </span>
                                        </td>
                                    </tr>

                                    <!-- Hero Edit Modal -->
                                    <div class="modal fade" id="PricingEditModal{{ $key }}" tabindex="-1" role="dialog"
                                        aria-labelledby="editmodaltitle" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="editmodaltitle">{{ __('messages.Edit Information') }}</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <!-- Modal Body Contant Start -->
                                                    <form class="create-slider-form-wraper" method="POST"
                                                        action="{{ route('widget3.update',$item->id) }}">
                                                        @csrf
                                                        @method('PUT')
                                                        <div class="form-group">
                                                            <label for="fwidget3_icon_text">{{ __('messages.Icon') }}</label>
                                                            <input id="fwidget3_icon_text" type="text"
                                                                name="fwidget3_icon_text" class="form-control"
                                                                value="{{ $item->dwidget3_icon_text }}">
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="fwidget3_info">{{ __('messages.Info') }}</label>
                                                            <textarea name="fwidget3_info" id="fwidget3_info" cols="1"
                                                                rows="2"
                                                                class="form-control">{{ $item->dwidget3_info }}</textarea>
                                                        </div>

                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-secondary"
                                                                data-dismiss="modal">{{ __('messages.Close') }}</button>
                                                            <button type="submit" class="btn btn-primary">{{ __('messages.Save') }}</button>
                                                        </div>

                                                    </form>
                                                    <!-- Modal Body Contant End -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Hero Edit Modal -->

                                @endforeach

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>{{ __('messages.No') }}</th>
                                    <th>{{ __('messages.Icon') }}</th>
                                    <th>{{ __('messages.Information') }}</th>
                                    <th scope="col">{{ __('messages.Action') }}</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
