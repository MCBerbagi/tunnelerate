@extends('admin.master')
@section('title')
{{ __('messages.Widget1') }}
@endsection
@section('admincontent')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">{{ __('messages.Widget1 Settings') }}</h4>

                    <!-- Error Start -->
                    @if($errors->any())
                        <ul class="alert alert-danger alert-dismissible fade show mt-5">
                            @foreach($errors->all() as $error)
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    <!-- Error End -->

                    <div class="row">
                        @if(!empty($widget1->dfw1_logo_image))
                            <div class="col-md-8">
                            @else
                                <div class="col-md-12">
                        @endif

                        @if(isset($widget1))


                            <form action="{{ route('widget1.update',$widget1->id) }}"
                                class="widget1-section-wraper-form" enctype="multipart/form-data" method="POST">

                                @csrf
                                @method('Put')


                                <div class="form-group">
                                    <label for="ffw1_logo_image">{{ __('messages.Footer Logo') }}</label>
                                    <input id="ffw1_logo_image" type="file" name="ffw1_logo_image" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label for="ffw1_paragraph">{{ __('messages.About Paragraph') }}</label>
                                    <textarea id="ffw1_paragraph" type="text" name="ffw1_paragraph" class="form-control"
                                        rows="4">{{ $widget1->dfw1_paragraph }}</textarea>
                                </div>

                                <div class="form-group">
                                    <label for="ffw1_social_icon_1">{{ __('messages.Social Icon 1') }}</label>
                                    <input id="ffw1_social_icon_1" type="text" name="ffw1_social_icon_1"
                                        class="form-control" value="{{ $widget1->dfw1_social_icon_1 }}">
                                </div>

                                <div class="form-group">
                                    <label for="ffw1_social_icon_1_link">{{ __('messages.Social Icon 1 Link') }}</label>
                                    <input id="ffw1_social_icon_1_link" type="text" name="ffw1_social_icon_1_link"
                                        class="form-control" value="{{ $widget1->dfw1_social_icon_1_link }}">
                                </div>

                                <div class="form-group">
                                    <label for="ffw1_social_icon_2">{{ __('messages.Social Icon 2') }}</label>
                                    <input id="ffw1_social_icon_2" type="text" name="ffw1_social_icon_2"
                                        class="form-control" value="{{ $widget1->dfw1_social_icon_2 }}">
                                </div>

                                <div class="form-group">
                                    <label for="ffw1_social_icon_2_link">{{ __('messages.Social Icon 2 Link') }}</label>
                                    <input id="ffw1_social_icon_2_link" type="text" name="ffw1_social_icon_2_link"
                                        class="form-control" value="{{ $widget1->dfw1_social_icon_2_link }}">
                                </div>

                                <div class="form-group">
                                    <label for="ffw1_social_icon_3">{{ __('messages.Social Icon 3') }}</label>
                                    <input id="ffw1_social_icon_3" type="text" name="ffw1_social_icon_3"
                                        class="form-control" value="{{ $widget1->dfw1_social_icon_3 }}">
                                </div>

                                <div class="form-group">
                                    <label for="ffw1_social_icon_3_link">{{ __('messages.Social Icon 3 Link') }}</label>
                                    <input id="ffw1_social_icon_3_link" type="text" name="ffw1_social_icon_3_link"
                                        class="form-control" value="{{ $widget1->dfw1_social_icon_3_link }}">
                                </div>

                                <div class="form-group">
                                    <label for="ffw1_social_icon_4">{{ __('messages.Social Icon 4') }}</label>
                                    <input id="ffw1_social_icon_4" type="text" name="ffw1_social_icon_4"
                                        class="form-control" value="{{ $widget1->dfw1_social_icon_4 }}">
                                </div>

                                <div class="form-group">
                                    <label for="ffw1_social_icon_4_link">{{ __('messages.Social Icon 4 Link') }}</label>
                                    <input id="ffw1_social_icon_4_link" type="text" name="ffw1_social_icon_4_link"
                                        class="form-control" value="{{ $widget1->dfw1_social_icon_4_link }}">
                                </div>
                                <button type="submit" class="btn btn-primary">{{ __('messages.Update') }}</button>
                                <a href="{{ route('widget1.destroy',$widget1->id ) }}"
                                    class="btn mb-1 btn-danger">{{ __('messages.Delete') }}</a>

                            </form>
                        @else


                            <form action="{{ route('widget1.store') }}"
                                class="widget1-section-wraper-form" enctype="multipart/form-data" method="POST">
                                @csrf

                                <div class="form-group">
                                    <label for="ffw1_logo_image">{{ __('messages.Footer Logo') }}</label>
                                    <input id="ffw1_logo_image" type="file" name="ffw1_logo_image" class="form-control"
                                        required>
                                </div>

                                <div class="form-group">
                                    <label for="ffw1_paragraph">{{ __('messages.About Paragraph') }}</label>
                                    <textarea id="ffw1_paragraph" type="text" name="ffw1_paragraph" class="form-control"
                                        rows="4">{{ __("messages.There are many variations of but the majority have suffered inject’s humour, or randomised word and don't look even slightly believable. If you are going to use") }}</textarea>
                                </div>

                                <div class="form-group">
                                    <label for="ffw1_social_icon_1">{{ __('messages.Social Icon 1') }}</label>
                                    <input id="ffw1_social_icon_1" type="text" name="ffw1_social_icon_1"
                                        class="form-control" placeholder="{{ __('messages.fa fa-facebook') }}">
                                </div>

                                <div class="form-group">
                                    <label for="ffw1_social_icon_1_link">{{ __('messages.Social Icon 1 Link') }}</label>
                                    <input id="ffw1_social_icon_1_link" type="text" name="ffw1_social_icon_1_link"
                                        class="form-control" placeholder="{{ __('messages.www.example.com') }}">
                                </div>

                                <div class="form-group">
                                    <label for="ffw1_social_icon_2">{{ __('messages.Social Icon 2') }}</label>
                                    <input id="ffw1_social_icon_2" type="text" name="ffw1_social_icon_2"
                                        class="form-control" placeholder="{{ __('messages.fa fa-twitter') }}">
                                </div>

                                <div class="form-group">
                                    <label for="ffw1_social_icon_2_link">{{ __('messages.Social Icon 2 Link') }}</label>
                                    <input id="ffw1_social_icon_2_link" type="text" name="ffw1_social_icon_2_link"
                                        class="form-control" placeholder="{{ __('messages.www.example.com') }}">
                                </div>

                                <div class="form-group">
                                    <label for="ffw1_social_icon_3">{{ __('messages.Social Icon 3') }}</label>
                                    <input id="ffw1_social_icon_3" type="text" name="ffw1_social_icon_3"
                                        class="form-control" placeholder="{{ __('messages.fa fa-instagram') }}">
                                </div>

                                <div class="form-group">
                                    <label for="ffw1_social_icon_3_link">{{ __('messages.Social Icon 3 Link') }}</label>
                                    <input id="ffw1_social_icon_3_link" type="text" name="ffw1_social_icon_3_link"
                                        class="form-control" placeholder="{{ __('messages.www.example.com') }}">
                                </div>

                                <div class="form-group">
                                    <label for="ffw1_social_icon_4">{{ __('messages.Social Icon 4') }}</label>
                                    <input id="ffw1_social_icon_4" type="text" name="ffw1_social_icon_4"
                                        class="form-control" placeholder="{{ __('messages.fa fa-vk') }}">
                                </div>

                                <div class="form-group">
                                    <label for="ffw1_social_icon_4_link">{{ __('messages.Social Icon 4 Link') }}</label>
                                    <input id="ffw1_social_icon_4_link" type="text" name="ffw1_social_icon_4_link"
                                        class="form-control" placeholder="{{ __('messages.www.example.com') }}">
                                </div>

                                <button type="submit" class="btn btn-primary">{{ __('messages.Create') }}</button>

                            </form>

                        @endif

                    </div>
                    @if(!empty($widget1->dfw1_logo_image))
                        <div class="col-md-4">

                            <div class="widget1-right-image">

                                <div class="widget1-icon mb-5">
                                    <h5>{{ __('messages.Footer Logo') }}</h5>

                                    <img class="img-fluid" src="{{ asset( $widget1->dfw1_logo_image ) }}"
                                        alt="img" />
                                </div>

                            </div>

                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
@endsection
