@extends('admin.master')
@section('title')
{{ __('messages.Contactinfo') }}
@endsection
@section('admincontent')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">
                        {{ __('messages.Contactinfo Table') }}
                        <button type="button" class="btn btn-primary float-right" data-toggle="modal"
                            data-target="#contactinfomodal">{{ __('messages.Create Contactinfo') }}</button>
                    </h4>
                    <!-- modal start -->
                    <!-- Modal -->
                    <div class="modal fade" id="contactinfomodal" tabindex="-1" role="dialog"
                        aria-labelledby="modaltitle" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="modaltitle">{{ __('messages.Create Contactinfo') }}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">

                                    <!-- Modal Body Contant Start -->
                                    <form action="{{ route('contactinfo.store') }}" method="POST"
                                        class="create-contactinfo-form-wraper" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group">
                                            <label for="fci_icon">{{ __('messages.Upload Icon') }}</label>
                                            <input id="fci_icon" type="file" name="fci_icon" class="form-control"
                                                required>
                                        </div>

                                        <div class="form-group">
                                            <label for="fci_info">{{ __('messages.Information') }}</label>
                                            <textarea id="fci_info" type="text" name="fci_info" class="form-control"
                                                rows="4" required>
                                                    <li>{{ __('messages.+1 (234) 567-8991') }}</li>
                                                    <li>{{ __('messages.+2 (234) 567-8991') }}</li>
                                            </textarea>
                                        </div>

                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-secondary"
                                                data-dismiss="modal">{{ __('messages.Close') }}</button>
                                            <button type="submit" class="btn btn-primary">{{ __('messages.Save') }}</button>
                                        </div>

                                    </form>
                                    <!-- Modal Body Contant End -->
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- modal end -->
                    <!-- Error Start -->
                    @if($errors->any())
                        <ul class="alert alert-danger alert-dismissible fade show mt-5">
                            @foreach($errors->all() as $error)
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    <!-- Error End -->
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered zero-configuration">
                            <thead>
                                <tr>
                                    <th>{{ __('messages.No') }}</th>
                                    <th>{{ __('messages.Icon') }}</th>
                                    <th>{{ __('messages.Contactinfo') }}</th>
                                    <th scope="col">{{ __('messages.Action') }}</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($contactinfo as $key => $item)

                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td><img src="{{ asset($item->dci_icon) }}" alt="img-alt" /></td>
                                        <td>{!! $item->dci_info !!}</td>
                                        <td>
                                            <span>

                                                <button type="button" class="btn mb-1 btn-primary modal-title"
                                                    data-toggle="modal" data-target="#ContactinfoEditModal{{ $key }}">
                                                    <i class="fa fa-pencil color-muted m-r-5"></i>
                                                </button>


                                                <a href="{{ route('contactinfo.destroy',$item->id) }}"
                                                    class="btn mb-1 btn-danger"><i
                                                        class="fa fa-close color-danger"></i></a>

                                            </span>
                                        </td>
                                    </tr>

                                    <!-- Hero Edit Modal -->
                                    <div class="modal fade" id="ContactinfoEditModal{{ $key }}" tabindex="-1"
                                        role="dialog" aria-labelledby="editmodaltitle" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="editmodaltitle">{{ __('messages.Edit Slider') }}</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <!-- Modal Body Contant Start -->
                                                    <form class="create-slider-form-wraper" method="POST"
                                                        action="{{ route('contactinfo.update',$item->id) }}"
                                                        enctype="multipart/form-data">
                                                        @csrf
                                                        @method('PUT')

                                                        <div class="form-group">
                                                            <label for="fci_icon">{{ __('messages.Upload Icon') }}</label>
                                                            <input id="fci_icon" type="file" name="fci_icon"
                                                                class="form-control"
                                                                value="{{ $item->dci_icon }}">
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="fci_info">{{ __('messages.Contact Info') }}</label>
                                                            <textarea id="fci_info" type="text" name="fci_info"
                                                                class="form-control"
                                                                rows="4">{{ $item->dci_info }}</textarea>
                                                        </div>

                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-secondary"
                                                                data-dismiss="modal">{{ __('messages.Close') }}</button>
                                                            <button type="submit" class="btn btn-primary">{{ __('messages.Save') }}</button>
                                                        </div>

                                                    </form>
                                                    <!-- Modal Body Contant End -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Hero Edit Modal -->

                                @endforeach

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>{{ __('messages.No') }}</th>
                                    <th>{{ __('messages.Icon') }}</th>
                                    <th>{{ __('messages.Contactinfo') }}</th>
                                    <th scope="col">{{ __('messages.Action') }}</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
