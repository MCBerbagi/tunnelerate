@extends('admin.master')
@section('title')
{{ __('messages.Contact') }}
@endsection
@section('admincontent')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">
                        {{ __('messages.Contact Table') }}
                    </h4>
                    <!-- modal start -->
                    <!-- Error Start -->
                    @if($errors->any())
                        <ul class="alert alert-danger alert-dismissible fade show mt-5">
                            @foreach($errors->all() as $error)
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    <!-- Error End -->
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered zero-configuration">
                            <thead>
                                <tr>
                                    <th>{{ __('messages.No') }}</th>
                                    <th>{{ __('messages.Name') }}</th>
                                    <th>{{ __('messages.Email') }}</th>
                                    <th>{{ __('messages.Subject') }}</th>
                                    <th>{{ __('messages.Message') }}</th>
                                    <th scope="col">{{ __('messages.Action') }}</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($contact as $key => $item)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $item->dco_name }}</td>
                                        <td>{{ $item->dco_email }}</td>
                                        <td>{{ $item->dco_subject }}</td>
                                        <td>{{ $item->dco_message }}</td>
                                        <td>
                                            <span>

                                                <a href="{{ route('contact.destroy',$item->id) }}"
                                                    class="btn mb-1 btn-danger"><i
                                                        class="fa fa-close color-danger"></i></a>

                                            </span>
                                        </td>
                                    </tr>

                                @endforeach

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>{{ __('messages.No') }}</th>
                                    <th>{{ __('messages.Name') }}</th>
                                    <th>{{ __('messages.Email') }}</th>
                                    <th>{{ __('messages.Subject') }}</th>
                                    <th>{{ __('messages.Message') }}</th>
                                    <th scope="col">{{ __('messages.Action') }}</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
