@extends('admin.master')
@section('title')
{{ __('messages.Team') }}
@endsection
@section('admincontent')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <!-- Error Start -->
                    @if($errors->any())
                        <ul class="alert alert-danger alert-dismissible fade show mt-5">
                            @foreach($errors->all() as $error)
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    <!-- Error End -->

                    <h4 class="card-title">
                        {{ __('messages.Team Table') }}
                        <button type="button" class="btn btn-primary float-right" data-toggle="modal"
                            data-target="#teammodal">{{ __('messages.Create Team') }}</button>
                    </h4>
                    <!-- modal start -->
                    <!-- Modal -->
                    <div class="modal fade" id="teammodal" tabindex="-1" role="dialog" aria-labelledby="modaltitle"
                        aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="modaltitle">{{ __('messages.Create Team') }}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <!-- Modal Body Contant Start -->
                                    <form action="{{ route('team.store') }}"
                                        class="create-team-form-wraper" method="POST" enctype="multipart/form-data">
                                        @csrf

                                        <div class="form-group">
                                            <label for="ftm_image">{{ __('messages.Team Image') }}</label>
                                            <input id="ftm_image" type="file" name="ftm_image" class="form-control"
                                                required>
                                        </div>

                                        <div class="form-group">
                                            <label for="ftm_title">{{ __('messages.Team Title') }}</label>
                                            <input id="ftm_title" type="text" name="ftm_title" class="form-control"
                                                placeholder="{{ __('messages.Your Title Here') }}" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="ftm_designation">{{ __('messages.Designation') }}</label>
                                            <input id="ftm_designation" type="text" name="ftm_designation"
                                                class="form-control" placeholder="{{ __('messages.Designation') }}">
                                        </div>

                                        <div class="form-group">
                                            <label for="ftm_social_icon1">{{ __('messages.Icon 1') }}</label>
                                            <input id="ftm_social_icon1" type="text" name="ftm_social_icon1"
                                                class="form-control" placeholder="{{ __('messages.fa fa-facebook') }}">
                                        </div>

                                        <div class="form-group">
                                            <label for="ftm_social_icon_url1">{{ __('messages.URL 1') }}</label>
                                            <input id="ftm_social_icon_url1" type="text" name="ftm_social_icon_url1"
                                                class="form-control" placeholder="{{ __('messages.www.example.com') }}">
                                        </div>

                                        <div class="form-group">
                                            <label for="ftm_social_icon2">{{ __('messages.Icon 2') }}</label>
                                            <input id="ftm_social_icon2" type="text" name="ftm_social_icon2"
                                                class="form-control" placeholder="{{ __('messages.fa fa-twitter') }}">
                                        </div>

                                        <div class="form-group">
                                            <label for="ftm_social_icon_url2">{{ __('messages.URL 2') }}</label>
                                            <input id="ftm_social_icon_url2" type="text" name="ftm_social_icon_url2"
                                                class="form-control" placeholder="{{ __('messages.www.example.com') }}">
                                        </div>

                                        <div class="form-group">
                                            <label for="ftm_social_icon3">{{ __('messages.Icon 3') }}</label>
                                            <input id="ftm_social_icon3" type="text" name="ftm_social_icon3"
                                                class="form-control" placeholder="{{ __('messages.fa fa-instagram') }}">
                                        </div>

                                        <div class="form-group">
                                            <label for="ftm_social_icon_url3">{{ __('messages.URL 3') }}</label>
                                            <input id="ftm_social_icon_url3" type="text" name="ftm_social_icon_url3"
                                                class="form-control" placeholder="{{ __('messages.www.example.com') }}">
                                        </div>

                                        <div class="form-group">
                                            <label for="ftm_social_icon4">{{ __('messages.Icon 4') }}</label>
                                            <input id="ftm_social_icon4" type="text" name="ftm_social_icon4"
                                                class="form-control" placeholder="{{ __('messages.fa fa-vk') }}">
                                        </div>

                                        <div class="form-group">
                                            <label for="ftm_social_icon_url4">{{ __('messages.URL 4') }}</label>
                                            <input id="ftm_social_icon_url4" type="text" name="ftm_social_icon_url4"
                                                class="form-control" placeholder="{{ __('messages.www.example.com') }}">
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal">{{ __('messages.Close') }}</button>
                                            <button type="submit" class="btn btn-primary">{{ __('messages.Save') }}</button>
                                        </div>
                                    </form>

                                    <!-- Modal Body Contant End -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- modal end -->
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered zero-configuration">
                            <thead>
                                <tr>
                                    <th>{{ __('messages.No') }}</th>
                                    <th>{{ __('messages.Team Image') }}</th>
                                    <th>{{ __('messages.Team Title') }}</th>
                                    <th>{{ __('messages.Designation') }}</th>
                                    <th>{{ __('messages.Action') }}</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($team as $key=> $item)

                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td><img style="width: 100px" src="{{ asset($item->dtm_image) }}" alt="img-alt" /></td>
                                        <td>{{ $item->dtm_title }}</td>
                                        <td>{{ $item->dtm_designation }}</td>
                                        <td>
                                            <span>

                                                <button type="button" class="btn mb-1 btn-primary modal-title"
                                                    data-toggle="modal" data-target="#TeamEditModal{{ $key }}">
                                                    <i class="fa fa-pencil color-muted m-r-5"></i>
                                                </button>


                                                <a href="{{ route('team.destroy',$item->id) }}"
                                                    class="btn mb-1 btn-danger"><i
                                                        class="fa fa-close color-danger"></i></a>
                                            </span>
                                        </td>
                                    </tr>

                                    <!-- team Edit Modal -->
                                    <div class="modal fade" id="TeamEditModal{{ $key }}" tabindex="-1" role="dialog"
                                        aria-labelledby="editmodaltitle" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="editmodaltitle">{{ __('messages.Edit team') }}</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <!-- Modal Body Contant Start -->
                                                    <form class="create-team-form-wraper" method="POST"
                                                        action="{{ route('team.update',$item->id) }}"
                                                        enctype="multipart/form-data" enctype="multipart/form-data">
                                                        @csrf
                                                        @method('PUT')
                                                        <div class="form-group">
                                                            <label for="ftm_image">{{ __('messages.Team Image') }}</label>
                                                            <input id="ftm_image" type="file" name="ftm_image"
                                                                class="form-control">
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="ftm_title">{{ __('messages.Team Title') }}</label>
                                                            <input id="ftm_title" type="text" name="ftm_title"
                                                                class="form-control" value="{{ $item->dtm_title }}">
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="ftm_designation">{{ __('messages.Designation') }}</label>
                                                            <input id="ftm_designation" type="text"
                                                                name="ftm_designation" class="form-control"
                                                                value="{{ $item->dtm_designation }}">
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="ftm_social_icon1">{{ __('messages.Icon 1') }}</label>
                                                            <input id="ftm_social_icon1" type="text"
                                                                name="ftm_social_icon1" class="form-control"
                                                                value="{{ $item->dtm_social_icon1 }}">
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="ftm_social_icon_url1">{{ __('messages.URL 1') }}</label>
                                                            <input id="ftm_social_icon_url1" type="text"
                                                                name="ftm_social_icon_url1" class="form-control"
                                                                value="{{ $item->dtm_social_icon_url1 }}">
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="ftm_social_icon2">{{ __('messages.Icon 2') }}</label>
                                                            <input id="ftm_social_icon2" type="text"
                                                                name="ftm_social_icon2" class="form-control"
                                                                value="{{ $item->dtm_social_icon2 }}">
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="ftm_social_icon_url2">{{ __('messages.URL 2') }}</label>
                                                            <input id="ftm_social_icon_url2" type="text"
                                                                name="ftm_social_icon_url2" class="form-control"
                                                                value="{{ $item->dtm_social_icon_url2 }}">
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="ftm_social_icon3">{{ __('messages.Icon 3') }}</label>
                                                            <input id="ftm_social_icon3" type="text"
                                                                name="ftm_social_icon3" class="form-control"
                                                                value="{{ $item->dtm_social_icon3 }}">
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="ftm_social_icon_url3">{{ __('messages.URL 3') }}</label>
                                                            <input id="ftm_social_icon_url3" type="text"
                                                                name="ftm_social_icon_url3" class="form-control"
                                                                value="{{ $item->dtm_social_icon_url3 }}">
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="ftm_social_icon4">{{ __('messages.Icon 4') }}</label>
                                                            <input id="ftm_social_icon4" type="text"
                                                                name="ftm_social_icon4" class="form-control"
                                                                value="{{ $item->dtm_social_icon4 }}">
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="ftm_social_icon_url4">{{ __('messages.URL 4') }}</label>
                                                            <input id="ftm_social_icon_url4" type="text"
                                                                name="ftm_social_icon_url4" class="form-control"
                                                                value="{{ $item->dtm_social_icon_url4 }}">
                                                        </div>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                data-dismiss="modal">{{ __('messages.Close') }}</button>
                                                            <button type="submit" class="btn btn-primary">{{ __('messages.Save') }}</button>
                                                        </div>
                                                    </form>
                                                    <!-- Modal Body Contant End -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- team Edit Modal -->
                                @endforeach

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>{{ __('messages.No') }}</th>
                                    <th>{{ __('messages.Team Image') }}</th>
                                    <th>{{ __('messages.Team Title') }}</th>
                                    <th>{{ __('messages.Designation') }}</th>
                                    <th>{{ __('messages.Action') }}</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
