@extends('admin.master')
@section('title')
{{ __('messages.Service') }}
@endsection
@section('admincontent')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <!-- Error Start -->
                    @if($errors->any())
                        <ul class="alert alert-danger alert-dismissible fade show mt-5">
                            @foreach($errors->all() as $error)
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    <!-- Error End -->

                    <h4 class="card-title">
                        {{ __('messages.Service Table') }}
                        <button type="button" class="btn btn-primary float-right" data-toggle="modal"
                            data-target="#servicemodal">{{ __('messages.Create Service') }}</button>
                    </h4>
                    <!-- modal start -->
                    <!-- Modal -->
                    <div class="modal fade" id="servicemodal" tabindex="-1" role="dialog" aria-labelledby="modaltitle"
                        aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="modaltitle">{{ __('messages.Create Service') }}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <!-- Modal Body Contant Start -->
                                    <form action="{{ route('service.store') }}"
                                        class="create-service-form-wraper" method="POST" enctype="multipart/form-data">
                                        @csrf

                                        <div class="form-group">
                                            <label for="fser_image">{{ __('messages.Service Image') }}</label>
                                            <input id="fser_image" type="file" name="fser_image" class="form-control"
                                                required>
                                        </div>

                                        <div class="form-group">
                                            <label for="fser_title">{{ __('messages.Service Title') }}</label>
                                            <input id="fser_title" type="text" name="fser_title" class="form-control"
                                                placeholder="Your Title Here" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="fser_paragraph">{{ __('messages.Paragraph') }}</label>
                                            <textarea name="fser_paragraph" id="fser_paragraph" cols="1" rows="3"
                                                class="form-control">{{ __('messages.mext of the printing and has been type setting industry. Lorem bee the') }}
                                                        </textarea>
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal">{{ __('messages.Close') }}</button>
                                            <button type="submit" class="btn btn-primary">{{ __('messages.Save') }}</button>
                                        </div>
                                    </form>

                                    <!-- Modal Body Contant End -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- modal end -->
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered zero-configuration">
                            <thead>
                                <tr>
                                    <th>{{ __('messages.No') }}</th>
                                    <th>{{ __('messages.Service Image') }}</th>
                                    <th>{{ __('messages.Service Title') }}</th>
                                    <th>{{ __('messages.Paragraph') }}</th>
                                    <th>{{ __('messages.Action') }}</th>
                                </tr>
                            </thead>
                            <tbody>


                                @foreach($service as $key=> $item)

                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td><img style="width: 50px" src="{{ asset($item->dser_image) }}" alt="img-alt" /></td>
                                        <td>{{ $item->dser_title }}</td>
                                        <td>{{ $item->dser_paragraph }}</td>
                                        <td>
                                            <span>

                                                <button type="button" class="btn mb-1 btn-primary modal-title"
                                                    data-toggle="modal" data-target="#ServiceEditModal{{ $key }}">
                                                    <i class="fa fa-pencil color-muted m-r-5"></i>
                                                </button>


                                                <a href="{{ route('service.destroy',$item->id) }}"
                                                    class="btn mb-1 btn-danger"><i
                                                        class="fa fa-close color-danger"></i></a>
                                            </span>
                                        </td>
                                    </tr>

                                    <!-- service Edit Modal -->
                                    <div class="modal fade" id="ServiceEditModal{{ $key }}" tabindex="-1" role="dialog"
                                        aria-labelledby="editmodaltitle" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="editmodaltitle">{{ __('messages.Edit service') }}</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <!-- Modal Body Contant Start -->
                                                    <form class="create-service-form-wraper" method="POST"
                                                        action="{{ route('service.update',$item->id) }}"
                                                        enctype="multipart/form-data" enctype="multipart/form-data">
                                                        @csrf
                                                        @method('PUT')
                                                        <div class="form-group">
                                                            <label for="fser_image">{{ __('messages.Service Image') }}</label>
                                                            <input id="fser_image" type="file" name="fser_image"
                                                                class="form-control">
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="fser_title">{{ __('messages.Service Title') }}</label>
                                                            <input id="fser_title" type="text" name="fser_title"
                                                                class="form-control" value="{{ $item->dser_title }}">
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="fser_paragraph">{{ __('messages.Paragraph') }}</label>
                                                            <input id="fser_paragraph" type="text" name="fser_paragraph"
                                                                class="form-control"
                                                                value="{{ $item->dser_paragraph }}">
                                                        </div>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                data-dismiss="modal">{{ __('messages.Close') }}</button>
                                                            <button type="submit" class="btn btn-primary">{{ __('messages.Save') }}</button>
                                                        </div>
                                                    </form>
                                                    <!-- Modal Body Contant End -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- service Edit Modal -->
                                @endforeach

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>{{ __('messages.No') }}</th>
                                    <th>{{ __('messages.Service Image') }}</th>
                                    <th>{{ __('messages.Service Title') }}</th>
                                    <th>{{ __('messages.Paragraph') }}</th>
                                    <th>{{ __('messages.Action') }}</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
