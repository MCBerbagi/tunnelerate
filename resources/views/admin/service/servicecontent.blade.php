@extends('admin.master')
@section('title')
{{ __('messages.Service Content') }}
@endsection
@section('admincontent')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">{{ __('messages.Content Section') }}</h4>

                    <!-- Error Start -->
                    @if($errors->any())
                        <ul class="alert alert-danger alert-dismissible fade show mt-5">
                            @foreach($errors->all() as $error)
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    <!-- Error End -->

                    <div class="row">
                        @if (!empty($servicecontent->dserc_big_image) or !empty($servicecontent->dserc_brand_image))
                            <div class="col-md-8">
                        @else
                            <div class="col-md-12">
                        @endif

                        @if(isset($servicecontent))


                            <form
                                action="{{ route('servicecontent.update',$servicecontent->id) }}"
                                class="servicecontent-section-wraper-form" enctype="multipart/form-data" method="POST">

                                @csrf
                                @method('Put')

                                <div class="form-group">
                                    <label for="fserc_sec_title">{{ __('messages.Section Title') }}</label>
                                    <input id="fserc_sec_title" type="text" name="fserc_sec_title" class="form-control"
                                        value="{{ $servicecontent->dserc_sec_title }}">
                                </div>

                                <div class="form-group">
                                    <label for="fserc_sec_title_2">{{ __('messages.Section Title 2') }}</label>
                                    <textarea id="fserc_sec_title_2" type="text" name="fserc_sec_title_2"
                                        class="form-control"
                                        rows="4">{{ $servicecontent->dserc_sec_title_2 }}</textarea>
                                </div>

                                <div class="form-group">
                                    <label for="fserc_big_image">{{ __('messages.Background Image') }}</label>
                                    <input id="fserc_big_image" type="file" name="fserc_big_image" class="form-control"
                                        value="{{ $servicecontent->dserc_big_image }}">
                                </div>

                                <button type="submit" class="btn btn-primary">{{ __('messages.Update') }}</button>
                                <a href="{{ route('servicecontent.destroy',$servicecontent->id ) }}"
                                    class="btn mb-1 btn-danger">{{ __('messages.Delete') }}</a>

                            </form>

                        @else

                            <form action="{{ route('servicecontent.store') }}"
                                class="servicecontent-section-wraper-form" enctype="multipart/form-data" method="POST">
                                @csrf

                                <div class="form-group">
                                    <label for="fserc_sec_title">{{ __('messages.Section Title') }}</label>
                                    <input id="fserc_sec_title" type="text" name="fserc_sec_title" class="form-control"
                                        placeholder="{{ __('messages.Your Title Here') }}" required>
                                </div>

                                <div class="form-group">
                                    <label for="fserc_sec_title_2">{{ __('messages.Section Title 2') }}</label>
                                    <textarea id="fserc_sec_title_2" type="text" name="fserc_sec_title_2"
                                        class="form-control" rows="4">{{ __('messages.Your Second Title Here') }}</textarea>
                                </div>

                                <div class="form-group">
                                    <label for="fserc_big_image">{{ __('messages.Background Image') }}</label>
                                    <input id="fserc_big_image" type="file" name="fserc_big_image" class="form-control"
                                        required>
                                </div>

                                <button type="submit" class="btn btn-primary">{{ __('messages.Create') }}</button>

                            </form>

                        @endif

                    </div>

                    @if(!empty($servicecontent->dserc_big_image))

                        <div class="col-md-4">

                            @if(!empty($servicecontent->dserc_big_image))
                                <div class="servicecontent-right-image">
                                    <div class="servicecontent-img mb-5">
                                        <h5>{{ __('messages.Service BG Image') }}</h5>

                                        <img class="img-fluid" src="{{ asset($servicecontent->dserc_big_image) }}"
                                            alt="img" />
                                    </div>


                                </div>
                            @endif

                        </div>


                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
@endsection
