<!DOCTYPE html>
<html class="h-100" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    @if(App\Models\General::count() > 0)
        @foreach(App\Models\General::latest()->get() as $item)

            @if(!empty( $item->dsite_title ))

                <title>{{ $item->dsite_title }} </title>
            @else

                <title>{{ __('messages.Merox - Business Management System') }}</title>
            @endif
        @endforeach
    @else
        <title>{{ __('messages.Merox - Business Management System') }}</title>
    @endif

    <!-- Favicon icon -->

    @if(App\Models\General::count() > 0)
        @foreach(App\Models\General::latest()->get() as $item)
            @if(!empty( $item->dfavicon_icon ))

                <link rel="icon" type="image/png" sizes="16x16" href="{{ asset( $item->dfavicon_icon ) }}">
            @else

                <link rel="icon" type="image/png" sizes="16x16"
                    href="{{ asset('assets/img/favicon.png') }}">
            @endif
        @endforeach
    @else
        <link rel="icon" type="image/png" sizes="16x16"
            href="{{ asset('assets/img/favicon.png') }}">
    @endif

    <!-- Custom Stylesheet -->
    <link
        href="{{ asset('assets/admin/icons/simple-line-icons/css/simple-line-icons.css') }}"
        rel="stylesheet">
    <link href="{{ asset('assets/admin/icons/font-awesome/css/font-awesome.min.css') }}"
        rel="stylesheet">
    <link href="{{ asset('assets/admin/icons/themify-icons/themify-icons.css') }}"
        rel="stylesheet">
    <link href="{{ asset('assets/admin/icons/linea-icons/linea.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/admin/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/admin/css/metisMenu.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/admin/css/summernote.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/admin/css/bootstrap-tagsinput.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/admin/css/adminmaster.css') }}" rel="stylesheet">
</head>

<body>


    <!--*******************
Preloader start
********************-->
    <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>
    <!--*******************
Preloader end
********************-->
