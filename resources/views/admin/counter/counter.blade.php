@extends('admin.master')
@section('title')
{{ __('messages.counter') }}
@endsection
@section('admincontent')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <!-- Error Start -->
                    @if($errors->any())
                        <ul class="alert alert-danger alert-dismissible fade show mt-5">
                            @foreach($errors->all() as $error)
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    <!-- Error End -->

                    <h4 class="card-title">
                        {{ __('messages.Counter Table') }}
                        <button type="button" class="btn btn-primary float-right" data-toggle="modal"
                            data-target="#countermodal">{{ __('messages.Create Counter') }}</button>
                    </h4>
                    <!-- modal start -->
                    <!-- Modal -->
                    <div class="modal fade" id="countermodal" tabindex="-1" role="dialog" aria-labelledby="modaltitle"
                        aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="modaltitle">{{ __('messages.Create Counter') }}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <!-- Modal Body Contant Start -->
                                    <form action="{{ route('counter.store') }}"
                                        class="create-counter-form-wraper" method="POST" enctype="multipart/form-data">
                                        @csrf

                                        <div class="form-group">
                                            <label for="fc_icon">{{ __('messages.Icon') }}</label>
                                            <input id="fc_icon" type="file" name="fc_icon" class="form-control"
                                                required>
                                        </div>

                                        <div class="form-group">
                                            <label for="fc_number">{{ __('messages.Count Number') }}</label>
                                            <input id="fc_number" type="text" name="fc_number" class="form-control"
                                                placeholder="{{ __('messages.10,000') }}" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="fc_title">{{ __('messages.Title') }}</label>
                                            <input id="fc_title" type="text" name="fc_title" class="form-control"
                                            placeholder="{{ __('messages.Your Title Here') }}">
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal">{{ __('messages.Close') }}</button>
                                            <button type="submit" class="btn btn-primary">{{ __('messages.Save') }}</button>
                                        </div>
                                    </form>

                                    <!-- Modal Body Contant End -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- modal end -->
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered zero-configuration">
                            <thead>
                                <tr>
                                    <th>{{ __('messages.No') }}</th>
                                    <th>{{ __('messages.Icon') }}</th>
                                    <th>{{ __('messages.Count Number') }}</th>
                                    <th>{{ __('messages.Title') }}</th>
                                    <th>{{ __('messages.Action') }}</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($counter as $key=> $item)

                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td><img style="width: 50px;height:50px" src="{{ asset($item->dc_icon) }}" alt="img-alt" /></td>
                                        <td>{{ $item->dc_number }}</td>
                                        <td>{{ $item->dc_title }}</td>
                                        <td>
                                            <span>

                                                <button type="button" class="btn mb-1 btn-primary modal-title"
                                                    data-toggle="modal" data-target="#CounterEditModal{{ $key }}">
                                                    <i class="fa fa-pencil color-muted m-r-5"></i>
                                                </button>


                                                <a href="{{ route('counter.destroy',$item->id) }}"
                                                    class="btn mb-1 btn-danger"><i
                                                        class="fa fa-close color-danger"></i></a>
                                            </span>
                                        </td>
                                    </tr>

                                    <!-- counter Edit Modal -->
                                    <div class="modal fade" id="CounterEditModal{{ $key }}" tabindex="-1" role="dialog"
                                        aria-labelledby="editmodaltitle" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="editmodaltitle">{{ __('messages.Edit counter') }}</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <!-- Modal Body Contant Start -->
                                                    <form class="create-counter-form-wraper" method="POST"
                                                        action="{{ route('counter.update',$item->id) }}"
                                                        enctype="multipart/form-data" enctype="multipart/form-data">
                                                        @csrf
                                                        @method('PUT')
                                                        <div class="form-group">
                                                            <label for="fc_icon">{{ __('messages.Icon') }}</label>
                                                            <input id="fc_icon" type="file" name="fc_icon"
                                                                class="form-control">
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="fc_number">{{ __('messages.Count Number') }}</label>
                                                            <input id="fc_number" type="text" name="fc_number"
                                                                class="form-control" value="{{ $item->dc_number }}">
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="fc_title">{{ __('messages.Title') }}</label>
                                                            <input id="fc_title" type="text" name="fc_title"
                                                                class="form-control" value="{{ $item->dc_title }}">
                                                        </div>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                data-dismiss="modal">{{ __('messages.Close') }}</button>
                                                            <button type="submit" class="btn btn-primary">{{ __('messages.Save') }}</button>
                                                        </div>
                                                    </form>
                                                    <!-- Modal Body Contant End -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- counter Edit Modal -->
                                @endforeach

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>{{ __('messages.No') }}</th>
                                    <th>{{ __('messages.Icon') }}</th>
                                    <th>{{ __('messages.Count Number') }}</th>
                                    <th>{{ __('messages.Title') }}</th>
                                    <th>{{ __('messages.Action') }}</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
