<div class="footer">
    <div class="copyright">
        @if(App\Models\General::count() > 0)
            @foreach(App\Models\General::latest()->get() as $item)
                @if(!empty( $item->dfooter_copyright ))
                    {!! $item->dfooter_copyright !!}
                @else

                    <p>{{ __('messages.Copyright') }} &copy; <a target="_blank" href="https://themeforest.net/user/theme_group">{{ __('messages.theme_group') }}</a></p>
                @endif
            @endforeach
        @else
        <p>{{ __('messages.Copyright') }} &copy; <a target="_blank" href="https://themeforest.net/user/theme_group">{{ __('messages.theme_group') }}</a></p>
        @endif
    </div>
</div>
</div>
