@extends('admin.master')
@section('title')
{{ __('messages.Cta') }}
@endsection
@section('admincontent')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">{{ __('messages.Cta Section') }}</h4>

                    <!-- Error Start -->
                    @if($errors->any())
                        <ul class="alert alert-danger alert-dismissible fade show mt-5">
                            @foreach($errors->all() as $error)
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    <!-- Error End -->

                    <div class="row">
                        @if(!empty($cta->dcta_big_image))
                            <div class="col-md-8">
                            @else
                                <div class="col-md-12">
                        @endif

                        @if(isset($cta))


                            <form action="{{ route('cta.update',$cta->id) }}"
                                class="cta-section-wraper-form" enctype="multipart/form-data" method="POST">

                                @csrf
                                @method('Put')

                                <div class="form-group">
                                    <label for="fcta_title">{{ __('messages.Title') }}</label>
                                    <input id="fcta_title" type="text" name="fcta_title" class="form-control"
                                        value="{{ $cta->dcta_title }}">
                                </div>

                                <div class="form-group">
                                    <label for="fcta_title_2">{{ __('messages.Title 2') }}</label>
                                    <textarea id="fcta_title_2" type="text" name="fcta_title_2" class="form-control"
                                        rows="4">{{ $cta->dcta_title_2 }}</textarea>
                                </div>

                                <div class="form-group">
                                    <label for="fcta_btn">{{ __('messages.Button Text') }}</label>
                                    <input id="fcta_btn" type="text" name="fcta_btn" class="form-control"
                                        value="{{ $cta->dcta_btn }}">
                                </div>

                                <div class="form-group">
                                    <label for="fcta_url">{{ __('messages.Button URL') }}</label>
                                    <input id="fcta_url" type="text" name="fcta_url" class="form-control"
                                        value="{{ $cta->dcta_url }}">
                                </div>

                                <div class="form-group">
                                    <label for="fcta_big_image">{{ __('messages.Cta BG Image') }}</label>
                                    <input id="fcta_big_image" type="file" name="fcta_big_image" class="form-control"
                                        value="{{ $cta->dcta_big_image }}">
                                </div>

                                <button type="submit" class="btn btn-primary">{{ __('messages.Update') }}</button>
                                <a href="{{ route('cta.destroy',$cta->id ) }}"
                                    class="btn mb-1 btn-danger">{{ __('messages.Delete') }}</a>

                            </form>

                        @else

                            <form action="{{ route('cta.store') }}" class="cta-section-wraper-form"
                                enctype="multipart/form-data" method="POST">
                                @csrf

                                <div class="form-group">
                                    <label for="fcta_title">{{ __('messages.Title') }}</label>
                                    <input id="fcta_title" type="text" name="fcta_title" class="form-control"
                                        placeholder="{{ __('messages.do you have any project ?') }}" required>
                                </div>

                                <div class="form-group">
                                    <label for="fcta_title_2">{{ __('messages.Title 2') }}</label>
                                    <textarea id="fcta_title_2" type="text" name="fcta_title_2" class="form-control"
                                        rows="4">{{ __('messages.let’s talk about business soluations with us') }}</textarea>
                                </div>

                                <div class="form-group">
                                    <label for="fcta_btn">{{ __('messages.Button Text') }}</label>
                                    <input id="fcta_btn" type="text" name="fcta_btn" class="form-control"
                                        placeholder="{{ __('messages.join with us') }}">
                                </div>

                                <div class="form-group">
                                    <label for="fcta_url">{{ __('messages.Button URL') }}</label>
                                    <input id="fcta_url" type="text" name="fcta_url" class="form-control"
                                        placeholder="{{ __('messages.www.example.com') }}">
                                </div>

                                <div class="form-group">
                                    <label for="fcta_big_image">{{ __('messages.Cta BG Image') }}</label>
                                    <input id="fcta_big_image" type="file" name="fcta_big_image" class="form-control"
                                        required>
                                </div>

                                <button type="submit" class="btn btn-primary">{{ __('messages.Create') }}</button>

                            </form>

                        @endif

                    </div>

                    @if(!empty($cta->dcta_big_image))

                        <div class="col-md-4">
                            @if(!empty($cta->dcta_big_image))

                                <div class="cta-right-image">
                                    <div class="cta-img mb-5">
                                        <h5>{{ __('messages.Cta BG Image') }}</h5>
                            <img class="img-fluid" src="{{ asset($cta->dcta_big_image) }}" alt="img" />
                                    </div>

                                </div>
                            @endif

                        </div>

                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
@endsection
