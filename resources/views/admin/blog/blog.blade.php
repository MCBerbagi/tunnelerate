@extends('admin.master')
@section('title')
{{ __('messages.Blog') }}
@endsection
@section('admincontent')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">
                        {{ __('messages.Blog Table') }}
                        <button type="button" class="btn btn-primary float-right" data-toggle="modal"
                            data-target="#blogmodal">{{ __('messages.Create Blog') }}</button>
                    </h4>
                    <!-- modal start -->
                    <!-- Modal -->
                    <div class="modal fade" id="blogmodal" tabindex="-1" role="dialog" aria-labelledby="modaltitle"
                        aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="modaltitle">{{ __('messages.Create Blog') }}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <!-- Modal Body Contant Start -->
                                    <form action="{{ route('blog.store') }}" method="POST"
                                        class="create-blog-form-wraper" enctype="multipart/form-data">
                                        @csrf

                                        <div class="form-group">
                                            <label for="fblog_title">{{ __('messages.Blog Title') }}</label>
                                            <input id="fblog_title" type="text" name="fblog_title" class="form-control"
                                                placeholder="{{ __('messages.Title Here') }}" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="fblog_paragraph">{{ __('messages.Short Paragraph') }}</label>
                                            <textarea id="fblog_paragraph" type="text" name="fblog_paragraph"
                                                class="form-control"
                                                rows="4">{{ __("messages.There are many variations of passages of Lorem Ipsum available, but the majority have suffered teration some form, by injected humour, or randomised words which don't look even slightly believable.") }}</textarea>
                                        </div>

                                        <div class="form-group">
                                            <label for="fblog_long_content">{{ __('messages.Blog Details Content') }}</label>
                                            <textarea id="fblog_long_content" type="text" name="fblog_long_content"
                                                class="form-control summernote" rows="3" required></textarea>
                                        </div>

                                        <div class="form-group">
                                            <label for="fblog_tags">{{ __('messages.Post Tags') }}</label>
                                            <input id="fblog_tags" type="text" name="fblog_tags" class="form-control"
                                                data-role="tagsinput" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="fblog_image">{{ __('messages.Upload Image') }}</label>
                                            <input id="fblog_image" type="file" name="fblog_image" class="form-control"
                                                required>
                                        </div>

                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-secondary"
                                                data-dismiss="modal">{{ __('messages.Close') }}</button>
                                            <button type="submit" class="btn btn-primary">{{ __('messages.Save') }}</button>
                                        </div>

                                    </form>
                                    <!-- Modal Body Contant End -->
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- modal end -->
                    <!-- Error Start -->
                    @if($errors->any())
                        <ul class="alert alert-danger alert-dismissible fade show mt-5">
                            @foreach($errors->all() as $error)
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    <!-- Error End -->
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered zero-configuration">
                            <thead>
                                <tr>
                                    <th>{{ __('messages.No') }}</th>
                                    <th>{{ __('messages.Blog Title') }}</th>
                                    <th>{{ __('messages.Image') }}</th>
                                    <th>{{ __('messages.Date') }}</th>
                                    <th scope="col">{{ __('messages.Action') }}</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($blog as $key => $item)

                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $item->dblog_title }}</td>
                                        <td><img style="width: 100px" src="{{ asset($item->dblog_image) }}" alt="img-alt" /></td>
                                        <td>{{ Carbon\Carbon::parse($item->created_at)->Format('d M, Y') }}
                                        </td>
                                        <td>
                                            <span>

                                                <button type="button" class="btn mb-1 btn-primary modal-title"
                                                    data-toggle="modal" data-target="#BlogEditModal{{ $key }}">
                                                    <i class="fa fa-pencil color-muted m-r-5"></i>
                                                </button>


                                                <a href="{{ route('blog.destroy',$item->id) }}"
                                                    class="btn mb-1 btn-danger"><i
                                                        class="fa fa-close color-danger"></i></a>

                                            </span>
                                        </td>
                                    </tr>

                                    <!-- Hero Edit Modal -->
                                    <div class="modal fade" id="BlogEditModal{{ $key }}" tabindex="-1" role="dialog"
                                        aria-labelledby="editmodaltitle" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="editmodaltitle">{{ __('messages.Edit Slider') }}</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <!-- Modal Body Contant Start -->
                                                    <form class="create-slider-form-wraper" method="POST"
                                                        action="{{ route('blog.update',$item->id) }}"
                                                        enctype="multipart/form-data">
                                                        @csrf
                                                        @method('PUT')

                                                        <div class="form-group">
                                                            <label for="fblog_title">{{ __('messages.Blog Title') }}</label>
                                                            <input id="fblog_title" type="text" name="fblog_title"
                                                                class="form-control"
                                                                value="{{ $item->dblog_title }}">
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="fblog_paragraph">{{ __('messages.Short Paragraph') }}</label>
                                                            <textarea id="fblog_paragraph" type="text"
                                                                name="fblog_paragraph" class="form-control"
                                                                rows="4">{{ $item->dblog_paragraph }}</textarea>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="fblog_long_content">{{ __('messages.Blog Details Content') }}</label>
                                                            <textarea id="fblog_long_content" type="text"
                                                                name="fblog_long_content"
                                                                class="form-control summernote"
                                                                rows="3">{{ $item->dblog_long_content }}</textarea>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="fblog_tags">{{ __('messages.Post Tags') }}</label>
                                                            <input id="fblog_tags" type="text" name="fblog_tags"
                                                                class="form-control" data-role="tagsinput"
                                                                value="{{ $item->dblog_tags }}">
                                                        </div>


                                                        <div class="form-group">
                                                            <label for="fblog_image">{{ __('messages.Upload Image') }}</label>
                                                            <input id="fblog_image" type="file" name="fblog_image"
                                                                class="form-control" value="{{ $item->dblog_image }}">
                                                        </div>


                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-secondary"
                                                                data-dismiss="modal">{{ __('messages.Close') }}</button>
                                                            <button type="submit" class="btn btn-primary">{{ __('messages.Save') }}</button>
                                                        </div>

                                                    </form>
                                                    <!-- Modal Body Contant End -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Hero Edit Modal -->

                                @endforeach

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>{{ __('messages.No') }}</th>
                                    <th>{{ __('messages.Blog Title') }}</th>
                                    <th>{{ __('messages.Image') }}</th>
                                    <th>{{ __('messages.Date') }}</th>
                                    <th scope="col">{{ __('messages.Action') }}</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
