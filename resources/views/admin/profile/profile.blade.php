@extends('admin.master')
@section('title')
{{ __('messages.Profile') }}
@endsection
@section('admincontent')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">{{ __('messages.Profile Settings') }}</h4>

                    <!-- Error Start -->
                    @if($errors->any())
                        <ul class="alert alert-danger alert-dismissible fade show mt-5">
                            @foreach($errors->all() as $error)
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    <!-- Error End -->

                    <div class="row">

                        <div class="col-lg-5 col-xl-4">

                            <div class="card">
                                <div class="card-body">
                                    <div class="media align-items-center mb-4">
                                        @if(!empty($profile->dpr_image))

                                            <img class="img-fluid admin-big-image"
                                                src="{{ asset($profile->dpr_image) }}" alt="img" />
                                        @else

                                            <div class="admin-big-profile-style-static">
                                                {{ __('messages.AM') }}
                                            </div>

                                        @endif
                                        <div class="media-body">

                                            @if(!empty($profile->dpr_title))
                                                <h3 class="mb-0">{{ $profile->dpr_title }}</h3>
                                            @else
                                                <h3 class="mb-0">{{ __('messages.Admin Max') }}</h3>
                                            @endif

                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col">
                                            <div class="card card-profile text-center">
                                                <span class="mb-1 text-primary"><i
                                                        class="icon-social-facebook"></i></span>

                                                @if(!empty($profile->dpr_social1))
                                                    <a target="_blank" href="{{ $profile->dpr_social1 }}"
                                                        class="text-muted px-4">{{ __('messages.Following') }}</a>
                                                @else
                                                    <a href="#" class="text-muted px-4">{{ __('messages.Following') }}</a>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="card card-profile text-center">
                                                <span class="mb-1 text-warning"><i
                                                        class="icon-social-twitter"></i></span>

                                                @if(!empty($profile->dpr_social2))
                                                    <a target="_blank" href="{{ $profile->dpr_social2 }}"
                                                        class="text-muted px-4">{{ __('messages.Following') }}</a>
                                                @else
                                                    <a href="#" class="text-muted px-4">{{ __('messages.Following') }}</a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <ul class="card-profile__info">
                                        <li class="mb-1"><strong class="text-dark mr-4">{{ __('messages.Mobile') }}</strong>
                                            @if(!empty($profile->dpr_mobile))
                                                <span>{{ $profile->dpr_mobile }}</span>
                                        </li>
                                    @else
                                        <span>{{ __('messages.0123456789') }}</span></li>
                                        @endif
                                        </li>
                                        <li><strong class="text-dark mr-4">{{ __('messages.Email') }}</strong>


                                            @if(!empty($profile->dpr_email))
                                                <span>{{ $profile->dpr_email }}</span>
                                        </li>
                                    @else
                                        <span>{{ __('messages.theme_group@gmail.com') }}</span></li>
                                        @endif

                                    </ul>
                                </div>
                            </div>

                        </div>


                        <div class="col-lg-7 col-xl-8">

                            @if(isset($profile))


                                <form action="{{ route('profile.update',$profile->id) }}"
                                    class="profile-section-wraper-form" enctype="multipart/form-data" method="POST">

                                    @csrf
                                    @method('Put')
                                    <div class="form-group">
                                        <label for="fpr_title">{{ __('messages.Name') }}</label>
                                        <input id="fpr_title" type="text" name="fpr_title" class="form-control" value="{{ $profile->dpr_title }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="fpr_mobile">{{ __('messages.Mobile') }}</label>
                                        <input id="fpr_mobile" type="text" name="fpr_mobile" class="form-control" value="{{ $profile->dpr_mobile }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="fpr_email">{{ __('messages.Email') }}</label>
                                        <input id="fpr_email" type="email" name="fpr_email" class="form-control" value="{{ $profile->dpr_email }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="fpr_image">{{ __('messages.Avatar') }}</label>
                                        <input id="fpr_image" type="file" name="fpr_image" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label for="fpr_social1">{{ __('messages.Facebook') }}</label>
                                        <input id="fpr_social1" type="text" name="fpr_social1" class="form-control" value="{{ $profile->dpr_social1 }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="fpr_social2">{{ __('messages.Twitter') }}</label>
                                        <input id="fpr_social2" type="text" name="fpr_social2" class="form-control" value="{{ $profile->dpr_social2 }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="fpr_content">{{ __('messages.Section Paragraph') }}</label>
                                        <textarea id="fpr_content" type="text" name="fpr_content" class="form-control"
                                            rows="4">{{ $profile->dpr_content }}</textarea>
                                    </div>

                                    <button type="submit" class="btn btn-primary">{{ __('messages.Update') }}</button>

                                    <a href="{{ route('profile.destroy',$profile->id ) }}"
                                        class="btn mb-1 btn-danger">{{ __('messages.Delete') }}</a>
                                </form>

                            @else


                                <form action="{{ route('profile.store') }}"
                                    class="profile-section-wraper-form" enctype="multipart/form-data" method="POST">
                                    @csrf

                                    <div class="form-group">
                                        <label for="fpr_title">{{ __('messages.Name') }}</label>
                                        <input id="fpr_title" type="text" name="fpr_title" class="form-control"
                                            placeholder="{{ __('messages.Name') }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="fpr_mobile">{{ __('messages.Mobile') }}</label>
                                        <input id="fpr_mobile" type="text" name="fpr_mobile" class="form-control"
                                            placeholder="{{ __('messages.+001xxx') }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="fpr_email">{{ __('messages.Email') }}</label>
                                        <input id="fpr_email" type="email" name="fpr_email" class="form-control"
                                            placeholder="{{ __('messages.Email') }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="fpr_image">{{ __('messages.Avatar') }}</label>
                                        <input id="fpr_image" type="file" name="fpr_image" class="form-control"
                                            required>
                                    </div>

                                    <div class="form-group">
                                        <label for="fpr_social1">{{ __('messages.Facebook') }}</label>
                                        <input id="fpr_social1" type="text" name="fpr_social1" class="form-control"
                                            placeholder="{{ __('messages.www.example.com') }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="fpr_social2">{{ __('messages.Twitter') }}</label>
                                        <input id="fpr_social2" type="text" name="fpr_social2" class="form-control"
                                            placeholder="{{ __('messages.www.example.com') }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="fpr_content">{{ __('messages.Section Paragraph') }}</label>
                                        <textarea id="fpr_content" type="text" name="fpr_content" class="form-control"
                                            rows="4">{{ __("messages.content here, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum their default text, and a search") }}</textarea>
                                    </div>

                                    <button type="submit" class="btn btn-primary">{{ __('messages.Create') }}</button>

                                </form>

                            @endif

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
