@includeif('admin.admin_header_style')
    @includeif('admin.admin_header_after_content')
        @includeif('admin.admin_nave')
            <div class="content-body">

                <div class="row page-titles mx-0">
                    <div class="col p-md-0">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a
                                    href="{{ route('dashboard.index') }}">{{ __('messages.Dashboard') }}</a></li>
                            <li class="breadcrumb-item active">@yield('title')</li>
                        </ol>
                    </div>
                </div>
                <!-- row -->
                @yield('admincontent')
            </div>
            @includeif('admin.admin_footer_copyright')
                @includeif('admin.admin_footer_scripts')
