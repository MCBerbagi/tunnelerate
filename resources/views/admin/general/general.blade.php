@extends('admin.master')
@section('title')
{{ __('messages.General') }}
@endsection
@section('admincontent')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">{{ __('messages.General Settings') }}</h4>

                    <!-- Error Start -->
                    @if($errors->any())
                        <ul class="alert alert-danger alert-dismissible fade show mt-5">
                            @foreach($errors->all() as $error)
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    <!-- Error End -->

                    <div class="row">
                        <div class="col-md-12">

                            @if(isset($general))


                                <form action="{{ route('general.update',$general->id) }}"
                                    class="general-section-wraper-form" enctype="multipart/form-data" method="POST">

                                    @csrf
                                    @method('Put')


                                    <div class="form-group">
                                        <label for="ftop_logo">{{ __('messages.Top Logo') }}</label>
                                        <div class="general_image">
                                            @if(!empty($general->dtop_logo))
                                                <img class="img-fluid" src="{{ asset($general->dtop_logo) }}"
                                                    alt="img" />
                                            @endif
                                        </div>
                                        <input type="file" id="ftop_logo" name="ftop_logo" class="form-control"
                                            value="{{ $general->dtop_logo }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="ffavicon_icon">{{ __('messages.Favicon') }}</label>
                                        <div class="general_image">
                                            @if(!empty($general->dfavicon_icon))
                                                <img class="img-fluid" src="{{ asset($general->dfavicon_icon) }}"
                                                    alt="img" />
                                            @endif
                                        </div>
                                        <input type="file" id="ffavicon_icon" name="ffavicon_icon" class="form-control"
                                            value="{{ $general->dfavicon_icon }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="fsite_title">{{ __('messages.Site Title') }}</label>
                                        <input type="text" name="fsite_title" id="fsite_title" class="form-control"
                                            placeholder="{{ __('messages.Site Name') }}" value="{{ $general->dsite_title }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="fbreadcrumb_bg">{{ __('messages.Breadcrumb BG') }}</label>
                                        <div class="general_image">
                                            @if(!empty($general->dbreadcrumb_bg))
                                                <img class="img-fluid" src="{{ asset($general->dbreadcrumb_bg) }}"
                                                    alt="img" />
                                            @endif
                                        </div>
                                        <input type="file" id="fbreadcrumb_bg" name="fbreadcrumb_bg"
                                            class="form-control" value="{{ $general->dbreadcrumb_bg }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="fmap_api_key">{{ __('messages.Google Map Apikey') }}</label>
                                        <input type="text" name="fmap_api_key" id="fmap_api_key" class="form-control"
                                            placeholder="{{ __('messages.Google Map Apikey') }}" value="{{ $general->dmap_api_key }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="ffooter_copyright">{{ __('messages.Footer Copyright Text') }}</label>
                                        <input type="text" name="ffooter_copyright" id="ffooter_copyright"
                                            class="form-control" placeholder="{{ __('messages.Footer Copyright Text') }}"
                                            value="{{ $general->dfooter_copyright }}">
                                    </div>

                                    <button type="submit" class="btn btn-primary">{{ __('messages.Update') }}</button>
                                    <a href="{{ route('general.destroy',$general->id ) }}"
                                        class="btn mb-1 btn-danger">{{ __('messages.Delete') }}</a>

                                </form>
                            @else


                                <form action="{{ route('general.store') }}"
                                    class="general-section-wraper-form" enctype="multipart/form-data" method="POST">
                                    @csrf


                                    <div class="form-group">
                                        <label for="ftop_logo">{{ __('messages.Top Logo') }}</label>
                                        <input type="file" id="ftop_logo" name="ftop_logo" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label for="ffavicon_icon">{{ __('messages.Favicon') }}</label>
                                        <input type="file" id="ffavicon_icon" name="ffavicon_icon" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label for="fsite_title">{{ __('messages.Site Title') }}</label>
                                        <input type="text" name="fsite_title" id="fsite_title" class="form-control"
                                            placeholder="{{ __('messages.Site Name') }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="fbreadcrumb_bg">{{ __('messages.Breadcrumb BG') }}</label>
                                        <input type="file" id="fbreadcrumb_bg" name="fbreadcrumb_bg"
                                            class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label for="fmap_api_key">{{ __('messages.Google Map Apikey') }}</label>
                                        <input type="text" name="fmap_api_key" id="fmap_api_key" class="form-control"
                                            placeholder="{{ __('messages.Google Map Apikey') }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="ffooter_copyright">{{ __('messages.Footer Copyright Text') }}</label>
                                        <input type="text" name="ffooter_copyright" id="ffooter_copyright"
                                            class="form-control" placeholder="{{ __('messages.Footer Copyright Text') }}">
                                    </div>

                                    <button type="submit" class="btn btn-primary">{{ __('messages.Create') }}</button>

                                </form>

                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection

