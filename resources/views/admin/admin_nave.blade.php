<div class="nk-sidebar">
    <div class="nk-nav-scroll">
        <ul class="metismenu" id="menu">

            <li>
                <a href="{{ route('dashboard.index') }}" aria-expanded="false">
                    <i class="fa fa-dashboard menu-icon"></i><span class="nav-text">{{ __('messages.Dashboard') }}</span>
                </a>
            </li>

            <li>
                <a href="{{ route('general.index') }}" aria-expanded="false">
                    <i class="icon-home menu-icon"></i><span class="nav-text">{{ __('messages.General Settings') }}</span>
                </a>
            </li>

            <li>
                <a href="{{ route('mainslider.index') }}" aria-expanded="false">
                    <i class="fa fa-sliders menu-icon"></i><span class="nav-text">{{ __('messages.Main Slider') }}</span>
                </a>
            </li>

            <li>
                <a href="{{ route('about.index') }}" aria-expanded="false">
                    <i class="fa fa-user-circle-o menu-icon"></i><span class="nav-text">{{ __('messages.About') }}</span>
                </a>
            </li>

            <li>
                <a href="{{ route('counter.index') }}" aria-expanded="false">
                    <i class="icon-hourglass menu-icon"></i><span class="nav-text">{{ __('messages.Counter') }}</span>
                </a>
            </li>

            <li>
                <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                    <i class="fa fa-sitemap menu-icon"></i><span class="nav-text">{{ __('messages.Services') }}</span>
                </a>
                <ul aria-expanded="false">
                    <li><a href="{{ route('service.index') }}">{{ __('messages.Create') }}</a></li>
                    <li><a href="{{ route('servicecontent.index') }}">{{ __('messages.Content') }}</a></li>

                </ul>
            </li>

            <li>
                <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                    <i class="icon-people menu-icon"></i><span class="nav-text">{{ __('messages.Team') }}</span>
                </a>
                <ul aria-expanded="false">
                    <li><a href="{{ route('team.index') }}">{{ __('messages.Create') }}</a></li>
                    <li><a href="{{ route('teamcontent.index') }}">{{ __('messages.Content') }}</a></li>

                </ul>
            </li>

            <li>
                <a href="{{ route('cta.index') }}" aria-expanded="false">
                    <i class="icon-home menu-icon"></i><span class="nav-text">{{ __('messages.Cta') }}</span>
                </a>
            </li>

            <li>
                <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                    <i class="icon-grid menu-icon"></i><span class="nav-text">{{ __('messages.Testimonial') }}</span>
                </a>
                <ul aria-expanded="false">
                    <li><a href="{{ route('testimonial.index') }}">{{ __('messages.Create') }}</a></li>
                    <li><a href="{{ route('testimonialcontent.index') }}">{{ __('messages.Content') }}</a></li>

                </ul>
            </li>

            <li>
                <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                    <i class="icon-control-play menu-icon"></i><span class="nav-text">{{ __('messages.Video') }}</span>
                </a>
                <ul aria-expanded="false">
                    <li><a href="{{ route('video.index') }}">{{ __('messages.Create') }}</a></li>
                    <li><a href="{{ route('videocontent.index') }}">{{ __('messages.Content') }}</a></li>

                </ul>
            </li>


            <li>
                <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                    <i class="icon-grid menu-icon"></i><span class="nav-text">{{ __('messages.Blog') }}</span>
                </a>
                <ul aria-expanded="false">
                    <li><a href="{{ route('blog.index') }}">{{ __('messages.Create') }}</a></li>
                    <li><a href="{{ route('blogcontent.index') }}">{{ __('messages.Content') }}</a></li>

                </ul>
            </li>

            <li>
                <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                    <i class="icon-grid menu-icon"></i><span class="nav-text">{{ __('messages.Subscribe') }}</span>
                </a>
                <ul aria-expanded="false">
                    <li><a href="{{ route('subscribe.index') }}">{{ __('messages.E-mail List') }}</a></li>
                    <li><a href="{{ route('subscribecontent.index') }}">{{ __('messages.Content') }}</a></li>
                </ul>
            </li>

            <li>
                <a href="{{ route('client.index') }}" aria-expanded="false">
                    <i class="icon-grid menu-icon"></i><span class="nav-text">{{ __('messages.communitypartner') }}</span>
                </a>
            </li>

            <li>
                <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                    <i class="icon-grid menu-icon"></i><span class="nav-text">{{ __('messages.Footer') }}</span>
                </a>
                <ul aria-expanded="false">
                    <li><a href="{{ route('widget1.index') }}">{{ __('messages.About') }}</a></li>
                    <li><a href="{{ route('widget2.index') }}">{{ __('messages.Quick Link') }}</a></li>
                    <li><a href="{{ route('widget3.index') }}">{{ __('messages.Contact Info') }}</a></li>

                </ul>
            </li>


            <li>
                <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                    <i class="icon-grid menu-icon"></i><span class="nav-text">{{ __('messages.Contact') }}</span>
                </a>
                <ul aria-expanded="false">
                    <li><a href="{{ route('contact.index') }}">{{ __('messages.Contact') }}</a></li>
                    <li><a href="{{ route('contactinfo.index') }}">{{ __('messages.Contact Info') }}</a></li>

                </ul>
            </li>

            <li>
                <a href="{{ url('admin/optimizer') }}" aria-expanded="false">
                    <i class="icon-grid menu-icon"></i><span class="nav-text">{{ __('messages.Optimizer') }}</span>
                </a>
            </li>
            <!-- menu end-->

        </ul>
    </div>
</div>
