@extends('admin.master')
@section('title')
{{ __('messages.Video') }}
@endsection
@section('admincontent')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">{{ __('messages.Video Settings') }}</h4>

                    <!-- Error Start -->
                    @if($errors->any())
                        <ul class="alert alert-danger alert-dismissible fade show mt-5">
                            @foreach($errors->all() as $error)
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    <!-- Error End -->

                    <div class="row">
                        @if(!empty($video->dv_image))
                            <div class="col-md-8">
                            @else
                                <div class="col-md-12">
                        @endif

                        @if(isset($video))


                            <form action="{{ route('video.update',$video->id) }}"
                                class="video-section-wraper-form" enctype="multipart/form-data" method="POST">

                                @csrf
                                @method('Put')

                                <div class="form-group">
                                    <label for="fv_image">{{ __('messages.Video Image') }}</label>
                                    <input id="fv_image" type="file" name="fv_image" class="form-control"
                                        value="{{ $video->dv_image }}">
                                </div>

                                <div class="form-group">
                                    <label for="fv_url">{{ __('messages.Video URL') }}</label>
                                <input id="fv_url" type="text" name="fv_url" class="form-control" value="{{ $video->dv_url }}">
                                </div>


                                <button type="submit" class="btn btn-primary">{{ __('messages.Update') }}</button>
                                <a href="{{ route('video.destroy',$video->id ) }}"
                                    class="btn mb-1 btn-danger">{{ __('messages.Delete') }}</a>

                            </form>

                        @else


                            <form action="{{ route('video.store') }}"
                                class="video-section-wraper-form" enctype="multipart/form-data" method="POST">
                                @csrf

                                <div class="form-group">
                                    <label for="fv_image">{{ __('messages.Video Image') }}</label>
                                    <input id="fv_image" type="file" name="fv_image" class="form-control" required>
                                </div>

                                <div class="form-group">
                                    <label for="fv_url">{{ __('messages.Video URL') }}</label>
                                    <input id="fv_url" type="text" name="fv_url" class="form-control"
                                        placeholder="{{ __('messages.www.example.com') }}">
                                </div>

                                <button type="submit" class="btn btn-primary">{{ __('messages.Create') }}</button>

                            </form>

                        @endif

                    </div>
                    @if(!empty($video->dv_image))
                        <div class="col-md-4">

                            <div class="video-right-image">
                                <div class="video-img mb-5">
                                    <h5>{{ __('messages.Video Image') }}</h5>
                                    <img class="img-fluid" src="{{ asset($video->dv_image) }}" alt="img" />
                                </div>

                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
@endsection
