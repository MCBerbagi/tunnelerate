@extends('admin.master')
@section('title')
{{ __('messages.About') }}
@endsection
@section('admincontent')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">{{ __('messages.About Section') }}</h4>

                    <!-- Error Start -->
                    @if($errors->any())
                        <ul class="alert alert-danger alert-dismissible fade show mt-5">
                            @foreach($errors->all() as $error)
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    <!-- Error End -->

                    <div class="row">
                        @if(!empty($about->dab_big_image) or !empty($about->dab_brand_image))
                            <div class="col-md-8">
                            @else
                                <div class="col-md-12">
                        @endif

                        @if(isset($about))


                            <form action="{{ route('about.update',$about->id) }}"
                                class="about-section-wraper-form" enctype="multipart/form-data" method="POST">

                                @csrf
                                @method('Put')

                                <div class="form-group">
                                    <label for="fab_sec_title">{{ __('messages.Section Title') }} </label>
                                    <input id="fab_sec_title" type="text" name="fab_sec_title" class="form-control"
                                        value="{{ $about->dab_sec_title }}">
                                </div>

                                <div class="form-group">
                                    <label for="fab_sec_title_2">{{ __('messages.Section Title 2') }}</label>
                                    <textarea id="fab_sec_title_2" type="text" name="fab_sec_title_2"
                                        class="form-control" rows="4">{{ $about->dab_sec_title_2 }}</textarea></div>

                                <div class="form-group">
                                    <label for="fab_srt_paragraph">{{ __('messages.Short Paragraph') }}</label>
                                    <textarea id="fab_srt_paragraph" type="text" name="fab_srt_paragraph"
                                        class="form-control" rows="3">{{ $about->dab_srt_paragraph }}</textarea>
                                </div>


                                <div class="form-group">
                                    <label for="fab_long_paragraph">{{ __('messages.Long Paragraph') }}</label>
                                    <textarea id="fab_long_paragraph" type="text" name="fab_long_paragraph"
                                        class="form-control" rows="4">{{ $about->dab_long_paragraph }}</textarea>
                                </div>

                                <div class="form-group">
                                    <label for="fab_list_info">{{ __('messages.About List Info') }}</label>
                                    <textarea id="fab_list_info" type="text" name="fab_list_info" class="form-control"
                                        rows="6">{{ $about->dab_list_info }}</textarea>
                                </div>

                                <div class="form-group">
                                    <label for="fab_btn">{{ __('messages.Button Text') }}</label>
                                    <input id="fab_btn" type="text" name="fab_btn" class="form-control"
                                        value="{{ $about->dab_btn }}">
                                </div>

                                <div class="form-group">
                                    <label for="fab_url">{{ __('messages.Button URL') }}</label>
                                    <input id="fab_url" type="text" name="fab_url" class="form-control"
                                        value="{{ $about->dab_url }}">
                                </div>

                                <div class="form-group">
                                    <label for="fab_big_image">{{ __('messages.About Image') }}</label>
                                    <input id="fab_big_image" type="file" name="fab_big_image" class="form-control"
                                        value="{{ $about->dab_big_image }}">
                                </div>


                                <div class="form-group">
                                    <label for="fab_brand_image">{{ __('messages.Brand Image') }}</label>
                                    <input id="fab_brand_image" type="file" name="fab_brand_image" class="form-control"
                                        value="{{ $about->dab_brand_image }}">
                                </div>

                                <button type="submit" class="btn btn-primary">{{ __('messages.Update') }}</button>
                                <a href="{{ route('about.destroy',$about->id ) }}"
                                    class="btn mb-1 btn-danger">{{ __('messages.Delete') }}</a>

                            </form>


                        @else


                            <form action="{{ route('about.store') }}"
                                class="about-section-wraper-form" enctype="multipart/form-data" method="POST">
                                @csrf

                                <div class="form-group">
                                    <label for="fab_sec_title">{{ __('messages.Section Title') }}</label>
                                    <input id="fab_sec_title" type="text" name="fab_sec_title" class="form-control"
                                        placeholder="{{ __('messages.About Us') }}" required>
                                </div>

                                <div class="form-group">
                                    <label for="fab_sec_title_2">{{ __('messages.Section Title 2') }}</label>
                                    <textarea id="fab_sec_title_2" type="text" name="fab_sec_title_2"
                                        class="form-control" rows="4">{{ __('messages.about <span>merox</span> pro') }}</textarea>
                                </div>

                                <div class="form-group">
                                    <label for="fab_srt_paragraph">{{ __('messages.Short Paragraph') }}</label>
                                    <textarea id="fab_srt_paragraph" type="text" name="fab_srt_paragraph"
                                        class="form-control"
                                        rows="3">{{ __("messages.There are many variations of passages of Lorem Ipsum but the majority have suffered alteration in some form injected, or randomised words which don't look even slightly. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything") }}</textarea>
                                </div>


                                <div class="form-group">
                                    <label for="fab_long_paragraph">{{ __('messages.Long Paragraph') }}</label>
                                    <textarea id="fab_long_paragraph" type="text" name="fab_long_paragraph"
                                        class="form-control"
                                        rows="4">{{ __("messages.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever when an unknown printer took a galley of Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever when an unknown printer took a galley of") }}</textarea>
                                </div>

                                <div class="form-group">
                                    <label for="fab_list_info">{{ __('messages.About List Info') }}</label>
                                    <textarea id="fab_list_info" type="text" name="fab_list_info" class="form-control"
                                        rows="6">
<li><i class="fa fa-check"></i>{{ __('messages.There are many variations of passages of Lorem') }} </li>
<li><i class="fa fa-check"></i>{{ __('messages.Majority have suffered alteration in some') }} </li>
<li><i class="fa fa-check"></i>{{ __("messages.Sure there isn't anything embarrassing hidden in the") }} </li>
<li><i class="fa fa-check"></i>{{ __('messages.All the Lorem Ipsum generators on the Internet tend to') }} </li>
        </textarea>
                                </div>

                                <div class="form-group">
                                    <label for="fab_btn">{{ __('messages.Button Text') }}</label>
                                    <input id="fab_btn" type="text" name="fab_btn" class="form-control"
                                        placeholder="{{ __('messages.about us') }}">
                                </div>

                                <div class="form-group">
                                    <label for="fab_url">{{ __('messages.Button URL') }}</label>
                                    <input id="fab_url" type="text" name="fab_url" class="form-control"
                                        placeholder="{{ __('messages.www.example.com') }}">
                                </div>

                                <div class="form-group">
                                    <label for="fab_big_image">{{ __('messages.About Image') }}</label>
                                    <input id="fab_big_image" type="file" name="fab_big_image" class="form-control"
                                        required>
                                </div>


                                <div class="form-group">
                                    <label for="fab_brand_image">{{ __('messages.Brand Image') }}</label>
                                    <input id="fab_brand_image" type="file" name="fab_brand_image" class="form-control">
                                </div>

                                <button type="submit" class="btn btn-primary">{{ __('messages.Create') }}</button>

                            </form>

                        @endif

                    </div>

                    @if(!empty($about->dab_big_image) or !empty($about->dab_brand_image))

                        <div class="col-md-4">

                            <div class="about-right-image">
                                @if(!empty($about->dab_big_image))
                                    <div class="about-img mb-5">
                                        <h5>{{ __('messages.About Image') }}</h5>

                                        <img class="img-fluid" src="{{ asset($about->dab_big_image) }}" alt="img" />
                                    </div>

                                @endif

                                @if(!empty($about->dab_brand_image))
                                    <div class="about-icon mb-5">
                                        <h5>{{ __('messages.Brand Image') }}</h5>

                                        <img class="img-fluid" src="{{ asset($about->dab_brand_image) }}" alt="img" />
                                    </div>

                                @endif
                            </div>

                        </div>


                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
@endsection
