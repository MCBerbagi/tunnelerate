@extends('admin.master')
@section('title')
{{ __('messages.Main Slider') }}
@endsection
@section('admincontent')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">
                        {{ __('messages.Slider Table') }}
                        <button type="button" class="btn btn-primary float-right" data-toggle="modal"
                            data-target="#mainslidermodal">{{ __('messages.Create Slider') }}</button>
                    </h4>
                    <!-- modal start -->
                    <!-- Modal -->
                    <div class="modal fade" id="mainslidermodal" tabindex="-1" role="dialog"
                        aria-labelledby="modaltitle" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="modaltitle">{{ __('messages.Create Slider') }}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">

                                    <!-- Modal Body Contant Start -->
                                    <form action="{{ route('mainslider.store') }}" method="POST"
                                        class="create-pricing-form-wraper" enctype="multipart/form-data">
                                        @csrf

                                        <div class="form-group">
                                            <label for="fms_title">{{ __('messages.Title') }}</label>
                                            <input id="fms_title" type="text" name="fms_title" class="form-control"
                                                placeholder="Your First Title Here" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="fms_title2">{{ __('messages.Title 2') }}</label>
                                            <textarea id="fms_title2" type="text" name="fms_title2" class="form-control"
                                                rows="2" required>{{ __('messages.we are coming <br> with <b>merox</b> business pro.') }}
                                                        </textarea>
                                        </div>

                                        <div class="form-group">
                                            <label for="fms_paragraph">{{ __('messages.Paragraph') }}</label>
                                            <textarea id="fms_paragraph" type="text" name="fms_paragraph"
                                                class="form-control" rows="3">{{ __('messages.There are many variations of passages of Lorem Ipsum , but the majority have suffered alteration in') }}
                                                        </textarea>
                                        </div>

                                        <div class="form-group">
                                            <label for="fms_btn1">{{ __('messages.Button 1') }}</label>
                                            <input id="fms_btn1" type="text" name="fms_btn1" class="form-control"
                                                placeholder="{{ __('messages.get started') }}">
                                        </div>

                                        <div class="form-group">
                                            <label for="fms_url1">{{ __('messages.Button Url 1') }}</label>
                                            <input id="fms_url1" type="text" name="fms_url1" class="form-control"
                                                placeholder="{{ __('messages.www.example.com') }} ">
                                        </div>

                                        <div class="form-group">
                                            <label for="fms_btn2">{{ __('messages.Button 2') }}</label>
                                            <input id="fms_btn2" type="text" name="fms_btn2" class="form-control"
                                                placeholder="{{ __('messages.learn more') }}">
                                        </div>

                                        <div class="form-group">
                                            <label for="fms_url2">{{ __('messages.Button Url 2') }}</label>
                                            <input id="fms_url2" type="text" name="fms_url2" class="form-control"
                                                placeholder="{{ __('messages.www.example.com') }}">
                                        </div>


                                        <div class="form-group">
                                            <label for="fms_image">{{ __('messages.Upload Image') }}</label>
                                            <input id="fms_image" type="file" name="fms_image" class="form-control"
                                                required>
                                        </div>

                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-secondary"
                                                data-dismiss="modal">{{ __('messages.Close') }}</button>
                                            <button type="submit" class="btn btn-primary">{{ __('messages.Save') }}</button>
                                        </div>

                                    </form>


                                    <!-- Modal Body Contant End -->
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- modal end -->
                    <!-- Error Start -->
                    @if($errors->any())
                        <ul class="alert alert-danger alert-dismissible fade show mt-5">
                            @foreach($errors->all() as $error)
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    <!-- Error End -->
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered zero-configuration">
                            <thead>
                                <tr>
                                    <th>{{ __('messages.No') }}</th>
                                    <th>{{ __('messages.Name') }}</th>
                                    <th>{{ __('messages.Image') }}</th>
                                    <th scope="col">{{ __('messages.Action') }}</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($mainslider as $key => $item)

                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $item->dms_title }}</td>
                                        <td><img style="width: 50px;height:50px" src="{{ asset($item->dms_image) }}" alt="img-alt" /></td>
                                        <td>
                                            <span>

                                                <button type="button" class="btn mb-1 btn-primary modal-title"
                                                    data-toggle="modal" data-target="#PricingEditModal{{ $key }}">
                                                    <i class="fa fa-pencil color-muted m-r-5"></i>
                                                </button>


                                                <a href="{{ route('mainslider.destroy',$item->id) }}"
                                                    class="btn mb-1 btn-danger"><i
                                                        class="fa fa-close color-danger"></i></a>
                                            </span>
                                        </td>
                                    </tr>

                                    <!-- Hero Edit Modal -->
                                    <div class="modal fade" id="PricingEditModal{{ $key }}" tabindex="-1" role="dialog"
                                        aria-labelledby="editmodaltitle" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="editmodaltitle">{{ __('messages.Edit Slider') }}</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">

                                                    <!-- Modal Body Contant Start -->
                                                    <form class="create-slider-form-wraper" method="POST"
                                                        action="{{ route('mainslider.update',$item->id) }}"
                                                        enctype="multipart/form-data">
                                                        @csrf
                                                        @method('PUT')

                                                        <div class="form-group">
                                                            <label for="fms_title">{{ __('messages.Title') }}</label>
                                                            <input id="fms_title" type="text" name="fms_title"
                                                                class="form-control" value="{{ $item->dms_title }}">
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="fms_title2">{{ __('messages.Title 2') }}</label>
                                                            <textarea id="fms_title2" type="text" name="fms_title2"
                                                                class="form-control"
                                                                rows="2">{{ $item->dms_title2 }}</textarea>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="fms_paragraph">{{ __('messages.Paragraph') }}</label>
                                                            <textarea id="fms_paragraph" type="text"
                                                                name="fms_paragraph" class="form-control"
                                                                rows="3">{{ $item->dms_paragraph }}</textarea>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="fms_btn1">{{ __('messages.Button 1') }}</label>
                                                            <input id="fms_btn1" type="text" name="fms_btn1"
                                                                class="form-control" value="{{ $item->dms_btn1 }}">
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="fms_url1">{{ __('messages.Button Url 1') }}</label>
                                                            <input id="fms_url1" type="text" name="fms_url1"
                                                                class="form-control" value="{{ $item->dms_url1 }}">
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="fms_btn2">{{ __('messages.Button 2') }}</label>
                                                            <input id="fms_btn2" type="text" name="fms_btn2"
                                                                class="form-control" value="{{ $item->dms_btn2 }}">
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="fms_url2">{{ __('messages.Button Url 2') }}</label>
                                                            <input id="fms_url2" type="text" name="fms_url2"
                                                                class="form-control" value="{{ $item->dms_url2 }}">
                                                        </div>


                                                        <div class="form-group">
                                                            <label for="fms_image">{{ __('messages.Upload Image') }}</label>
                                                            <input id="fms_image" type="file" name="fms_image"
                                                                class="form-control">
                                                        </div>

                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-secondary"
                                                                data-dismiss="modal">{{ __('messages.Close') }}</button>
                                                            <button type="submit" class="btn btn-primary">{{ __('messages.Save') }}</button>
                                                        </div>

                                                    </form>
                                                    <!-- Modal Body Contant End -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Hero Edit Modal -->

                                @endforeach

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>{{ __('messages.No') }}</th>
                                    <th>{{ __('messages.Name') }}</th>
                                    <th>{{ __('messages.Image') }}</th>
                                    <th scope="col">{{ __('messages.Action') }}</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
