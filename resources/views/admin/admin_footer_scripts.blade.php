<script src="{{ asset('assets/admin/plugins/common/common.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/custom.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/settings.js') }}"></script>
<script src="{{ asset('assets/admin/js/gleek.js') }}"></script>
<script src="{{ asset('assets/admin/js/styleSwitcher.js') }}"></script>
<script src="{{ asset('assets/admin/js/summernote.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/summernote-init.js') }}"></script>
{{-- <script src="{{ asset('assets/admin/js/toastr.min.js') }}"></script>
--}}
<script src="{{ asset('assets/admin/js/bootstrap-tagsinput.js') }}"></script>
<script src="{{ asset('assets/admin/plugins/tables/js/jquery.dataTables.min.js') }}">
</script>
<script
    src="{{ asset('assets/admin/plugins/tables/js/datatable/dataTables.bootstrap4.min.js') }}">
</script>
<script
    src="{{ asset('assets/admin/plugins/tables/js/datatable-init/datatable-basic.min.js') }}">
</script>

<script src="{{ asset('assets/admin/js/sweetalert.min.js') }}"></script>

@if(Session::has('message'))
    <script>
        (function ($) {
            "use strict";

            var type = "{{ Session::get('alert-type', 'info') }}";
            switch (type) {
                case 'info':

                    swal("Good job!", "{{ Session::get('message') }}", "info", {
                        button: "OK",
                    });

                    break;

                case 'success':
                    swal("Good job!", "{{ Session::get('message') }}", "success", {
                        button: "OK",
                    });
                    break;

                case 'warning':
                    swal("Good job!", "{{ Session::get('message') }}", "warning", {
                        button: "OK",
                    });
                    break;

                case 'error':
                    swal("Good job!", "{{ Session::get('message') }}", "error", {
                        button: "OK",
                    });
                    break;
            }


        })(jQuery);

    </script>

@endif

<script>
    $('.btn-danger').on('click', function (event) {
        event.preventDefault();
        const url = $(this).attr('href');
        swal({
            title: 'Are you sure?',
            text: 'This record and it`s details will be permanantly deleted!',
            icon: 'error',
            buttons: ["Cancel", "Yes!"],
        }).then(function (value) {
            if (value) {
                window.location.href = url;
            } else {
                swal({
                    title: 'Hurrah!',
                    text: 'Your file is safe Now!',
                    icon: 'success',
                    button: "OK",
                })
            }
        });
    });

</script>
</body>

</html>
