@extends('admin.master')
@section('title')
{{ __('messages.Dashboard') }}
@endsection
@section('admincontent')
<!-- Content body start -->
<div class="container-fluid mt-3">
    <div class="row">
        <div class="col-lg-3 col-sm-6">
            <div class="card gradient-1">
                <div class="card-body">
                    <h3 class="card-title text-white">{{ __('messages.Total Subscribers') }}</h3>
                    <div class="d-inline-block">
                        <h2 class="text-white">{{ App\Models\Subscribe::count() }}</h2>
                    </div>
                    <span class="float-right display-5 opacity-5"><i class="fa fa-users"></i></span>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-sm-6">
            <div class="card gradient-2">
                <div class="card-body">
                    <h3 class="card-title text-white">{{ __('messages.Total Service') }}</h3>
                    <div class="d-inline-block">
                        <h2 class="text-white">{{ App\Models\Service::count() }}</h2>
                    </div>
                    <span class="float-right display-5 opacity-5"><i class="fa fa-sliders"></i></span>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-sm-6">
            <div class="card gradient-3">
                <div class="card-body">
                    <h3 class="card-title text-white">{{ __('messages.Total Contact') }}</h3>
                    <div class="d-inline-block">
                        <h2 class="text-white">{{ App\Models\Contact::count() }}</h2>
                    </div>
                    <span class="float-right display-5 opacity-5"><i class="fa fa-users"></i></span>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-sm-6">
            <div class="card gradient-4">
                <div class="card-body">
                    <h3 class="card-title text-white">{{ __('messages.Total Blog') }}</h3>
                    <div class="d-inline-block">
                        <h2 class="text-white">{{ App\Models\Blog::count() }}</h2>
                    </div>
                    <span class="float-right display-5 opacity-5"><i class="fa fa-bold"></i></span>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- Content body end -->
@endsection
