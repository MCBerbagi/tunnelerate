@extends('admin.master')
@section('title')
{{ __('messages.Subscribe Content') }}
@endsection
@section('admincontent')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">{{ __('messages.Content Section') }}</h4>

                    <!-- Error Start -->
                    @if($errors->any())
                        <ul class="alert alert-danger alert-dismissible fade show mt-5">
                            @foreach($errors->all() as $error)
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    <!-- Error End -->

                    <div class="row">
                        @if(!empty($subscribecontent->dsubscribeco_image))
                            <div class="col-md-8">
                            @else
                                <div class="col-md-12">
                        @endif

                        @if(isset($subscribecontent))


                            <form
                                action="{{ route('subscribecontent.update',$subscribecontent->id) }}"
                                class="subscribecontent-section-wraper-form" enctype="multipart/form-data"
                                method="POST">

                                @csrf
                                @method('Put')

                                <div class="form-group">
                                    <label for="fsubscribeco_title">{{ __('messages.Title') }}</label>
                                    <textarea id="fsubscribeco_title" type="text" name="fsubscribeco_title"
                                        class="form-control"
                                        rows="4">{{ $subscribecontent->dsubscribeco_title }}</textarea>
                                </div>

                                <div class="form-group">
                                    <label for="fsubscribeco_image">{{ __('messages.Background Image') }}</label>
                                    <input id="fsubscribeco_image" type="file" name="fsubscribeco_image"
                                        class="form-control" value="{{ $subscribecontent->dsubscribeco_image }}">
                                </div>

                                <div class="form-group">
                                    <label for="fsubscribeco_btn_title">{{ __('messages.Button Title') }}</label>
                                    <input id="fsubscribeco_btn_title" type="text" name="fsubscribeco_btn_title"
                                        class="form-control" value="{{ $subscribecontent->dsubscribeco_btn_title }}">

                                </div>

                                <button type="submit" class="btn btn-primary">{{ __('messages.Update') }}</button>
                                <a href="{{ route('subscribecontent.destroy',$subscribecontent->id ) }}"
                                    class="btn mb-1 btn-danger">{{ __('messages.Delete') }}</a>

                            </form>


                        @else


                            <form action="{{ route('subscribecontent.store') }}"
                                class="subscribecontent-section-wraper-form" enctype="multipart/form-data"
                                method="POST">
                                @csrf

                                <div class="form-group">
                                    <label for="fsubscribeco_title">{{ __('messages.Title') }}</label>
                                    <textarea id="fsubscribeco_title" type="text" name="fsubscribeco_title"
                                        class="form-control"
                                        rows="4">{{ __('messages.subscribe our newsletter<br> to get more update') }}</textarea>
                                </div>

                                <div class="form-group">
                                    <label for="fsubscribeco_image">{{ __('messages.Background Image') }}</label>
                                    <input id="fsubscribeco_image" type="file" name="fsubscribeco_image"
                                        class="form-control" required>
                                </div>

                                <div class="form-group">
                                    <label for="fsubscribeco_btn_title">{{ __('messages.Button Title') }}</label>
                                    <input id="fsubscribeco_btn_title" type="text" name="fsubscribeco_btn_title"
                                        class="form-control" placeholder="{{ __('messages.Subscribe') }}">
                                </div>

                                <button type="submit" class="btn btn-primary">{{ __('messages.Create') }}</button>

                            </form>

                        @endif

                    </div>

                    @if(!empty($subscribecontent->dsubscribeco_image))

                        <div class="col-md-4">

                            <div class="subscribecontent-right-image">
                                <div class="subscribecontent-img mb-5">
                                    <h5>{{ __('messages.Background Image') }}</h5>
                                    <img class="img-fluid" src="{{ asset($subscribecontent->dsubscribeco_image) }}"
                                        alt="img" />
                                </div>
                            </div>
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
@endsection
