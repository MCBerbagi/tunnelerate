<!--
Main wrapper start
 *-->
<div id="main-wrapper">

    <!--
Nav header start
 *-->
    <div class="nav-header">
        <div class="brand-logo">
            <a href="{{ route('dashboard.index') }}">
                <b class="logo-abbr">
                    @if(App\Models\General::count() > 0)
                        @foreach(App\Models\General::latest()->get() as $item)
                            @if(!empty( $item->dfavicon_icon ))

                                <img src="{{ asset( $item->dfavicon_icon ) }}" alt="img-alt">
                            @else

                                <img src="{{ asset('assets/img/logo-compact.png') }}"
                                    alt="img-alt">
                            @endif
                        @endforeach
                    @else
                        <img src="{{ asset('assets/img/logo-compact.png') }}" alt="img-alt">
                    @endif
                </b>
                <span class="brand-title">

                    @if(App\Models\General::count() > 0)
                        @foreach(App\Models\General::latest()->get() as $item)

                            @if(!empty( $item->dtop_logo ))

                                <img src="{{ asset( $item->dtop_logo ) }}" alt="img-alt">
                            @else

                                <img src="{{ asset('assets/img/logo-1.png') }}" alt="img-alt">
                            @endif
                        @endforeach
                    @else
                        <img src="{{ asset('assets/img/logo-1.png') }}" alt="img-alt">
                    @endif

                </span>
            </a>
        </div>
    </div>
    <!--
Nav header end
 *-->

    <!--
Header start
 *-->
    <div class="header">
        <div class="header-content clearfix">

            <div class="nav-control">
                <div class="hamburger">
                    <span class="toggle-icon"><i class="icon-menu"></i></span>
                </div>
            </div>
            <div class="header-left"></div>
            <div class="header-right">
                <ul class="clearfix">
                    <li class="icons dropdown d-none d-md-flex">
                        <a class="mb-10" href="{{ url('/') }}" target="_blank" rel="noopener noreferrer">{{ __('messages.View Site') }} <i class="fa fa-external-link"></i> </a>
                    </li>


                    <li class="icons dropdown d-none d-md-flex">
                        <a href="javascript:void(0)" class="log-user"  data-toggle="dropdown">
                            {{ Config::get('languages')[App::getLocale()]['display'] }}  <i class="fa fa-angle-down f-s-14" aria-hidden="true"></i>
                        </a>
                        <div class="drop-down dropdown-language animated fadeIn  dropdown-menu">
                            <div class="dropdown-content-body">
                                <ul>
                                    @foreach (Config::get('languages') as $lang => $language)
                                    @if ($lang != App::getLocale())
                                           <li><a href="{{ route('lang.switch', $lang) }}">{{$language['display']}}</a></li>
                                    @endif
                                @endforeach
                                </ul>
                            </div>
                        </div>
                    </li>

                    <li class="icons dropdown">
                        <div class="user-img c-pointer position-relative" data-toggle="dropdown">
                            <span class="activity active"></span>


                            @if(!empty($profile->dpr_image))

                                <img src="{{ asset($profile->dpr_image) }}" alt="img-alt">

                            @else
                                <div class="admin-profile-style-static">
                                    {{ __('messages.AM') }}
                                </div>
                            @endif


                        </div>
                        <div class="drop-down dropdown-profile animated fadeIn dropdown-menu">
                            <div class="dropdown-content-body">
                                <ul>
                                    <li>
                                        <a href="{{ route('profile.index') }}"><i
                                                class="icon-user"></i>
                                            <span>{{ __('messages.Profile') }}</span></a>
                                    </li>
                                    <hr class="my-2">
                                    <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                      document.getElementById('logout-form').submit();"><i
                                                class="icon-key"></i>{{ __('messages.Logout') }}</a>


                                        <form id="logout-form" action="{{ route('logout') }}"
                                            method="POST" class="d-none">
                                            @csrf
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
