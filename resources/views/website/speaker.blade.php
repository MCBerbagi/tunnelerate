<div id="merox-team-area" class="merox-team-area mt-100 mb-70 wow fadeInUp">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                @if(count($teamcontent) > 0)
                    @foreach($teamcontent as $itemcontent)
                        <div class="section-title">
                            <h4>Tunnelerate</h4>
                            <h3>Our Speaker</h3>
                        </div>
                    @endforeach
                @endif

            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">

            @if(count($team) > 0)
                @foreach($team as $item)
                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                        <div class="single-team-wrapper text-center pt-20">
                            <div class="team-img">
                                <img src="{{ asset( $item->dtm_image ) }}" alt="{{ $item->dtm_title }}" />
                            </div>
                            <div class="team-content our-speaker">
                                <h4>{{ $item->dtm_title }}</h4>
                                <span>{{ $item->dtm_designation }}</span>
                                <div class="team-social-box">
                                    <a href="{{ $item->dtm_social_icon_url1 }}"><i
                                            class="{{ $item->dtm_social_icon1 }}"></i></a>

                                    <a href="{{ $item->dtm_social_icon_url2 }}"><i
                                            class="{{ $item->dtm_social_icon2 }}"></i></a>

                                    <a href="{{ $item->dtm_social_icon_url3 }}"><i
                                            class="{{ $item->dtm_social_icon3 }}"></i></a>

                                    <a href="{{ $item->dtm_social_icon_url4 }}"><i
                                            class="{{ $item->dtm_social_icon4 }}"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

            @endif
        </div>
    </div>
</div>
