<!-- merox contact area start -->
<div id="merox-contact-area" class="merox-contact-area mt-100 mb-100">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="section-title">
                    <h4>Partnering with us</h4>
                    <h3>Partnering with us</h3>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xl-7 col-lg-7 col-md-12 col-sm-12 pr-30">
                <div class="merox-partner-form-area">

                    <form class="merox-contact-form" id="merox-partner-form">
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                    <small class="text-danger" id="email-error"></small>
                                    <input class="form-control" type="email" name="fco_email"
                                           placeholder="Email" id="email">
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                    <small class="text-danger" id="nama-error"></small>
                                    <input class="form-control" type="text" name="fco_name_partner"
                                           placeholder="Nama" id="nama">
                                </div>

                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <small class="text-danger" id="job-error"></small>
                                    <input class="form-control" type="text" name="fco_job"
                                           placeholder="Job" id="job">
                                </div>
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <small class="text-danger" id="company-error"></small>
                                    <input class="form-control" type="text" name="fco_company"
                                           placeholder="Company Name" id="company">
                                </div>
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <small class="text-danger" id="num-error"></small>
                                    <input class="form-control" type="text" name="fco_num"
                                           placeholder="Mobile Number" id="num">
                                </div>
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <small class="text-danger" id="linkedin-profile-error"></small>
                                    <input class="form-control" type="text" name="fco_linkedin_profile"
                                           placeholder="linkedin Profile" id="linkedin">
                                </div>
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <small class="text-danger" id="type-error"></small>
                                    <label class="col-md-12">Company Type</label>
                                    <select name="fco_type" class="form-control" placeholder="Company Type" id="type">
                                        <option value="Education">Education</option>
                                        <option value="Corporation">Corporation</option>
                                        <option value="Government">Government</option>
                                        <option value="Investment Firm">Investment Firm</option>
                                        <option value="Self Employed">Self Employed</option>
                                        <option value="Entreprenuer/Startup">Entreprenuer/Startup</option>
                                        <option value="Other">Other</option>
                                    </select>
                                </div>
                                <br>
                                <br>
                                <br>
                                <br>
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <small class="text-danger" id="companyo-error"></small>
                                    <label class="col-md-12">Company operating in</label>
                                    <select name="fco_company_operating_in" class="form-control" placeholder="Company Operating In" id="companyo">
                                        <option value="Indonesia">Indonesia</option>
                                        <option value="Singapore">Singapore</option>
                                        <option value="Malaysia">Malaysia</option>
                                        <option value="The Philippines">The Philippines</option>
                                        <option value="Thailand">Thailand</option>
                                        <option value="Vietnam">Vietnam</option>
                                        <option value="Brunei">Brunei</option>
                                        <option value="Myanmar">Myanmar</option>
                                        <option value="Cambodia">Cambodia</option>
                                        <option value="Laos">Laos</option>
                                        <option value="Timor-Leste">Timor-Leste</option>
                                        <option value="Outside SEA"> Outside SEA</option>
                                    </select>
                                </div>
                                <br>
                                <br>
                                <br>
                                <br>
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <small class="text-danger" id="investort-error"></small>
                                    <label class="col-md-12">Which type of partnership below that suits your interest?</label>
                                    <select name="fco_investor_type" class="form-control" placeholder="Company Operating In" id="investort">
                                        <option value="Mentorship and/or Speaker">Mentorship and/or Speaker</option>
                                        <option value="Service Provider (Such As - SaaS, Cloud Services, Logistics Services)">Service Provider (Such As - SaaS, Cloud Services, Logistics Services)</option>
                                        <option value="Venture Capital (VC) Partner">Venture Capital (VC) Partner</option>
                                        <option value="Tunnelerate & Government Entity's Partnership">Tunnelerate & Government Entity's Partnership</option>
                                        <option value="Educational Institution">Educational Institution</option>
                                        <option value="Media or Other Related Company">Media or Other Related Company</option>
                                        <option value="Overseas Partner">Overseas Partner</option>
                                        <option value="Other">Other</option>
                                    </select>
                                </div>
                                <br>
                                <br>
                                <br>
                                <br>
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <small class="text-danger" id="havey-error"></small>
                                    <label class="col-md-12">Have you Invested in venture</label>
                                    <select name="fco_have_you_invested_in_venture" class="form-control" placeholder="Have you Invested in venture" id="havey">
                                        <option value="Yes - as a LP">Yes - as a LP</option>
                                        <option value="Yes - as a Angel">Yes - as a Angel</option>
                                        <option value="Yes - Other">Yes - Other</option>
                                        <option value="No"></option>
                                    </select>
                                </div>
                                <br>
                                <br>
                                <br>
                                <br>
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <small class="text-danger" id="haveym-error"></small>
                                    <label class="col-md-12">Have you mentored any startup before?</label>
                                    <select name="fco_have_you_mentored_any_startup_before" class="form-control" placeholder="Have you mentored any startup before?" id="haveym">
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                </div>
                                <br>
                                <br>
                                <br>
                                <br>
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <small class="text-danger" id="howdy-error"></small>
                                    <input class="form-control" type="text" name="fco_how_do_you"
                                           placeholder="How do you prefer to be contacted?" id="howdy">
                                </div>
                                <br>
                                <br>
                                <br>
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <small class="text-danger" id="howdid-error"></small>
                                    <input class="form-control" type="text" name="fco_how_did"
                                           placeholder="How did you hear about Tunnelerate" id="howdid">
                                </div>
                            <div class="row">
                                <div class="col-xl-12">
                                    <small class="text-danger" id="message-error"></small>
                                    <button type="submit" id="submit" class="btn btn-type-1">Send Message</button>
                                </div>
                            </div>
                            <p class="text-success form-message float-left mt-15" id="success-message"> </p>
                        </div>
                    </form>

                </div>
            </div>
            <div class="col-xl-5 col-lg-5 col-md-12 col-sm-12">
                <div class="contact-map-area">
                    <div id="merox-map"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- merox google map area end -->
