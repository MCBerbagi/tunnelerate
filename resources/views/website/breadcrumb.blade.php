@if(count($general) >0)
    @foreach($general as $item)
        @if(!empty( $item->dbreadcrumb_bg ))
            <div id="merox-breadcrumb-area" class="merox-breadcrumb-area"
                style="background-image: url({{ asset( $item->dbreadcrumb_bg ) }})">
        @endif
    @endforeach
@endif


<div class=" container">
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumb-content-box">
                <h2>{{ __($title) }}</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('index') }}"
                            title="Home">{{ __('messages.Home') }}</a>
                    </li>
                    <li class="breadcrumb-item active">{{ __($title) }}</li>
                </ul>
            </div>
        </div>
    </div>
</div>
</div>
<!-- breadcrumb area end -->
