<link rel="stylesheet" href="{{ asset('assets/website/css/mainscroll.css') }}">
<link href="http://fonts.cdnfonts.com/css/avenir-lt-std" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Lato:100,400,700' rel='stylesheet' type='text/css'>
<nav class="nav-apply">
    <ul class="ul-apply">
      <li class="li-apply"><a class="a current" href="#section-1">#6 Point of View by Tunnelerate with Jafar Saifudin from MDI Ventures</a></li>
      <li class="li-apply"><a class="a" href="#section-2">#5 Point of View by Tunnelerate Benny Tjong Jaring Pangan</a></li>
      <li class="li-apply"><a class="a" href="#section-3">#4 Point of View by Tunnelerate: Riswanto - Silicon Valley Engineer turned Investor</a></li>
      <li class="li-apply"><a class="a" href="#section-4">#3 Point of View by Tunnelerate Special Edition: True Fighting Spirit by Hadi Kuncoro, CEO</a></li>
      <li class="li-apply"><a class="a" href="#section-5">#2 Point of View by Tunnelerate: People, Culture and Startup Life</a></li>
      <li class="li-apply"><a class="a" href="#section-6">The Long Journey of Entrepreneurship</a></li>
    </ul>
  </nav>

  <main class="main">
    <section id="section-1">
      <h1 style="color: rgb(33, 118, 247);">#6 Point of View by Tunnelerate with Jafar Saifudin from MDI Ventures</h1>
      <p style="color:rgb(110, 110, 110); text-align: justify">Very interesting interview with Jafar Saifuding from MDI Ventures. Share their point of view and what are the criteria for a good and attractive startup for Venture Capital. and how should startups take the journey to be even better.</p>
      <iframe src="https://anchor.fm/tunnelerate/embed/episodes/6-Point-of-View-by-Tunnelerate-with-Jafar-Saifudin-from-MDI-Ventures-e1fldht/a-a7imf14" height="100%" width="100%" frameborder="0" scrolling="no"></iframe>
    </section>
    <section id="section-2">
      <h1 style="color: rgb(33, 118, 247);">#5 Point of View by Tunnelerate Benny Tjong Jaring Pangan</h1>
      <p style="color:rgb(110, 110, 110)">A very candid interview with Founder and CEO of Jaring Pangan Indonesia, JaPang, Benny Tjong. Our Program & Partnership Associate, Rifan Kalbuadi, sit together with Benny, listening to his journey from a hospitality background person to someone with a strong passion in providing a better solutions for Indonesia farmers through entrepreneurship path.</p>
      <iframe src="https://anchor.fm/tunnelerate/embed/episodes/5-Point-of-View-by-Tunnelerate-Benny-Tjong-Jaring-Pangan-e1egkdf/a-a7e2hd1" height="100%" width="100%" frameborder="0" scrolling="no"></iframe>
    </section>
    <section id="section-3">
      <h1 style="color: rgb(33, 118, 247);">#4 Point of View by Tunnelerate: Riswanto - Silicon Valley Engineer turned Investor</h1>
      <p style="color:rgb(110, 110, 110)">Our Founding Partner, Pak Riswanto . believes in the interconnectedness between innovation, the importance of problem-solving, and willingness to try.  Coming back from Sillicon Valley a few years ago, he decided to keep giving back through several entrepreneurial journey with fellow partners.   Watch his story, interviewed by our Strategic Partnership Manager, Jatin Raisinghani, only on Tunnelerate YouTube.  Kindly watch our video for so much insights.</p>
      <iframe src="https://anchor.fm/tunnelerate/embed/episodes/4-Point-of-View-by-Tunnelerate-Riswanto---Silicon-Valley-Engineer-turned-Investor-e1dcffa/a-a79ls2p" height="100%" width="100%" frameborder="0" scrolling="no"></iframe>
    </section>
    <section id="section-4">
      <h1 style="color: rgb(33, 118, 247);">#3 Point of View by Tunnelerate Special Edition: True Fighting Spirit by Hadi Kuncoro, CEO</h1>
      <p style="color:rgb(110, 110, 110)">Hadi Kuncoro, Group CEO PowerCommerce Asia invites us to think more about allowing ourselves to be fearless and taking chances. His experience in leading different kinds of companies give so many angles of leadership, hard work, and team building.</p>
      <iframe src="https://anchor.fm/tunnelerate/embed/episodes/3-Point-of-View-by-Tunnelerate-Special-Edition-True-Fighting-Spirit-by-Hadi-Kuncoro--CEO-PowerCommerce-Asia-e1d6rfv/a-a790unj" height="100%" width="100%" frameborder="0" scrolling="no"></iframe>
    </section>
    <section id="section-5">
        <h1 style="color: rgb(33, 118, 247);">#2 Point of View by Tunnelerate: People, Culture and Startup Life</h1>
        <p style="color:rgb(110, 110, 110)">Ayunda Afifa, tells so much stories for us to learn, especially the one who's interested in startups. Click the play button to know deeply about both startups and herself through her crazy ideas. Together with our host, Jatin Raisinghani and all the good questions he asked.</p>
        <iframe src="https://anchor.fm/tunnelerate/embed/episodes/2-Point-of-View-by-Tunnelerate-People--Culture-and-Startup-Life-e1csulu/a-a77ropu" height="100%" width="100%" frameborder="0" scrolling="no"></iframe>
      </section>
      <section id="section-6">
        <h1 style="color: rgb(33, 118, 247);">The Long Journey of Entrepreneurship</h1>
        <p style="color:rgb(110, 110, 110)">Ivan Arie Sustiawan is the Founding & Managing Partner at Tunnelerate, sharing his views on supporting founders with our host, Ayunda Afifa, Partner at Tunnelerate. Tunnelerate is on a mission to accelerate the entrepreneurship passage. Our incoming bi-annual accelerator program is designed to support founders in the long run.</p>
        <iframe src="https://anchor.fm/tunnelerate/embed/episodes/The-Long-Journey-of-Entrepreneurship-e1cit2r/a-a76olp6" height="100%" width="100%" frameborder="0" scrolling="no"></iframe>
      </section>



  </main>

<script src="{{ asset('assets/website/js/hear-our-point.js') }}"></script>
