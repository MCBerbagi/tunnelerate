<!-- merox contact area start -->
<div id="merox-contact-area" class="merox-contact-area mt-100 mb-100">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="section-title">
                    <h4>Investors</h4>
                    <h3>Investors</h3>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xl-7 col-lg-7 col-md-12 col-sm-12 pr-30">
                <div class="merox-partner-form-area">

                    <form class="merox-contact-form" id="merox-invest-form">
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                    <small class="text-danger" id="email-error"></small>
                                    <input class="form-control" type="email" name="fco_email"
                                           placeholder="Email" id="email">
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                    <small class="text-danger" id="nama-error"></small>
                                    <input class="form-control" type="text" name="fco_name_invest"
                                           placeholder="Nama" id="nama">
                                </div>

                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <small class="text-danger" id="job-error"></small>
                                    <input class="form-control" type="text" name="fco_job"
                                           placeholder="Job" id="job">
                                </div>
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <small class="text-danger" id="company-error"></small>
                                    <input class="form-control" type="text" name="fco_company"
                                           placeholder="Company Name" id="company">
                                </div>
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <small class="text-danger" id="linkedin-profile-error"></small>
                                    <input class="form-control" type="text" name="fco_linkedin_profile"
                                           placeholder="linkedin Profile" id="linkedin">
                                </div>
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <small class="text-danger" id="type-error"></small>
                                    <label class="col-md-12">Company Type</label>
                                    <select name="fco_type" class="form-control" placeholder="Company Type" id="type">
                                        <option value="Education">Education</option>
                                        <option value="Corporation">Corporation</option>
                                        <option value="Government">Government</option>
                                        <option value="Investment Firm">Investment Firm</option>
                                        <option value="Self Employed">Self Employed</option>
                                        <option value="Entreprenuer/Startup">Entreprenuer/Startup</option>
                                        <option value="Other">Other</option>
                                    </select>
                                </div>
                                <br>
                                <br>
                                <br>
                                <br>
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <small class="text-danger" id="investort-error"></small>
                                    <label class="col-md-12">Target investement amount?</label>
                                    <select name="fco_target_invest" class="form-control" placeholder="Target investement amount?" id="invest">
                                        <option value="less than $50.000">less than $50.000</option>
                                        <option value="$50.001 to $100.000">$50.001 to $100.000</option>
                                        <option value="$100.001 to $250.000">$100.001 to $250.000</option>
                                        <option value="$250.001 to $500.000">$250.001 to $500.000</option>
                                        <option value="$500.001 to $1.000.000">$500.001 to $1.000.000</option>
                                        <option value=" $1.000.001 to $3.000.000"> $1.000.001 to $3.000.000</option>
                                        <option value="$3.000.001 to $5.000.000">$3.000.001 to $5.000.000</option>
                                        <option value="$5.000.001 to $10.000.000">$5.000.001 to $10.000.000</option>
                                        <option value="$10.000.001+">$10.000.001+</option>
                                    </select>
                                </div>
                                <br>
                                <br>
                                <br>
                                <br>
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <small class="text-danger" id="havey-error"></small>
                                    <label class="col-md-12">Have you Invested in venture</label>
                                    <select name="fco_have_you_invested_in_venture" class="form-control" placeholder="Have you Invested in venture" id="havey">
                                        <option value="Yes - as a LP">Yes - as a LP</option>
                                        <option value="Yes - as a Angel">Yes - as a Angel</option>
                                        <option value="Yes - Other">Yes - Other</option>
                                        <option value="No">No</option>
                                    </select>
                                </div>
                                <br>
                                <br>
                                <br>
                                <br>
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <small class="text-danger" id="howdid-error"></small>
                                    <input class="form-control" type="text" name="fco_how_did"
                                           placeholder="How did you hear about Tunnelerate" id="howdid">
                                </div>
                                <div class="row">
                                    <div class="col-xl-12">
                                        <small class="text-danger" id="message-error"></small>
                                        <button type="submit" id="submit" class="btn btn-type-1">Send Message</button>
                                    </div>
                                </div>
                                <p class="text-success form-message float-left mt-15" id="success-message"> </p>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
            <div class="col-xl-5 col-lg-5 col-md-12 col-sm-12">
                <div class="contact-map-area">
                    <div id="merox-map"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- merox google map area end -->
