<!doctype html>
<html class="no-js" lang="en-us">

<head>
{{--    <meta charset="utf-8">--}}
{{--    <meta http-equiv="x-ua-compatible" content="ie=edge">--}}
<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @if(count($general) > 0)
        @foreach($general as $item)

            @if(!empty( $item->dsite_title ))
                <title>Tunnelerate </title>
            @endif
        @endforeach
    @endif
    <meta name="viewport" content="width=device-width, initial-scale=1">


    @if(count($general) > 0)
        @foreach( $general as $item )
            @if(!empty( $item->dfavicon_icon ))
                <link rel="shortcut icon" type="image/x-icon" src="{{asset('assets/tunnel_terbaru/assets/img/logo-tunnelerate.png')}}">
        @endif
    @endforeach
@endif

<!-- css here -->
    <link rel="stylesheet" href="{{ asset('assets/website/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/website/css/meanmenu.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/website/css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/website/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/website/css/fontawesome-all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/website/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/website/css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/website/css/scrolltop.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/website/css/default.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/website/css/wedget.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/website/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/website/css/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/website/css/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/website/css/style-website.css') }}">


    <!-- Favicons -->
    <link href="{{asset('assets/tunnel_terbaru/assets/img/logo-tunnelerate.png')}}" rel="icon">
    <link href=" {{asset('assets/tunnel_terbaru/assets/img/logo-tunnelerate.png')}}" rel="tunnelerate-icon">

    <!-- Google Fonts -->
    <!--<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">-->

    <!-- Vendor CSS Files -->
    <link href="{{('assets/tunnel_terbaru/assets/vendor/aos/aos.css')}}" rel="stylesheet">
    <link href="{{('assets/tunnel_terbaru/assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{('assets/tunnel_terbaru/assets/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
    <link href="{{('assets/tunnel_terbaru/assets/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
    <link href="{{('assets/tunnel_terbaru/assets/vendor/glightbox/css/glightbox.min.css')}}" rel="stylesheet">
    <link href="{{('assets/tunnel_terbaru/assets/vendor/remixicon/remixicon.css')}}" rel="stylesheet">
    <link href="{{('assets/tunnel_terbaru/assets/vendor/swiper/swiper-bundle.min.css')}}" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="{{('assets/tunnel_terbaru/assets/css/style.css')}}" rel="stylesheet">

    {{-- <link href="http://fonts.cdnfonts.com/css/helvetica-neue-9" rel="stylesheet"> --}}
    <link href="http://fonts.cdnfonts.com/css/avenir-lt-std" rel="stylesheet">
</head>

<body>
