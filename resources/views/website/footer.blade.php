<!-- ======= Footer ======= -->
<footer id="footer">

    <div class="footer-top">
        <div class="container">
            <div class="row">


                <div class="col-md-3" style="padding-bottom: 20px;">
                    <h3><img src="{{asset('assets/tunnel_terbaru/assets/img/logo-tunnelerate.png')}}" alt=""
                        width="100px"></h3>
                <p style="font-family: 'Avenir LT Std'">
                    South Quarter Building Complex <br>
                    Jalan RA Kartini Ksb 8, Cilandak, Jakarta.<br>
                    Selatan <br>
                    +6221 5098 2659<br>
                    contact@tunnelerate.com
                </p>

                </div>


                <div class="col-md-6 ">
                    <h4 style="font-family: 'Avenir LT Std'; padding-left:30px;">Discover</h4>
                    <div class="div_footer_hypers">
                    <ul class="ul_footer_hypers">
                        <li> <a style="font-family: 'Avenir LT Std'" href="{{ route('index') }}">Home</a></li>
                        <li> <a style="font-family: 'Avenir LT Std'; margin-left: 20px;" href="{{ route('acc') }}">Our Program</a></li>
                        <li> <a style="font-family: 'Avenir LT Std'; margin-left: 20px;" href="{{ route('partner') }}">Partner with us</a>
                        </li>
                        <li> <a style="font-family: 'Avenir LT Std'; margin-left: 20px;" href="{{ route('invest') }}">Investors</a></li>
                        <li> <a style="font-family: 'Avenir LT Std'; margin-left: 20px;" href="{{ route('apply') }}">Apply</a></li>
                    </ul>
                    </div>

                    <div class="div_footer_hypers">
                    <ul class="ul_footer_hypers">
                        <li> <a style="font-family: 'Avenir LT Std'; "
                                href="https://www.instagram.com/tunnelerate/"><i class="fa fa-instagram" style="font-size:24px"></i></a></li>
                        <li> <a style="font-family: 'Avenir LT Std'; margin-left: 20px;"
                                href="https://www.linkedin.com/company/tunnelerate/"><i class="fa fa-linkedin-square" style="font-size:24px" aria-hidden="true"></i></a></li>
                        <li> <a style="font-family: 'Avenir LT Std'; margin-left: 20px;" href="https://anchor.fm/tunnelerate/"><i class="fa fa-spotify" style="font-size:24px" aria-hidden="true"></i></a>
                        </li>
                        <li> <a style="font-family: 'Avenir LT Std'; margin-left: 20px;"
                                href="https://www.youtube.com/channel/UCk-o5LFqtHmez_-txTDRUfw"><i class="fa fa-youtube-play" aria-hidden="true" style="font-size:24px"></i></a></li>
                    </ul>
                </div>
                </div>



                <div class="col-md-3 footer-contact">
                    @includeIf('website.messages')
                    <h4 style="font-family: 'Avenir LT Std'">Stay up to date on all <br> the latest Tunnelerate <br>
                        news</h4>
                    <form action="{{ route('frsubscribe.store') }}" method="post">
                        @csrf
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="text" name="nama" class="form-control" placeholder="Nama"><br>
                        <input type="email" name="email" class="form-control" placeholder="Email">

                        <br>
                        <button type="submit" style="color:#fff" class="btn btn-warning btn-sm">Subscribe Now <i class="bi bi-arrow-right"
                                style="color:#fff"></i></button>
                    </form>



                </div>

            </div>
        </div>
    </div>

    <div style="background-color:#3476b0" class="d-md-flex py-2 ">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div style="font-family: 'Avenir LT Std'; color:#fff; " class="text-center">
                &copy; Copyright <strong><span>Tunnelerate</span></strong>. All Rights Reserved v 2.1.1
            </div>
        </div>
        <div class="col-md-4"></div>

        {{-- <div class="">
            <div style="font-family: 'Avenir LT Std'; color:#fff; " class="text-center">
                &copy; Copyright <strong><span>Tunnelerate</span></strong>. All Rights Reserved v 2.1.1
            </div>
            <div class="credits">
                <!-- All the links in the footer should remain intact. -->
                <!-- You can delete the links only if you purchased the pro version. -->
                <!-- Licensing information: https://bootstrapmade.com/license/ -->
                <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/presento-bootstrap-corporate-template/ -->

            </div>

        </div> --}}
        {{-- <div style="font-family: 'Avenir LT Std'; color:#fff;" class="me-md text-left text-md-start pr-60">
            Reach us out at <strong><span>contact@tunnelerate.com</span></strong>
        </div> --}}
    </div>
</footer><!-- End Footer -->
