<div id="merox-client-slider" class="merox-client-slider pb-70">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                @if(count($teamcontent) > 0)
                    @foreach($teamcontent as $itemcontent)
                        <div class="section-title">
                            <h1 style="font-weight:1000;color: #05355f">VC Partner</h1>
                            <hr>
                        </div>
                    @endforeach
                @endif

            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xl-10 col-lg-10 col-md-12 mx-auto">
                <div class="all-client-slider-vc owl-carousel owl-theme">
                @if(count($vcpartner) > 0)
                    @foreach($vcpartner as $item )
                        <!-- single client start -->
                            <a href="{{ $item->dcli_link }}">
                                <div class="single-client">
                                    <img src="{{ asset( $item->dcli_image ) }}" alt="client-image"
                                         class="img-fluid" style="width: 200px"/>
                                </div>
                            </a>
                            <!-- single client end -->
                        @endforeach

                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
