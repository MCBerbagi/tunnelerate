    <!-- js here -->
    <script src="{{ asset('assets/website/js/modernizr-3.5.0.min.js') }}"></script>
    <script src="{{ asset('assets/website/js/jquery-3.6.0.min.js') }}"></script>
    <script src="{{ asset('assets/website/js/popper.min.js') }}"></script>
    <script src="{{ asset('assets/website/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/website/js/meanmenu.min.js') }}"></script>
    <script src="{{ asset('assets/website/js/magnific-popup.min.js') }}"></script>
    <script src="{{ asset('assets/website/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('assets/website/js/waypoints.js') }}"></script>
    <script src="{{ asset('assets/website/js/counterup.min.js') }}"></script>
    <script src="{{ asset('assets/website/js/scrolltop.js') }}"></script>
    <script src="{{ asset('assets/website/js/wow.min.js') }}"></script>
    <script src="{{ asset('assets/website/js/plugins.js') }}"></script>
    <script src="{{ asset('assets/website/js/main.js') }}"></script>
    <!-- Vendor JS Files -->
    <script src="{{asset('assets/tunnel_terbaru/assets/vendor/purecounter/purecounter.js')}}"></script>
    <script src="{{asset('assets/tunnel_terbaru/assets/vendor/aos/aos.js')}}"></script>
    <script src="{{asset('assets/tunnel_terbaru/assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('assets/tunnel_terbaru/assets/vendor/glightbox/js/glightbox.min.js')}}"></script>
    <script src="{{asset('assets/tunnel_terbaru/assets/vendor/isotope-layout/isotope.pkgd.min.js')}}"></script>
    <script src="{{asset('assets/tunnel_terbaru/assets/vendor/swiper/swiper-bundle.min.js')}}"></script>
    <script src="{{asset('assets/tunnel_terbaru/assets/vendor/php-email-form/validate.js')}}"></script>

    <!-- Template Main JS File -->
    <script src="{{asset('assets/tunnel_terbaru/assets/js/main.js')}}"></script>
{{--    <script src="{{ asset('assets/admin/js/sweetalert.min.js') }}"></script>--}}
{{--    <!-- Vendor JS Files -->--}}
{{--    <script src="{{(asset('assets/tunnel_terbaru/assets/vendor/purecounter/purecounter.js')}}"></script>--}}
{{--    <script src="{{(asset('assets/tunnel_terbaru/assets/vendor/aos/aos.js')}}"></script>--}}
{{--    <script src="{{(asset('assets/tunnel_terbaru/assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>--}}
{{--    <script src="{{(asset('assets/tunnel_terbaru/assets/vendor/glightbox/js/glightbox.min.js')}}"></script>--}}
{{--    <script src="{{(asset('assets/tunnel_terbaru/assets/vendor/isotope-layout/isotope.pkgd.min.js')}}"></script>--}}
{{--    <script src="{{(asset('assets/tunnel_terbaru/assets/vendor/swiper/swiper-bundle.min.js')}}"></script>--}}
{{--    <script src="{{(asset('assets/tunnel_terbaru/assets/vendor/php-email-form/validate.js')}}"></script>--}}

{{--    <!-- Template Main JS File -->--}}
{{--    <script src="{{('assets/tunnel_terbaru/assets/js/main.js')}}"></script>--}}

    @if(Session::has('message'))
        <script>
            (function ($) {
                "use strict";

                var type = "{{ Session::get('alert-type', 'info') }}";
                switch (type) {
                    case 'info':

                        swal("Good job!", "{{ Session::get('message') }}", "info", {
                            button: "OK",
                        });

                        break;

                    case 'success':
                        swal("Good job!", "{{ Session::get('message') }}", "success", {
                            button: "OK",
                        });
                        break;

                    case 'warning':
                        swal("Good job!", "{{ Session::get('message') }}", "warning", {
                            button: "OK",
                        });
                        break;

                    case 'error':
                        swal("Good job!", "{{ Session::get('message') }}", "error", {
                            button: "OK",
                        });
                        break;
                }


            })(jQuery);

        </script>

    @endif


    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // For Subscribe Form

        $(document).on('click', '#sub-submit', function (e) {
            e.preventDefault();

            $('#merox-email-error').text('');

            email_variable = $('#subscribe-email').val();
            $.ajax({
                url: "{{ route('frsubscribe.store') }}",
                type: "POST",
                data: {
                    fs_email: email_variable,
                },
                success: function (response) {
                    if (response) {
                        $('#subscribe-success-message').text(response.success);
                        $("#merox-subscribe-form")[0].reset();
                    }
                },
                error: function (response) {
                    $('#merox-email-error').text(response.responseJSON.errors.fs_email);
                }
            });
        });
        // For Contact Form

        $('#merox-contact-form').on('submit', function (event) {
            event.preventDefault();
            $('#name-error').text('');
            $('#email-error').text('');
            $('#subject-error').text('');
            $('#message-error').text('');

            name_variable = $('#name').val();
            email_variable = $('#email').val();
            subject_variable = $('#subject').val();
            message_variable = $('#message').val();

            $.ajax({
                url: "{{ route('frcontact.store') }}",
                type: "POST",
                data: {
                    fco_name: name_variable,
                    fco_email: email_variable,
                    fco_subject: subject_variable,
                    fco_message: message_variable,
                },
                success: function (response) {
                    if (response) {
                        $('#success-message').text(response.success);
                        $("#merox-contact-form")[0].reset();
                    }
                },
                error: function (response) {
                    $('#name-error').text(response.responseJSON.errors.fco_name);
                    $('#email-error').text(response.responseJSON.errors.fco_email);
                    $('#subject-error').text(response.responseJSON.errors.fco_subject);
                    $('#message-error').text(response.responseJSON.errors.fco_message);
                }
            });
        });
        // For Parnership Form

        $('#merox-partner-form').on('submit', function (event) {
            event.preventDefault();
            $('#nama-error').text('');
            $('#email-error').text('');
            $('#job-error').text('');
            $('#company-error').text('');
            $('#num-error').text('');
            $('#linkedin-profile-error').text('');
            $('#type-error').text('');
            $('#companyo-error').text('');
            $('#investort-error').text('');
            $('#havey-error').text('');
            $('#haveym-error').text('');
            $('#howdy-error').text('');
            $('#howdid-error').text('');

            name_variable = $('#nama').val();
            email_variable = $('#email').val();
            company_variable = $('#company').val();
            job_variable = $('#job').val();
            num_variable = $('#num').val();
            linkedin_variable = $('#linkedin').val();
            type_variable = $('#type').val();
            companyo_variable = $('#companyo').val();
            investort_variable = $('#investort').val();
            havey_variable = $('#havey').val();
            haveym_variable = $('#haveym').val();
            howdy_variable = $('#howdy').val();
            howdid_variable = $('#howdid').val();

            $.ajax({
                url: "{{ route('frpartner.store') }}",
                type: "POST",
                data: {
                    fco_name_partner: name_variable,
                    fco_email: email_variable,
                    fco_job: job_variable,
                    fco_num: num_variable,
                    fco_company: company_variable,
                    fco_linkedin_profile: linkedin_variable,
                    fco_type: type_variable,
                    fco_company_operating_in: companyo_variable,
                    fco_investor_type: investort_variable,
                    fco_have_you_invested_in_venture: havey_variable,
                    fco_have_you_mentored_any_startup_before: haveym_variable,
                    fco_how_do_you: howdy_variable,
                    fco_how_did: howdid_variable,
                },
                success: function (response) {
                    if (response) {
                        $('#success-message').text(response.success);
                        $("#merox-partner-form")[0].reset();
                    }
                },
                error: function (response) {
                    $('#nama-error').text(response.responseJSON.errors.fco_name_partner);
                    $('#email-error').text(response.responseJSON.errors.fco_email);
                    $('#job-error').text(response.responseJSON.errors.fco_job);
                    $('#num-error').text(response.responseJSON.errors.fco_num);
                    $('#company-error').text(response.responseJSON.errors.fco_company);
                    $('#linkedin-profile-error').text(response.responseJSON.errors.fco_linkedin_profile);
                    $('#companyo-error').text(response.responseJSON.errors.fco_company_operating_in);
                    $('#investort-error').text(response.responseJSON.errors.fco_investor_type);
                    $('#havey-error').text(response.responseJSON.errors.fco_have_you_invested_in_venture);
                    $('#haveym-error').text(response.responseJSON.errors.fco_have_you_mentored_any_startup_before);
                    $('#howdy-error').text(response.responseJSON.errors.fco_how_do_you);
                    $('#howdid-error').text(response.responseJSON.errors.fco_how_did);
                }
            });
        });

        //invest
        $('#merox-invest-form').on('submit', function (event) {
            event.preventDefault();
            $('#nama-error').text('');
            $('#email-error').text('');
            $('#job-error').text('');
            $('#company-error').text('');
            $('#linkedin-profile-error').text('');
            $('#type-error').text('');
            $('#investort-error').text('');
            $('#havey-error').text('');
            $('#howdid-error').text('');

            name_variable = $('#nama').val();
            email_variable = $('#email').val();
            company_variable = $('#company').val();
            job_variable = $('#job').val();
            linkedin_variable = $('#linkedin').val();
            type_variable = $('#type').val();
            investort_variable = $('#invest').val();
            havey_variable = $('#havey').val();
            howdid_variable = $('#howdid').val();

            $.ajax({
                url: "{{ route('frinvest.store') }}",
                type: "POST",
                data: {
                    fco_name_invest: name_variable,
                    fco_email: email_variable,
                    fco_job: job_variable,
                    fco_company: company_variable,
                    fco_linkedin_profile: linkedin_variable,
                    fco_type: type_variable,
                    fco_target_invest: investort_variable,
                    fco_have_you_invested_in_venture:havey_variable,
                    fco_how_did: howdid_variable,
                },
                success: function (response) {
                    if (response) {
                        $('#success-message').text(response.success);
                        $("#merox-partner-form")[0].reset();
                    }
                },
                error: function (response) {
                    $('#nama-error').text(response.responseJSON.errors.fco_name_invest);
                    $('#email-error').text(response.responseJSON.errors.fco_email);
                    $('#job-error').text(response.responseJSON.errors.fco_job);
                    $('#company-error').text(response.responseJSON.errors.fco_company);
                    $('#linkedin-profile-error').text(response.responseJSON.errors.fco_linkedin_profile);
                    $('#investort-error').text(response.responseJSON.errors.fco_target_invest);
                    $('#havey-error').text(response.responseJSON.errors.fco_have_you_invested_in_venture);
                    $('#howdid-error').text(response.responseJSON.errors.fco_how_did);
                }
            });
        });
        //Acc
        $('#merox-acc-form').on('submit', function (event) {
            event.preventDefault();
            $('#nama-error').text('');
            $('#email-error').text('');
            $('#linkedin-profile-error').text('');
            $('#url-error').text('');
            $('#company-desc-error').text('');
            $('#havey-error').text('');
            $('#companyo-error').text('');
            $('#linkedin-profile1-error').text('');
            $('#linkedin-profile2-error').text('');

            $('#dyah-error').text('');
            $('#demo-error').text('');
            $('#who_are_you_competitors-error').text('');
            $('#dya-error').text('');

            name_variable = $('#nama').val();
            email_variable = $('#email').val();
            linkedin_variable = $('#linkedin').val();
            url_variable = $('#url').val();
            company_desc_variable = $('#company_desc').val();
            isy_variable = $('#isy').val();
            companyo_variable = $('#companyo').val();
            linkedin1_variable = $('#linkedin1').val();
            linkedin2_variable = $('#linkedin2').val();

            dyah_variable = $('#dyah').val();
            demo_variable = $('#demo').val();
            way_variable = $('#who_are_you_competitors').val();
            dya_variable = $('#dya').val();
            var formData = new FormData();
            formData.append('file', $('#formFile')[0].files[0]);
            $.ajax({
                url: "{{ route('fracc.store') }}",
                type: "POST",
                data: {
                    fco_name_acc: name_variable,
                    fco_email: email_variable,
                    fco_linkedin_profile: linkedin_variable,
                    fco_url: url_variable,
                    fco_company_desc: company_desc_variable,
                    fco_incorporate: isy_variable,
                    fco_company_operating_in: companyo_variable,
                    fco_linkedin_profile1: linkedin1_variable,
                    fco_linkedin_profile2: linkedin2_variable,
                    fco_gambar: file_variable,
                    fco_do_you_have_a_working_product:dyah_variable,
                    fco_who_are_you_competitors: howdid_variable,
                    fco_do_you_have_a_working_product: howdid_variable
                },
                success: function (response) {
                    if (response) {
                        $('#success-message').text(response.success);
                        $("#merox-partner-form")[0].reset();
                    }
                },
                error: function (response) {
                    $('#nama-error').text(response.responseJSON.errors.fco_name_invest);
                    $('#email-error').text(response.responseJSON.errors.fco_email);
                    $('#job-error').text(response.responseJSON.errors.fco_job);
                    $('#company-error').text(response.responseJSON.errors.fco_company);
                    $('#linkedin-profile-error').text(response.responseJSON.errors.fco_linkedin_profile);
                    $('#investort-error').text(response.responseJSON.errors.fco_target_invest);
                    $('#havey-error').text(response.responseJSON.errors.fco_have_you_invested_in_venture);
                    $('#howdid-error').text(response.responseJSON.errors.fco_how_did);
                }
            });
        });
    </script>


    </body>

    </html>
