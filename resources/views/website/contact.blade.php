<!-- merox contact area start -->
<div id="merox-contact-area" class="merox-contact-area mt-100 mb-100">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="section-title">
                    <h4>{{ __('messages.contact us') }}</h4>
                    <h3>{{ __('messages.contact us') }}</h3>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xl-7 col-lg-7 col-md-12 col-sm-12 pr-30">
                <div class="merox-contact-form-area">

                    <form class="merox-contact-form" id="merox-contact-form">
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                    <small class="text-danger" id="name-error"></small>
                                    <input class="form-control" type="text" name="fco_name"
                                        placeholder="{{ __('messages.Name *') }}" id="name">
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                    <small class="text-danger" id="email-error"></small>
                                    <input class="form-control" type="email" name="fco_email"
                                        placeholder="{{ __('messages.E-mail *') }}" id="email">
                                </div>

                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <small class="text-danger" id="subject-error"></small>
                                    <input class="form-control" type="text" name="fco_subject"
                                        placeholder="{{ __('messages.Subject') }}" id="subject">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xl-12">
                                    <small class="text-danger" id="message-error"></small>
                                    <textarea class="form-control" name="fco_message" cols="30" rows="8"
                                        placeholder="{{ __('messages.Request') }}"
                                        id="message"></textarea>
                                    <button type="submit" id="submit" class="btn btn-type-1">Send Message</button>
                                </div>
                            </div>
                            <p class="text-success form-message float-left mt-15" id="success-message"> </p>
                        </div>
                    </form>

                </div>
            </div>
            <div class="col-xl-5 col-lg-5 col-md-12 col-sm-12">
                <div class="contact-map-area">
                    <div id="merox-map"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- merox contact area end -->
<!-- merox contact info area start -->
<div id="merox-contact-info-area" class="merox-contact-info-area mb-70">
    <div class="container">
        <div class="row">

            @if( count($contactinfo) > 0)

                @foreach( $contactinfo as $item )
                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                        <div class="contact-info-box">
                            <div class="contact-info-icon">
                                <img src="{{ asset( $item->dci_icon ) }}" alt="icon">
                            </div>
                            <ul class="contact-info">
                                {!! $item->dci_info !!}
                            </ul>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</div>
<!-- merox contact info area end -->

<!-- merox google map area start -->
@if(count($general) >0)

    @foreach($general as $item)

        @if(!empty( $item->dmap_api_key ))
            <script src="https://maps.googleapis.com/maps/api/js?key={{ $item->dmap_api_key }}"></script>
        @endif
    @endforeach
@endif
<script src="{{ asset('assets/website/js/maps.active.js') }}"></script>
<!-- merox google map area end -->
