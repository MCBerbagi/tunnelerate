<div id="merox-services-area" class="merox-services-area pt-130 pb-130">
    @if(count($servicecontent) > 0)
        @foreach( $servicecontent as $itemcontent )
        <div class="merox-services-area-img">
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-xl-4 offset-xl-1 col-lg-4 order-2 wow fadeInRight">
                        <div class="section-title lite">
                            <h4>{{ $itemcontent->dserc_sec_title }}</h4>
                            <h3>{{ $itemcontent->dserc_sec_title_2 }}</h3>
                        </div>
                    </div>
        @endforeach

    @endif
    <div class="col-xl-7 col-lg-8 wow fadeInLeft">
        <div class="services-box">
            @if(count($service) > 0)

                @foreach($service as $item)

                    <div class="single-services">
                        <img src="{{ asset( $item->dser_image ) }}" alt="{{ $item->dser_title }}">
                        <div class="services-content-box">
                            <h3>{{ $item->dser_title }}</h3>
                            <p>{{ $item->dser_paragraph }}</p>
                        </div>
                    </div>
                @endforeach

            @endif
        </div>
    </div>
</div>
</div>
</div>
