<div id="merox-video-area" class="merox-video-area pt-130 pb-130">

    @if(count($videocontent) > 0)
        @foreach( $videocontent as $itemcontent )
            <div class="merox-video-area-img"><img src="{{ asset( $itemcontent->dvideoc_big_image ) }}"
                    alt="{{ $itemcontent->dvideoc_sec_title }}"></div>
            <div class="container">
                <div class="row">
                    <div class="col-xl-4 offset-xl-1 col-lg-4 order-2 wow fadeInRight">
                        <div class="section-title lite">
                            <h4>{{ $itemcontent->dvideoc_sec_title }}</h4>
                            <h3>{{ $itemcontent->dvideoc_sec_title_2 }}</h3>
                        </div>
                    </div>
        @endforeach

    @endif
    @if(count($video) > 0)
        @foreach( $video as $item )
            <div class="col-xl-7 col-lg-8 wow fadeInLeft">
                <div class="video-box text-center position-relative">
                    <img src="{{ asset( $item->dv_image ) }}" alt="video-img">
                    <div class="videos-icon">
                        <a class="popup-video" href="{{ $item->dv_url }}"><i class="fa fa-play"></i></a>
                    </div>
                </div>
            </div>
        @endforeach

    @endif
</div>
</div>
</div>
