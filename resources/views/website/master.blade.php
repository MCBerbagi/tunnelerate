@includeIf('website.style')
    <!--[if lte ie 9]>
            <p class="browserupgrade">you are using an <strong>outdated</strong> browser. please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
	<![endif]-->

    <!-- preloaders start -->
    <div class="preloader">
        <div class="preloader-inner">
            <div class="preloader-icon">
                <span></span>
                <span></span>
            </div>
        </div>
    </div>
    <!-- preloaders end -->
    <!-- header area start -->
    @includeIf('website.header')

        <!-- header area end -->
        <!-- main area start -->
        <main>

            @yield('website-content')

        </main>
        <!-- main area end -->

        <!-- scrolltop button -->
        <div class="material-scrolltop"></div>

        @includeIf('website.script')
