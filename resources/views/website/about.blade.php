<style>
    @if(!empty($about->dab_brand_image))
    .about-img-area:before { content: url({{ asset( $about->dab_brand_image ) }});}
    @endif;
</style>

        <div id="merox-about-area" class="merox-about-area mt-100 mb-70">
        <div class="container">
            <div class="row">
                <div class="col-xl-5 col-lg-6 col-md-12 wow fadeInLeft">
                    <div class="about-img-area">
                        @if(!empty($about->dab_big_image))
                        <img src="{{ asset($about->dab_big_image) }}" alt="about" />
@endif
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 offset-xl-1 wow fadeInRight">
                    <div class="section-title">

                        @if(!empty($about->dab_sec_title))
                            <h4>{{ $about->dab_sec_title }}</h4>

                        @endif

                        @if(!empty($about->dab_sec_title_2))
                            <h3>{!! $about->dab_sec_title_2 !!}</h3>
                        @endif
                    </div>
                    <div class="about-content">
                        @if(!empty($about->dab_srt_paragraph))
                            <p>{{ $about->dab_srt_paragraph }}</p>
                        @endif

                        <ul class="about-content-list">

                            @if(!empty($about->dab_list_info))
                                {!! $about->dab_list_info !!}

                            @endif
                        </ul>
                        <a href="@if (!empty($about->dab_url)){{ $about->dab_url }}@else # @endif "
                            class="btn btn-type-4">
                            @if(!empty($about->dab_btn))
                                {{ $about->dab_btn }}
                            @endif
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
