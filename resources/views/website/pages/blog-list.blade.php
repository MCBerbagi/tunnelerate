@extends('website.master')
@section('website-content')
<!-- breadcrumb area start -->
@includeIf('website.breadcrumb', ['title' =>'messages.blog list'])
<!-- breadcrumb area end -->
<!-- merox blog-list area start -->
@includeIf('website.blog-list')
<!-- merox blog-list area end -->
<!-- merox footer area start -->
@includeIf('website.footer')
<!-- merox footer area end -->
@endsection

