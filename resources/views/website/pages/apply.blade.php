@extends('website.master')
@section('website-content')
    <!-- Simulate a smartphone / tablet -->

        <div class="col-md-6">
            @includeIf('website.messages')
            <h4 style="font-family: 'Avenir LT Std', sans-serif;">Why Tunnelerate?</h4>
            <hr>
        </div>

        <div class="col-md-6">
            <p style="font-family: 'Avenir LT Std', sans-serif;">To say that we are builders an founders ourselves sounds like an overused statement. But we cannot find other words to describe who we are. All of Tunnelerate Partners are founders and startup builders with various combined range of experience in tech. Product, operational, strategy, finance, and people & culture management aspects of building companies.
                <br><br> We are inviting you to our exciting program and let us help you scale and grow your company.
            </p>
        </div>


    <br>
    <div class="col-sm-8" id="left">


        <form action="{{ route('fracc.store') }}" method="post" enctype="multipart/form-data" accept-charset="utf-8">

            <section id="section1" class="page-section">
                <div class="form-group">
                    <input type="text" name="name" class="form-control" id="name" placeholder="Your name*" required>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="email" id="email" placeholder="Your email*" required>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="linkedin" id="subject"
                           placeholder="Your Linkedin profile*" required>
                </div>

                <div class="form-group">
                    <input type="text" class="form-control" name="other" id="subject"
                           placeholder="Other co-founder Linkedin profile*" required>
                </div>


            </section>
            <section id="section2" class="page-section">
                <div class="form-group">
                    <input type="text" class="form-control" name="company_name" id="subject" placeholder="Company name*"
                           required>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="based" id="subject" placeholder="Company based*"
                           required>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="web" id="subject" placeholder="Company website*"
                           required>
                </div>


                <div class="form-group">
                    <input type="text" class="form-control" name="desc" id="subject"
                           placeholder="Company description under 50 characters*" maxlength="50" required>
                </div>
                <div class="form-group">
                    <p>Pitchdeck</p>
                    <input type="file" class="form-control" name="pitch" id="subject"
                           placeholder="Pitch deck (pdf version)*" required>
                </div>

            </section>
            <section id="section3" class="page-section">
                <div class="form-group">
                    <input type="text" class="form-control" name="demo" id="subject"
                           placeholder="Product demo video url, if any (url and not private)*" required>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="solved" id="subject"
                           placeholder="Problem you’re trying to solve?*" required>
                </div>

                <div class="form-group">
                    <input type="text" class="form-control" name="solusi" id="subject"
                           placeholder="Solution that you provide?*" required>
                </div>

                <div class="form-group">
                    <input type="text" class="form-control" name="works" id="subject"
                           placeholder="How the product works?*" required>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="for" id="subject" placeholder="Whose the product for?*"
                           required>
                </div>

            </section>
            <section id="section4" class="page-section">
                <div class="form-group">
                    <input type="text" class="form-control" name="need" id="need"
                           placeholder="What do you need from this program?*" required>
                </div>

                <div class="form-group">
                    <input type="text" class="form-control" name="any" id="any"
                           placeholder="Anything that we need to know?*" required>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="hdyh" id="subject"
                           placeholder="How did you hear about Tunnelerate?*" required>
                </div>

                <div class="text-center">
                    <button type="submit" class="get-started-btn">Submit <i class="bi bi-arrow-right"></i></button>
                </div>
            </section>
        </form>
    </div>

    <div class="col-sm-4 card-body" id="right">
        <div class="apply-card">
            <ul id="menu-apply" class="navbar-nav ml-auto">

                <li class="nav-item-apply">
                    <a class="nav-link-apply" href="#section1">
                        Section 1
                    </a>
                </li>

                <li class="nav-item-apply">
                    <a class="nav-link-apply" href="#section2">
                        Section 2
                    </a>
                </li>

                <li class="nav-item-apply">
                    <a class="nav-link-apply" href="#section3">
                        Section 3
                    </a>
                </li>

                <li class="nav-item-apply">
                    <a class="nav-link-apply" href="#section4">
                        Section 4
                    </a>
                </li>
            </ul>
        </div>
    </div>
    </div>
    </nav>

    </div>
    {{-- @includeIf('website.footer') --}}
    <!-- merox footer area end -->
@endsection
