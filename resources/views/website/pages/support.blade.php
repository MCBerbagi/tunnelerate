@extends('website.master')
@section('website-content')
<!-- breadcrumb area start -->
{{--@includeIf('website.breadcrumb', ['title' =>'messages.team'])--}}
<!-- breadcrumb area end -->
<!-- merox team area start -->
@includeIf('website.speaker')
@includeIf('website.mentor')
@includeIf('website.communitypartner')
<!-- merox team area end -->
<!-- merox footer area start -->
@includeIf('website.footer')
<!-- merox footer area end -->
@endsection

