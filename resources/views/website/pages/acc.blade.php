@extends('website.master')
@section('website-content')
<br>
<main id="main">
    <!-- Get section -->
    <section id="get" class="d-flex">
        <div class="container" data-aos="fade-up">
            <div class="row" data-aos="fade-up" data-aos-delay="100">
                <div class="col-md-6">
                    <h4 style="font-family: 'Avenir LT Std'">Accelerator Program</h4>
                    <hr>

                </div>
                <div class="col-md-6">
                    <p style="font-family: 'Avenir LT Std'">Entrepreneurship is not easy, but as we know, far from
                        impossible. The beauty part of imagination, dreams, creativity, and persistence, is the ability
                        to re-creating a better version of our lives in the future. Tunnelerate is welcome to work
                        together with fellow founders, dreamers with a strong willingness to solve a myriad of societal
                        problems through the utilization of technologies. Our Bi-annual accelerator program is designed
                        to serve early-stage companies with market-driven products who want to build it in the long
                        haul.</p>
                </div>
                <div class="col-md-2">
                </div>
                <div class="col-md-12">
                    <img src="{{asset('assets/tunnel_terbaru/assets/img/blog/blog-recent-7.jpg')}}" alt="">
                </div>
                <div class="col-md-2">
                </div>
            </div>
        </div>
    </section>
    <!-- end Get Section -->

    <!-- section partners -->
    <section id="our-philosophy" class="our-philosophy">
        <div class="container" data-aos="fade-up">
            <h5 style="font-family: 'Avenir LT Std'">Our philosophy</h5>
            <div class="row" data-aos="fade-up" data-aos-delay="100">
                <div class="col-md-4">
                    <hr>
                    <h5 style="font-family: 'Avenir LT Std'">Day 1 and beyond</h5>
                    <p style="text-align: justify; font-family: 'Avenir LT Std'">We aim to shape the company of tomorrow
                        from day one. Building something new is really hard and we want to help others to avoid falling
                        into the same pitfalls that we had encountered previously. Thus, partnering with us from the
                        very beginning will give an outlasting impact to the community. </p>
                </div>

                <div class="col-md-4">
                    <hr>
                    <h5 style="font-family: 'Avenir LT Std'">Huge problem, small steps</h5>
                    <p style="text-align: justify; font-family: 'Avenir LT Std'">We are keen to partner with courageous
                        Indonesian founders that are looking to solve a myriad of societal problems using unique
                        methods. We love partnering with someone that has a clear and concise plan to handle the
                        problems one at a time.</p>
                </div>

                <div class="col-md-4">
                    <hr>
                    <h5 style="font-family: 'Avenir LT Std'">Painkillers, not vitamins</h5>
                    <p style="text-align: justify; font-family: 'Avenir LT Std'">For us, it does not have to be
                        something truly advanced or innovative. must-have, we love to discover those that are a must
                        have for somebody else. Because we feel that there will always be a lack of necessity in our
                        daily life.</p>
                </div>

                <div class="col-md-4">
                    <hr>
                    <h5 style="font-family: 'Avenir LT Std'">Here to help</h5>
                    <p style="text-align: justify; font-family: 'Avenir LT Std'">We are proud to have extensive
                        knowledge and experience at building great companies. Therefore, we want to spend our time
                        assisting others when facing common problems such as go-to market strategy, hiring, refining
                        business model, structure fundraising, technology development, product design, and strategic
                        partnerships.</p>
                </div>

                <div class="col-md-4">
                    <hr>
                    <h5 style="font-family: 'Avenir LT Std'">Not only capital</h5>
                    <p style="text-align: justify; font-family: 'Avenir LT Std'">We might NOT be the right partner if
                        you only seek capital. Yet, we highly value those who want to become a far better entrepreneur,
                        decision maker, strategic thinker, as well as scale their company. Our investments only show
                        that we are committed to assist you throughout your entrepreneurial journey.</p>
                </div>

                <div class="col-md-4">
                    <hr>
                    <h5 style="font-family: 'Avenir LT Std'">With open arms</h5>
                    <p style="text-align: justify; font-family: 'Avenir LT Std'">As ingrained within our core values, we
                        will welcome everyone with open arms. Whether you are someone that is just starting out, need
                        assistance for growth, and looking to partner with us or with our companies within our
                        ecosystem, please do not hesitate to contact us as it is never too early nor too late.</p>
                </div>

            </div>
        </div>
    </section>


    <section>
        <div class="container" data-aos="fade-up">
            <h4 style="font-family: 'Avenir LT Std'">Overview and preferences</h4>
            <hr>

            <div class="row" data-aos="fade-up" data-aos-delay="100">
                <div class="col-md-6">
                    <p style="font-family: 'Avenir LT Std'">Your startup is in a Pre-seed or Seed-stage who wants to get
                        a combination of capital and operational support from Tunnelerate partnership & networks.
                        <br> <br> The program will run its first batch from May 16th to July 22nd 2022 with the whole
                        sessions to be held virtually. <br><br>
                        We are accepting applications from January 24th until April 15th 2022, with the final acceptance
                        offers to be released to all founders on April 22nd.
                    </p>
                </div>

                <div class="col-md-6">
                    <p style="font-family: 'Avenir LT Std'">Market-driven & revenue generating startups that is already
                        incorporated and build products
                        <br> <br> The Tunnelerate Demo Day will be located offline in Jakarta where all the founders
                        will pitch toward selected judges and participating Institutional Investors. <br><br>
                        All startups that are accepted will be invested around up to $100,000. We will be accepting up
                        to 8 startups per batch
                    </p>
                </div>

            </div>
            <br><br>
            <div class="card">
                <div class="card-body" style="background-color: #3476b0; margin-top: -50px; border-radius: 5px;">
                    <br>
                    <div class="row" data-aos="fade-up" data-aos-delay="100">
                        <div class="col-md-6">
                            <div class="div-block-4">

                                <h4 style="font-family: 'Avenir LT Std'; color: white"> Join our first cohort <br> if
                                    you feel that your startup <br> suits our preferences</h4>
                                <img class="col-acc-arrow" src="{{'assets/tunnel_terbaru/arrow.svg'}}" style="">
                            </div>
                            <br><br><br>
                            <div class="div-block-4">
                                <a href="{{ route('apply') }}" class="apply-btn text-white">Apply Now <i
                                        class="bi bi-arrow-right" style="color: #fff;"></i></a>
                                <img class="col-acc-blink" src="{{'assets/tunnel_terbaru/blink.svg'}}">
                            </div>
                        </div>
                        <div class="col-md-6" style="padding: 20px 20px;">
                            <img src="{{asset('assets/tunnel_terbaru/assets/img/blog/blog-6.jpg')}}"
                                style="border-radius: 5px;" alt="" width="100%">
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </section>
</main>

{{--
<!-- breadcrumb area start -->--}}
{{-- @includeIf('website.breadcrumb', ['title' =>'Accelerator'])--}}
{{--
<!-- breadcrumb area end -->--}}
{{-- <style>
    --
    }
    }

        {
            {
            -- @if( !empty($accleratorss->dab_brand_image))--
        }
    }

        {
            {
            -- .about-img-area:before {
                content: url({{ asset( $accleratorss->dab_brand_image )
            }
        }

        );
    }

    --
    }
    }

        {
            {
            -- @endif;
            --
        }
    }

        {
            {
            --
</style>--}}
{{-- <div id="merox-about-area" class="merox-about-area mt-100 mb-70">--}}

    {{-- <div class="container">--}}
        {{-- <div class="row">--}}
            {{-- <div class="col-xl-5 col-lg-6 col-md-12 wow fadeInLeft">--}}
                {{-- <div class="about-img-area">--}}
                    {{-- @if(!empty($accleratorss->dab_big_image))--}}
                    {{-- <img src="{{ asset($accleratorss->dab_big_image) }}" alt="about" />--}}
                    {{-- <br>--}}
                    {{-- <br>--}}
                    {{-- <br>--}}
                    {{-- <img src="{{ asset($accleratorss->dab_big_imagee) }}" alt="about" />--}}
                    {{-- @endif--}}
                    {{-- </div>--}}
                {{-- </div>--}}
            {{-- <div class="col-xl-6 col-lg-6 col-md-12 offset-xl-1 wow fadeInRight">--}}
                {{-- <div class="section-title">--}}

                    {{-- @if(!empty($accleratorss->dab_sec_title))--}}
                    {{-- <h4>{{ $accleratorss->dab_sec_title }}</h4>--}}
                    {{-- @endif--}}

                    {{-- @if(!empty($accleratorss->dab_sec_title_2))--}}
                    {{-- <h3>{!! $accleratorss->dab_sec_title_2 !!}</h3>--}}
                    {{-- @endif--}}
                    {{-- </div>--}}
                {{-- <div class="about-content">--}}
                    {{-- @if(!empty($accleratorss->dab_srt_paragraph))--}}
                    {{-- <p>{{ $accleratorss->dab_srt_paragraph }}</p>--}}
                    {{-- @endif--}}

                    {{-- <ul class="about-content-list">--}}

                        {{-- @if(!empty($accleratorss->dab_list_info))--}}
                        {{-- {!! $accleratorss->dab_list_info !!}--}}
                        {{-- @endif--}}
                        {{-- </ul>--}}
                    {{-- <p>--}}
                        {{-- @if(!empty($accleratorss->dab_long_paragraph))--}}
                        {{-- {{ $accleratorss->dab_long_paragraph }}--}}
                        {{-- @endif--}}
                        {{-- </p>--}}
                    {{-- </div>--}}
                {{-- </div>--}}
            {{-- </div>
        <div class="row">--}}
            {{-- <div class="col-xl-12 col-lg-12 col-md-12 wow fadeInLeft">--}}
                {{-- <div class="about-img-area">--}}
                    {{-- @if(!empty($accleratorss->dab_big_imageee))--}}
                    {{-- <img src="{{ asset($accleratorss->dab_big_imageee) }}" alt="about" />--}}
                    {{-- <br>--}}
                    {{-- <br>--}}
                    {{-- <br>--}}
                    {{-- @endif--}}
                    {{-- </div>--}}
                {{-- </div>--}}
            {{-- </div>--}}
        {{--
    </div>--}}


    {{-- </div>--}}
<!-- merox contact area start -->
{{-- @includeIf('website.acc')--}}
<!-- merox contact area end -->
<!-- merox footer area start -->
@includeIf('website.footer')
<!-- merox footer area end -->
@endsection
