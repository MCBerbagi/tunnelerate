@extends('website.master')
@section('website-content')
<section id="hero" class="d-flex align-items-center">

    <div class="container" data-aos="zoom-out" data-aos-delay="100">
        <div class="row">
            <div class="col-xl-8">
                <div class="div-block-4">
                    <h1 >Accelerating Your<br>
                    Entrepreneurial<br>
                    Journey</h1>
                    <img class="col-arrow-up" src="{{'assets/tunnel_terbaru/arrow.svg'}}">
                </div>
                <div class="div-block-4">
                    <a href="{{ route('apply') }}" class="btn-get-started scrollto"
                        style="background: darkorange; border-color: #fff;font-family: 'Avenir LT Std'">Join Our
                        Program</a>
                    <img class="col-blink-up" src="{{'assets/tunnel_terbaru/blink.svg'}}">

                </div>
                <!--<i class="bi bi-arrow-right" style="color: white;"></i></a>-->
            </div>
        </div>
    </div>

</section><!-- End Hero -->
<!-- ======= How we eork Section ======= -->
<section id="how" class="how-we">
    <div class="container">
        <h1 style="color: #05355f">How We Work</h1>
        <hr>
        <div class="row">
            <div class="col-md-4 col-justify">
                <div class="col-wework">

                    <img class="mt-20 " src="{{asset('assets/tunnel_terbaru/assets/img/how/icon-grouwn.png')}}"
                    alt="" width="25%">
                    <h4 class="mt-10 mb-10">Helping you grow</h4>
                    <h6 class="col-text-color">As founders, we know how hard it is to build a company from
                        scratch. We want to help you realize and grow your business from ground-up during in our 10
                        weeks long program without any program fees.</h6>
                    <br>
                    <a href="{{ route('about') }}" >Learn about us <i
                            class="bi bi-arrow-right"></i></a>
                </div>
            </div>
            <div class="col-md-4 col-justify">
                <img class="mt-20 " src="{{asset('assets/tunnel_terbaru/assets/img/how/expertise.png')}}"
                    alt="" width="25%">
                <h4 class="mt-10 mb-10">Industry Expertise</h4>
                <h6 class="col-text-color">We are a set of ex-founders that want to give back to the
                    community. Thus, we want to help other startup founders in anyway possible such as operation,
                    strategy, finance, legal, human capital, marketing, and so forth.</h6>
                <br>
                <a href="{{ route('about') }}" >Learn about us <i
                        class="bi bi-arrow-right"></i></a>
            </div>
            <div class="col-md-4 col-justify">
                <img class="mt-20 " src="{{asset('assets/tunnel_terbaru/assets/img/how/long-run.png')}}"
                    alt="" width="25%">
                <h4 class="mt-10 mb-10">Here for the long-run</h4>
                <h6 class="col-text-color">As part of our commitment, we invest up to $100,000. We will
                    continue to support you during and after the program as we want to be your go-to partner for the
                    long term.</h6>
                <br>
                <br>
                <a href="{{ route('about') }}" >Learn about us <i
                        class="bi bi-arrow-right"></i></a>
            </div>
        </div>
        <br><br>

        <div class="row">

            <h1 style="color: #05355f">Selected Companies Within Our Ecosystem</h1>
            <hr>

            <div class="col-md-6 mx-auto" >
                <div class="card card-body shadow col-companies">
                    <div class="text-center col-comp">
                        <img src="{{asset('assets/tunnel_terbaru/assets/img/clients/client-1.png')}}" alt=""
                            width="15%">
                    </div>
                    <h4  class="card-title col-comp-text">Jaring Pangan</h4><br>
                    <p class="col-justify card-text">Indonesian AgriTech startup that enables
                        farmers sell directly to consumers.</p>
                    <br>
                </div>
            </div>


            <div class="col-md-6 mx-auto">
                <div class="card card-body shadow col-companies">
                    <div class="text-center col-comp">
                        <img class="mt-20" src="{{asset('assets/tunnel_terbaru/assets/img/clients/client-2.png')}}"
                            alt="" width="30%">
                    </div>
                    <h4 class="card-title col-comp-text">Era Tani</h4><br>
                    <p class="col-justify card-text">Tech-based agriculture micro financing,
                        supply chain, and commerce solutions for paddy farmers.</p>
                </div>
            </div>

        </div>

    </div>
</section>
<!-- End Clients Section -->

@includeIf('website.communitypartner')
@includeIf('website.mediapartner')
@includeIf('website.vcpartner')
@includeIf('website.blog-grid')
<!-- ======= About Section ======= -->
<section>
    <div class="container">
        <div class="">
                <br>
                <div class="row">
                    @if(count($video) > 0)
                    @foreach( $video as $item )
                    <div class="col-md-7" style="">
                        <div class="video-box text-center position-relative" style="padding: 20px 20px;">
                            <img src="{{ asset( $item->dv_image ) }}" width="853"  height="600" alt="video-img">
                            <div class="videos-icon">
                                <a class="popup-video" href="{{ $item->dv_url }}"><i class="fa fa-play"></i></a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @endif
                    <div class="col-md-5" >
                        <div class="jof">
                            <h1 style="font-family: 'Avenir LT Std'; color: white">Join our first cohort </h1>
                            <h5 style="font-family: 'Avenir LT Std'; color: white"> Calling all courageous founders
                            </h5>
                            {{-- <img class="col-arrow-bottom" src="{{'assets/tunnel_terbaru/arrow.svg'}}"> --}}
                        </div>
                        {{-- <br><br><br> --}}
                        {{-- <div class="div-block-4">
                            <a href="{{ route('apply') }}" class="apply-btn text-white">Apply Now <i
                                    class="bi bi-arrow-right" style="color: #fff;"></i></a>
                            <img class="col-blink-bottom" src="{{'assets/tunnel_terbaru/blink.svg'}}">
                        </div> --}}
                    </div>


            </div>
        </div>
    </div>

</section><!-- End About Section -->

<section id="hop">
    <h1 style="color: #05355f;">Hear our Point of View to get valuable insights</h1>
    <hr>
    <iframe src="{{ route('hearourpoint') }}" frameborder="0" style="" height="400" width="100%"></iframe>
<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4 text-center"><a href="https://anchor.fm/tunnelerate/"  class="btn btn-outline-warning">See More Sights </a></div>
    <div class="col-md-4"></div>
</div>
</section>


<!-- End #main -->
{{--
<!-- merox slider area start -->--}}
{{--@includeIf('website.main-slider')--}}
{{--
<!-- merox slider area end -->--}}
{{--
<!-- merox about area start -->--}}
{{--@includeIf('website.about')--}}
{{--
<!-- merox about area end -->--}}
{{--
<!-- merox counter area start -->--}}

{{--
<!-- merox counter area end -->--}}
{{--
<!-- merox services area start -->--}}
{{--@includeIf('website.services')--}}
{{--
<!-- merox services area end -->--}}
{{--
<!-- merox team area start -->--}}
{{--@includeIf('website.testimonial')--}}
{{--@includeIf('website.team')--}}
{{--
<!-- merox team area end -->--}}
{{--
<!-- merox call to action area start -->--}}
{{--@includeIf('website.cta')--}}
{{--
<!-- merox call to action area end -->--}}
{{--
<!-- merox testimonial area start -->--}}

{{--
<!-- merox testimonial area end -->--}}
{{--
<!-- merox video area start -->--}}
{{--@includeIf('website.video')--}}
{{--
<!-- merox video area end -->--}}
{{--
<!-- merox blog area start -->--}}
{{--@includeIf('website.blog')--}}
{{--
<!-- merox blog area end -->--}}
{{--
<!-- merox subscribe area start -->--}}
{{--@includeIf('website.subscribe')--}}
{{--
<!-- merox subscribe area end -->--}}
{{--
<!-- merox client slider area start -->--}}
{{--@includeIf('website.client')--}}
{{--
<!-- merox client slider area end -->--}}
{{--
<!-- merox footer area start -->--}}

@includeIf('website.footer')
<!-- merox footer area end -->
@endsection

