@extends('website.master')
@section('website-content')
@includeIf('website.breadcrumb', ['title' =>'Search For: '.$search])
    <!-- merox blog area start -->
    <!-- merox blog area start -->
    <div id="merox-blog-area" class="merox-blog-area mt-100 mb-100">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8 pr-30">

                    @if($search_blog->isNotEmpty())
                        @foreach($search_blog as $item)
                            <!-- single blog post -->
                            <div class="col-xl-12">
                                <div class="single-blog-wraper mb-30  wow fadeInUp">
                                    <div class="blog-img"><img src="{{ asset( $item->dblog_image ) }}"
                                            alt="{{ $item->dblog_title }}" /></div>
                                    <div class="single-blog-content">
                                        <div class="blog-date">
                                            {{ Carbon\Carbon::parse($item->created_at)->Format('d M, Y') }}
                                        </div>
                                        <h4><a href="{{ url('/') }}/blogdetails/{{ $item->id }}/{{ Str::slug($item->dblog_title) }}"
                                                class="post-title">{{ $item->dblog_title }}</a></h4>
                                        <p>{{ $item->dblog_paragraph }}</p>
                                    </div>
                                </div>
                            </div>
                            <!-- single blog post -->
                        @endforeach
                    @else

                        <div>
                            <h2>{{ __('messages.No posts found') }}</h2>
                        </div>

                    @endif


                </div>
                @includeIf('website.sidebar')
            </div>
        </div>
    </div>
    <!-- merox blog area end -->

    <!-- merox blog area end -->
    <!-- merox footer area start -->
    @includeIf('website.footer')
        <!-- merox footer area end -->
        @endsection
