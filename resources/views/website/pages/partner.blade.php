@extends('website.master')
@section('website-content')
<br>
<main id="main">
    <!-- Get section -->
    <section id="get" class="d-flex">
        <div class="container" data-aos="fade-up">
            <div class="row" data-aos="fade-up" data-aos-delay="100">
                <div class="col-md-6">
                    @includeIf('website.messages')
                    <h4 style="font-family: 'Avenir LT Std'">Partner with us</h4>
                    <hr>

                </div>
                <div class="col-md-6">
                    <p style="font-family: 'Avenir LT Std'">Invest early on revenue generating industries such as
                        edutech, agritech, ecommerce, gaming and many more. Be a part of early stage startups and see
                        their journey grow. Invest small but see a big impact. Fund promising startups during demo days
                        or even pre program.
                        <br><br>
                        The Tunnelerate Accelerator Program invites industry leaders and promising startups to
                        collaborate and share experiences. Use our sources to gain insights into emerging markets. Join
                        our closed networking groups and build a strong relationships with our community of founders.
                        <br><br>
                        Mentors are a crucial part of our ecosystem. Share your experiences, advice, guidance and wisdom
                        to founders, they shape the company and create lasting impact and relationships.
                        <br><br>
                        Take this opportunity to partner with us. At Tunnelerate our vision is to help local founders to
                        be leaders in their industry. With your help and our experienced founders we aim to give our
                        local founders a platform to grow and succeed with their companies.
                    </p>
                </div>
                <div class="col-md-2">
                </div>
                <div class="col-md-8">
                    <img src="{{asset('assets/tunnel_terbaru/assets/img/blog/blog-recent-4.jpg')}}" alt="" width="100%">
                </div>
                <div class="col-md-2">
                </div>
            </div>
        </div>
    </section>
    <!-- end Get Section -->
    @includeIf('website.speaker')
    @includeIf('website.mentor')
    @includeIf('website.communitypartner')
    <!-- section partners -->
    <section id="ways" class="ways">
        <div class="container" data-aos="fade-up">
            <h4 style="font-family: 'Avenir LT Std'">Ways we can collaborate together</h4>
            <hr>
            <div class="row" data-aos="fade-up" data-aos-delay="100">
                <div class="col-md-3" style="padding: 15px 15px;">
                    <div class="card cal-collab">
                        <img class="card-img-top" src="{{asset('assets/tunnel_terbaru/assets/img/tabs-1.jpg')}}"
                            width="250px" height="170px" alt="Card image cap">
                        <div class="card-body shadow">
                            <h5 class="card-title" style="font-family: 'Avenir LT Std'">Mentors and speakers</h5>
                            <p class="card-text" style="font-family: 'Avenir LT Std'">Connect and advice our founders.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-3" style="padding: 15px 15px;">
                    <div class="card cal-collab">
                        <img class="card-img-top" src="{{asset('assets/tunnel_terbaru/assets/img/tabs-2.jpg')}}"
                            width="250px" height="170px" alt="Card image cap">
                        <div class="card-body shadow">
                            <h4 class="card-title" style="font-family: 'Avenir LT Std'">Software tools</h4>
                            <p class="card-text" style="font-family: 'Avenir LT Std'">Providing working tools for our
                                portfolio startups.</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-3" style="padding: 15px 15px;">
                    <div class="card cal-collab">
                        <img class="card-img-top" src="{{asset('assets/tunnel_terbaru/assets/img/tabs-3.jpg')}}"
                            width="250px" height="170px" alt="Card image cap">
                        <div class="card-body shadow">
                            <h4 class="card-title" style="font-family: 'Avenir LT Std'">Media</h4>
                            <p class="card-text" style="font-family: 'Avenir LT Std'">Promote exciting up and coming
                                startups to reach bigger audience.</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-3" style="padding: 15px 15px;">
                    <div class="card cal-collab">
                        <img class="card-img-top" src="{{asset('assets/tunnel_terbaru/assets/img/tabs-4.jpg')}}"
                            width="250px" height="170px" alt="Card image cap">
                        <div class="card-body shadow">
                            <h4 class="card-title" style="font-family: 'Avenir LT Std'">Venture Capital</h4>
                            <p class="card-text" style="font-family: 'Avenir LT Std'">Get early access to high quality
                                startups for your deal flow.</p>
                        </div>
                    </div>
                </div>
                <div><br></div>

                <div class="col-md-3" style="padding: 15px 15px;">
                    <div class="card cal-collab">
                        <img class="card-img-top" src="{{asset('assets/tunnel_terbaru/assets/img/tabs-5.jpg')}}"
                            width="250" height="170" alt="Card image cap">
                        <div class="card-body shadow">
                            <h4 class="card-title" style="font-family: 'Avenir LT Std'">Education Institution</h4>
                            <p class="card-text" style="font-family: 'Avenir LT Std'">Help your students network and
                                prosper with their startups.</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-3" style="padding: 15px 15px;">
                    <div class="card cal-collab">
                        <img class="card-img-top" src="{{asset('assets/tunnel_terbaru/assets/img/tabs-6.jpg')}}"
                            width="250" height="170" alt="Card image cap">
                        <div class="card-body shadow">
                            <h4 class="card-title" style="font-family: 'Avenir LT Std'">Overseas partners</h4>
                            <p class="card-text" style="font-family: 'Avenir LT Std'">Assist and guide Indonesian
                                startups with</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-3" style="padding: 15px 15px;">
                    <div class="card cal-collab">
                        <img class="card-img-top" src="{{asset('assets/tunnel_terbaru/assets/img/tabs-7.jpg')}}"
                            alt="Card image cap">
                        <div class="card-body shadow">
                            <h4 class="card-title" style="font-family: 'Avenir LT Std'">Government entities</h4>
                            <p class="card-text" style="font-family: 'Avenir LT Std'">Gain access to local technology
                                companies.</p>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </section>


    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
        <div class="container" data-aos="fade-up">

            <div class="row" data-aos="fade-up" data-aos-delay="100">

                <div class="col-lg-6">

                    <div class="row">
                        <div class="col-md-12" style="padding-bottom: 40px;">

                            <div class="card-body" style="background-color: lightgrey;">
                                <h3 class="card-title" style="font-family: 'Avenir LT Std'">Be part of our <br>
                                    ecosystem.</h3>
                                <hr>
                                <p style="font-family: 'Avenir LT Std'">Gain access to the latest exciting companies in
                                    Indonesia and partner with them since day one.</p>
                            </div>


                        </div>

                    </div>

                </div>

                <div class="col-lg-6">

                    <form action="{{ route('frpartner.store') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="text" name="name" class="form-control" id="name" placeholder="Name" required>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" name="email" id="email" placeholder="Email"
                                required>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="job" id="subject" placeholder="Job Title"
                                required>
                        </div>

                        <div class="form-group">
                            <input type="text" class="form-control" name="company" id="subject"
                                placeholder="Company Name" required>
                        </div>
                        <div class="form-group">
                            <input type="number" class="form-control" name="num" id="subject"
                                placeholder="Mobile Number" required>
                        </div>
                        <div class="form-group">
                            <input type="url" class="form-control" name="company_webb" id="subject"
                                placeholder="Company Website" required>
                        </div>
                        <div class="form-group">
                            <input type="url" class="form-control" name="linkedin" id="subject"
                                placeholder="Your Linkedin url" required>
                        </div>

                        <div class="form-group">
                            <select name="type" id="" class="form-control">
                                <option value="">Company type</option>
                                <option value="Education">Education</option>
                                <option value="Corporation">Corporation</option>
                                <option value="Government">Government</option>
                                <option value="Investment Firm">Investment Firm</option>
                                <option value="Self Employed">Self Employed</option>
                                <option value="Entreprenuer/Startup">Entreprenuer/Startup</option>
                                <option value="Other">Other</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <select name="compoin" id="" class="form-control">
                                <option value="">Company operating in</option>
                                <option value="Indonesia">Indonesia</option>
                                <option value="Singapore">Singapore</option>
                                <option value="Malaysia">Malaysia</option>
                                <option value="The Philippines">The Philippines</option>
                                <option value="Thailand">Thailand</option>
                                <option value="Vietnam">Vietnam</option>
                                <option value="Brunei">Brunei</option>
                                <option value="Myanmar">Myanmar</option>
                                <option value="Cambodia">Cambodia</option>
                                <option value="Laos">Laos</option>
                                <option value="Timor-Leste">Timor-Leste</option>
                                <option value="Outside SEA"> Outside SEA</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <select name="typepart" id="" class="form-control">
                                <option value="">Type of partnership</option>
                                <option value="Mentorship and/or Speaker">Mentorship and/or Speaker</option>
                                <option value="Service Provider (Such As - SaaS, Cloud Services, Logistics Services)">
                                    Service Provider (Such As - SaaS, Cloud Services, Logistics Services)</option>
                                <option value="Venture Capital (VC) Partner">Venture Capital (VC) Partner</option>
                                <option value="Tunnelerate & Government Entity's Partnership">Tunnelerate & Government
                                    Entity's Partnership</option>
                                <option value="Educational Institution">Educational Institution</option>
                                <option value="Media or Other Related Company">Media or Other Related Company</option>
                                <option value="Overseas Partner">Overseas Partner</option>
                                <option value="Other">Other</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <select name="hyii" id="" class="form-control">
                                <option value="">Have you invested in venture?</option>
                                <option value="Yes - as a LP">Yes - as a LP</option>
                                <option value="Yes - as a Angel">Yes - as a Angel</option>
                                <option value="Yes - Other">Yes - Other</option>
                                <option value="No">No</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <select name="hyii" id="" class="form-control">
                                <option value="">Have you mentored any startup before?</option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="hdyh" id="subject"
                                placeholder="How did you hear about Tunnelerate?" required>
                        </div>

                        <div class="form-group">
                            <input type="text" class="form-control" name="hwcc" id="subject"
                                placeholder="How we can contact you? (please provide any reachable contact information)"
                                required>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="get-started-btn">Submit <i
                                    class="bi bi-arrow-right"></i></button>
                        </div>
                    </form>
                </div>

            </div>

        </div>
    </section><!-- End Contact Section -->



</main><!-- End #main -->
{{--
<!-- breadcrumb area start -->--}}
{{-- @includeIf('website.breadcrumb', ['title' =>'Partnership'])--}}
{{--
<!-- breadcrumb area end -->--}}
{{-- <style>
    --
    }
    }

        {
            {
            -- @if( !empty($partnerr->dab_brand_image))--
        }
    }

        {
            {
            -- .about-img-area:before {
                content: url({{ asset( $partnerr->dab_brand_image )
            }
        }

        );
    }

    --
    }
    }

        {
            {
            -- @endif;
            --
        }
    }

        {
            {
            --
</style>--}}
{{-- <div id="merox-about-area" class="merox-about-area mt-100 mb-70">--}}

    {{-- <div class="container">--}}
        {{-- <div class="row">--}}
            {{-- <div class="col-xl-5 col-lg-6 col-md-12 wow fadeInLeft">--}}
                {{-- <div class="about-img-area">--}}
                    {{-- @if(!empty($partnerr->dab_big_image))--}}
                    {{-- <img src="{{ asset($partnerr->dab_big_image) }}" alt="about" />--}}
                    {{-- <br>--}}
                    {{-- <br>--}}
                    {{-- <img src="{{ asset($partnerr->dab_big_imagee) }}" alt="about" />--}}
                    {{-- @endif--}}
                    {{-- </div>--}}
                {{-- </div>--}}
            {{-- <div class="col-xl-6 col-lg-6 col-md-12 offset-xl-1 wow fadeInRight">--}}
                {{-- <div class="section-title">--}}

                    {{-- @if(!empty($partnerr->dab_sec_title))--}}
                    {{-- <h4>{{ $partnerr->dab_sec_title }}</h4>--}}
                    {{-- @endif--}}

                    {{-- @if(!empty($partnerr->dab_sec_title_2))--}}
                    {{-- <h3>{!! $partnerr->dab_sec_title_2 !!}</h3>--}}
                    {{-- @endif--}}
                    {{-- </div>--}}
                {{-- <div class="about-content">--}}
                    {{-- @if(!empty($partnerr->dab_srt_paragraph))--}}
                    {{-- <p>{{ $partnerr->dab_srt_paragraph }}</p>--}}
                    {{-- @endif--}}

                    {{-- <ul class="about-content-list">--}}

                        {{-- @if(!empty($partnerr->dab_list_info))--}}
                        {{-- {!! $partnerr->dab_list_info !!}--}}
                        {{-- @endif--}}
                        {{-- </ul>--}}
                    {{-- <p>--}}
                        {{-- @if(!empty($partnerr->dab_long_paragraph))--}}
                        {{-- {{ $partnerr->dab_long_paragraph }}--}}
                        {{-- @endif--}}
                        {{-- </p>--}}
                    {{-- </div>--}}
                {{-- </div>--}}
            {{-- </div>--}}
        {{-- </div>--}}
    {{-- </div>--}}
<!-- merox contact area start -->
{{-- @includeIf('website.partner')--}}
<!-- merox contact area end -->
<!-- merox footer area start -->
@includeIf('website.footer')
<!-- merox footer area end -->
<!-- merox footer area end -->
@endsection
