@extends('website.master')
@section('website-content')
<!-- breadcrumb area start -->
    <!-- breadcrumb area end -->
    <!-- merox blog area start -->
    <div id="merox-blog-area" class="merox-blog-area mt-100 mb-100">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8 pr-30">
                    <div class="merox-blog-details-wraper">
                        <div class="blog-details-content">

                            <div class="blog-details-img">
                                <img src="{{ asset( $blog->dblog_image ) }}" alt="{{ $blog->dblog_title }}">
                            </div>
                            <div class="blog-date">
                                {{ Carbon\Carbon::parse($blog->created_at)->Format('d M, Y') }}
                            </div>
                            <h4>{{ $blog->dblog_title }}</h4>
                            {!! $blog->dblog_long_content !!}

                        </div>

                        <!-- tags & share area start -->
                        <div class="blog-post-tag">
                            @php
                                $single_post_tags = explode(',',$post_tags->dblog_tags);
                            @endphp

                            @if(!empty( $single_post_tags ))

                                @foreach($single_post_tags as $tag)

                                    @if(!empty( $tag ))

                                        <a href="{{ url('/') }}/blog-list">{{ $tag }}</a>
                                    @else

                                    {{ __('messages.No Tags Found') }}
                                    @endif
                                @endforeach

                            @endif

                        </div>
                        <!-- tags & share area end -->

                        <!-- recent comments area start -->
                        <div class="recent-comments-area mt-50">
                            <div class="post-heading">
                                <h3>{{ __('messages.recent comments') }}</h3>
                            </div>

                            <div id="disqus_thread"></div>
                            <script>
                                /**
                                 *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                                 *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables    */
                                /*
                                var disqus_config = function () {
                                this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
                                this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                                };
                                */
                                (function() { // DON'T EDIT BELOW THIS LINE
                                    var d = document, s = d.createElement('script');
                                    s.src = 'https://tunnelerate-com.disqus.com/embed.js';
                                    s.setAttribute('data-timestamp', +new Date());
                                    (d.head || d.body).appendChild(s);
                                })();
                            </script>
                        </div>
                        <!-- recent comments area end -->
                    </div>
                </div>

                <div class="col-xl-4 col-lg-4">
                    <!-- search wedget -->
                    <div class="single-sid-wdg sm-mt-30">
                        <form action="{{ route('blog.search') }}" method="GET"
                            class="wdg-search-form">
                            <input name="search" type="text" placeholder="{{ __('messages.type to search here...') }}">
                            <button class="submit-btn" type="submit"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                    <!-- latest post wedget -->
                    <div class="single-sid-wdg">
                        <div class="widget_recent_entries">
                            <h3>Latest post</h3>

                            @if(count($blog_relat_sidebar) > 0)

                                @foreach($blog_relat_sidebar as $blog_relat_item)
                                    <div class="single-wdg-post">
                                        <div class="wdg-post-img">
                                            <a
                                                href="{{ url('/') }}/blogdetails/{{ $blog_relat_item->id }}/{{ Str::slug($blog_relat_item->dblog_title) }}">
                                                <img src="{{ asset( $blog_relat_item->dblog_image) }}"
                                                    alt="{{ $blog_relat_item->dblog_title }}" class="img-fluid">
                                            </a>
                                        </div>
                                        <div class="wdg-post-content">

                                            <h5><a
                                                    href="{{ url('/') }}/blogdetails/{{ $blog_relat_item->id }}/{{ Str::slug($blog_relat_item->dblog_title) }}">{{ $blog_relat_item->dblog_title }}</a>
                                            </h5>
                                            <span>{{ Carbon\Carbon::parse($blog_relat_item->created_at)->Format('d M, Y') }}</span>
                                        </div>

                                    </div>
                                @endforeach

                            @else

                                <div class="single-wdg-post">
                                    <div class="wdg-post-img">
                                        <a href="#">
                                            <img src="{{ asset('assets/img/wdg/1.jpg') }}"
                                                alt="blog thumbnail" class="img-fluid">
                                        </a>
                                    </div>
                                    <div class="wdg-post-content">

                                        <h5><a href="#">{{ __('messages.How to build a company culture of success') }}</a></h5>
                                        <span>{{ __('messages.Sep 30, 2019') }}</span>
                                    </div>

                                </div>

                                <div class="single-wdg-post">
                                    <div class="wdg-post-img">
                                        <a href="#">
                                            <img src="{{ asset('assets/img/wdg/2.jpg') }}"
                                                alt="blog thumbnail" class="img-fluid">
                                        </a>
                                    </div>
                                    <div class="wdg-post-content">

                                        <h5><a href="#">{{ __('messages.Investment Update, Successful people ask') }}</a></h5>
                                        <span>{{ __('messages.Dec 11, 2019') }}</span>
                                    </div>

                                </div>

                                <div class="single-wdg-post">
                                    <div class="wdg-post-img">
                                        <a href="#">
                                            <img src="{{ asset('assets/img/wdg/3.jpg') }}"
                                                alt="blog thumbnail" class="img-fluid">
                                        </a>
                                    </div>
                                    <div class="wdg-post-content">

                                        <h5><a href="#">{{ __('messages.Investment Update, Successful people ask') }}</a></h5>
                                        <span>{{ __('messages.Jan 15, 2019') }}</span>
                                    </div>

                                </div>

                            @endif

                        </div>
                    </div>

                    <!-- tags wedget -->
                    <div class="single-sid-wdg">
                        <div class="widget_tag_cloud">
                            <h3>post tags</h3>
                            <div class="tagcloud">
                                @php
                                    $single_post_tags = explode(',',$post_tags->dblog_tags);
                                @endphp

                                @if(!empty( $single_post_tags ))

                                    @foreach($single_post_tags as $tag)

                                        @if(!empty( $tag ))

                                            <a href="{{ url('/') }}/blog-list">{{ $tag }}</a>
                                        @else

                                        {{ __('messages.No Tags Found') }}
                                        @endif
                                    @endforeach

                                @endif
                            </div>
                        </div>
                    </div>

                    <!-- tags wedget -->
                </div>
            </div>
        </div>
    </div>
    <!-- merox blog area end -->
    <!-- merox footer area start -->
    @includeIf('website.footer')
        <!-- merox footer area end -->
        @endsection
