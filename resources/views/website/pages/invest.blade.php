@extends('website.master')
@section('website-content')
<br>
<main id="main">
    <!-- Get section -->
    <section id="get" class="d-flex">
        <div class="container" data-aos="fade-up">
            <div class="row" data-aos="fade-up" data-aos-delay="100">
                <div class="col-md-6">
                    @includeIf('website.messages')
                    <h4 style="font-family: 'Avenir LT Std'">More than 17.500 islands, 700 languages,
                        300 ethics groups to be tapped</h4>
                    <hr>
                    <p style="font-family: 'Avenir LT Std'">We personally see what Indonesia could offer to the world
                        and with this enormous potential, we want to deal with it one step at a time. But we could not
                        do it alone, none of us can. At Tunnelerate, we have our own version of success and that is
                        building an inclusive Indonesian startup landscape, together. Therefore, we are looking to
                        partner with crazy ideas that may have a significant impact on society from day one.</p>

                </div>
                <div class="col-md-6">
                    <img src="{{asset('assets/tunnel_terbaru/assets/img/blog/blog-recent-2.jpg')}}" alt="" width="100%">
                </div>

            </div>
        </div>
    </section>
    <!-- end Get Section -->

    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
        <div class="container" data-aos="fade-up">

            <div class="row" data-aos="fade-up" data-aos-delay="100">

                <div class="col-lg-6">

                    <div class="row">
                        <div class="col-md-12" style="padding-bottom: 50px;">
                            <div class="card shadow">
                                <div class="card-body" style="background-color: lightgrey;">
                                    <h3 class="card-title" style="font-family: 'Avenir LT Std'">Invest with us</h3>
                                    <hr>
                                    <p style="font-family: 'Avenir LT Std'">We are excited to announce that we are still
                                        opening up for anyone that wants to take the step with us in order to bring
                                        inclusivity to Indonesia startup landscape and inviting individuals,
                                        institutions, and organizations to invest with us. As our fund is focused on
                                        investing in well-diversified Indonesian companies from the very beginning in
                                        order to bring immediate, sustainable, and long-lasting impact to the society.
                                    </p>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>

                <div class="col-lg-6">
                    <form action="{{ route('frinvest.store') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="text" name="name" class="form-control" id="name" placeholder="Name" required>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" name="email" id="email" placeholder="Email"
                                required>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="company" id="subject"
                                placeholder="Company Name" required>
                        </div>

                        <div class="form-group">
                            <input type="text" class="form-control" name="job" id="subject" placeholder="Job Title"
                                required>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="desc" id="subject"
                                placeholder="Company Description" required>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="linkedin" id="subject"
                                placeholder="Linkedin url" required>
                        </div>

                        <div class="form-group">
                            <select name="investor" id="" class="form-control">
                                <option value="">Investor type</option>
                                <option value="Personal Investment">Personal Investment</option>
                                <option value="Single Family Office">Single Family Office</option>
                                <option value="Multi Family Office">Multi Family Office</option>
                                <option value="Corporate (Not VC)">Corporate (Not VC)</option>
                                <option value="Corporate VC">Corporate VC</option>
                                <option value="VC Fund">VC Fund</option>
                                <option value="PE Fund">PE Fund</option>
                                <option value="Fund of Funds">Fund of Funds</option>
                                <option value="Development Bank">Development Bank</option>
                                <option value="Impact">Impact</option>
                                <option value="Institution">Institution</option>
                                <option value="Legal Counsel">Legal Counsel</option>
                                <option value="Other">Other</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <select name="hyii" id="" class="form-control">
                                <option value="">Have you invested in venture?</option>
                                <option value="Yes - as a LP">Yes - as a LP</option>
                                <option value="Yes - as a Angel">Yes - as a Angel</option>
                                <option value="Yes - Other">Yes - Other</option>
                                <option value="No">No</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <select name="target" id="" class="form-control">
                                <option value="">Target investment amount</option>
                                <option value="less than $50.000">less than $50.000</option>
                                <option value="$50.001 to $100.000">$50.001 to $100.000</option>
                                <option value="$100.001 to $250.000">$100.001 to $250.000</option>
                                <option value="$250.001 to $500.000">$250.001 to $500.000</option>
                                <option value="$500.001 to $1.000.000">$500.001 to $1.000.000</option>
                                <option value=" $1.000.001 to $3.000.000"> $1.000.001 to $3.000.000</option>
                                <option value="$3.000.001 to $5.000.000">$3.000.001 to $5.000.000</option>
                                <option value="$5.000.001 to $10.000.000">$5.000.001 to $10.000.000</option>
                                <option value="$10.000.001+">$10.000.001+</option>
                            </select>
                        </div>


                        <div class="form-group">
                            <select name="swiy" id="" class="form-control">
                                <option value="">Select what interest you</option>
                                <option value="LP commitment into Tunnelerate Fund">LP commitment into Tunnelerate Fund
                                </option>
                                <option value="Private debt">Private debt</option>
                                <option value="Direct Investments and Co-Investment">Direct Investments and
                                    Co-Investment</option>
                                <option value="Venture debt">Venture debt</option>
                                <option value="Secondaries">Secondaries</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <select name="hdyh" id="" class="form-control">
                                <option value="">How Did you hear about us</option>
                                <option value="Online News">Online News</option>
                                <option value="Media story on television or radio">Media story on television or radio
                                </option>
                                <option value="Linkedin">Linkedin</option>
                                <option value="Web Search">Web Search</option>
                                <option value="Referred by an existing LP of Tunnelerate">Referred by an existing LP of
                                    Tunnelerate</option>
                                <option value="Referred by an portofolio company of Tunnelerate">Referred by a
                                    portofolio company of Tunnelerate</option>
                                <option value="Referred by an existing LP of Tunnelerate">Referred by an existing
                                    partner of Tunnelerate</option>
                                <option value="Referred by start-up">Referred by start-up</option>
                            </select>
                        </div>


                        <div class="text-center">
                            <button type="submit" class="get-started-btn">Submit <i
                                    class="bi bi-arrow-right"></i></button>
                        </div>
                    </form>
                </div>

            </div>

        </div>
    </section><!-- End Contact Section -->


</main><!-- End #main -->
<!-- breadcrumb area start -->
{{-- @includeIf('website.breadcrumb', ['title' =>'Investment'])--}}
{{--
<!-- breadcrumb area end -->--}}
{{-- <style>
    --
    }
    }

        {
            {
            -- @if( !empty($investt->dab_brand_image))--
        }
    }

        {
            {
            -- .about-img-area:before {
                content: url({{ asset( $investt->dab_brand_image )
            }
        }

        );
    }

    --
    }
    }

        {
            {
            -- @endif;
            --
        }
    }

        {
            {
            --
</style>--}}
{{-- <div id="merox-about-area" class="merox-about-area mt-100 mb-70">--}}

    {{-- <div class="container">--}}
        {{-- <div class="row">--}}
            {{-- <div class="col-xl-5 col-lg-6 col-md-12 wow fadeInLeft">--}}
                {{-- <div class="about-img-area">--}}
                    {{-- @if(!empty($investt->dab_big_image))--}}
                    {{-- <img src="{{ asset($investt->dab_big_image) }}" alt="about" />--}}
                    {{-- @endif--}}
                    {{-- </div>--}}
                {{-- </div>--}}
            {{-- <div class="col-xl-6 col-lg-6 col-md-12 offset-xl-1 wow fadeInRight">--}}
                {{-- <div class="section-title">--}}

                    {{-- @if(!empty($investt->dab_sec_title))--}}
                    {{-- <h4>{{ $investt->dab_sec_title }}</h4>--}}
                    {{-- @endif--}}

                    {{-- @if(!empty($investt->dab_sec_title_2))--}}
                    {{-- <h3>{!! $investt->dab_sec_title_2 !!}</h3>--}}
                    {{-- @endif--}}
                    {{-- </div>--}}
                {{-- <div class="about-content">--}}
                    {{-- @if(!empty($investt->dab_srt_paragraph))--}}
                    {{-- <p>{{ $investt->dab_srt_paragraph }}</p>--}}
                    {{-- @endif--}}

                    {{-- <ul class="about-content-list">--}}

                        {{-- @if(!empty($investt->dab_list_info))--}}
                        {{-- {!! $investt->dab_list_info !!}--}}
                        {{-- @endif--}}
                        {{-- </ul>--}}
                    {{-- <p>--}}
                        {{-- @if(!empty($investt->dab_long_paragraph))--}}
                        {{-- {{ $investt->dab_long_paragraph }}--}}
                        {{-- @endif--}}
                        {{-- </p>--}}
                    {{-- </div>--}}
                {{-- </div>--}}
            {{-- </div>--}}
        {{-- </div>--}}


    {{-- </div>--}}
<!-- merox contact area start -->
{{-- @includeIf('website.invest')--}}
<!-- merox contact area end -->
<!-- merox footer area start -->
@includeIf('website.footer')
<!-- merox footer area end -->
@endsection
