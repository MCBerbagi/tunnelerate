@extends('website.master')
@section('website-content')
<!-- breadcrumb area start -->
@includeIf('website.breadcrumb', ['title' => 'messages.blog grid'])
<!-- breadcrumb area end -->
<!-- merox blog-list area start -->
@includeIf('website.blog-grid')
<!-- merox blog-list area end -->
<!-- merox footer area start -->
@includeIf('website.footer')
<!-- merox footer area end -->
@endsection

