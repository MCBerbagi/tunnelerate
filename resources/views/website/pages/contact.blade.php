@extends('website.master')
@section('website-content')
<!-- breadcrumb area start -->
@includeIf('website.breadcrumb', ['title' =>'messages.contact'])
<!-- breadcrumb area end -->
<!-- merox contact area start -->
@includeIf('website.contact')
<!-- merox contact area end -->
<!-- merox footer area start -->
@includeIf('website.footer')
<!-- merox footer area end -->
@endsection

