@extends('website.master')
@section('website-content')
<!-- breadcrumb area start -->
@includeIf('website.breadcrumb', ['title' =>'messages.testimonial'])
<!-- breadcrumb area end -->
<!-- merox testimonial area start -->
@includeIf('website.testimonial')
<!-- merox testimonial area end -->
<!-- merox footer area start -->
@includeIf('website.footer')
<!-- merox footer area end -->
@endsection

