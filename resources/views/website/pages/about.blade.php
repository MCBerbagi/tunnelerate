@extends('website.master')
@section('website-content')
<br>
<main id="main">
    <!-- Get section -->
    <section id="get" class="d-flex">
        <div class="container" data-aos="fade-up">
            <div class="row" data-aos="fade-up" data-aos-delay="100">
                <div class="col-md-6">
                    <h4 style="font-family: 'Avenir LT Std'">Get to know us more</h4>
                    <hr>
                    <p style="font-family: 'Avenir LT Std'">As founders, we know how hard it is to build a company from
                        scratch. We want to help you realize and grow your business from ground-up during our 10 weeks
                        long program without any program fees.</p>
                </div>
                <div class="col-md-6">
                    <img src="{{ asset('assets/tunnel_terbaru/assets/img/blog/blog-recent-1.jpg') }}" alt=""
                        width="100%">
                </div>
            </div>
        </div>
    </section>
    <!-- end Get Section -->

    <!-- section partners -->
    <section id="partners">
        <div class="container" data-aos="fade-up">
            <div class="row row-partners" data-aos="fade-up" data-aos-delay="100">
                <div class="col-md-4" style="padding-bottom: 10px;">
                    <h4 style="font-family: 'Avenir LT Std'">Partners</h4>
                    <hr>
                    <a style="font-family: 'Avenir LT Std'" href="">Fireside call with ivan <i class="bi bi-arrow-right"
                            style="color: orange;"></i></a>
                </div>

                <div class="col-md-4" style="padding-bottom: 50px;">
                    <a href="https://id.linkedin.com/in/ivanariesustiawan">
                        <div class="card">
                            <div class="card-body shadow col-our-partners">
                                <div class="text-center">
                                    <img src="{{ asset('assets/tunnel_terbaru/assets/img/team/profile-pak-ivan-1642401856.png') }}"
                                        alt="" width="200px">
                                </div>
                                <h4 class="card-title" style="font-family: 'Avenir LT Std'">Ivan Arie Sustiawan
                                </h4>
                                <p class="card-text" style="font-family: 'Avenir LT Std'">Ivan is a Founding &
                                    Managing Partner at Tunnelerate. Formerly CEO of TaniHub Group, Ivan has 20+ years
                                    in retail, logistics, e-commerce, and financial industry. As a startup enthusiast
                                    himself, Ivan has been actively angel investing and act as an Advisor in several
                                    early stage startups.</p>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-md-4" style="padding-bottom: 50px;">
                    <a href="https://id.linkedin.com/in/ayundaafifa">
                        <div class="card">
                            <div class="card-body shadow col-our-partners">
                                <div class="text-center">
                                    <img src="{{ asset('assets/tunnel_terbaru/assets/img/team/profile-mba-afi-1642401928.png') }}"
                                        alt="" width="200px">
                                </div>
                                <h4 style="font-family: 'Avenir LT Std'" class="card-title">Ayunda Afifa</h4>
                                <p style="font-family: 'Avenir LT Std'" class="card-text" style="font-size: 18px">
                                    Afi is a Partner at Tunnelerate. Having worked together with several startup
                                    founders from 2014 and combined with her 11+ years of experience in building &
                                    managing partnerships across countries & entities, she is actively involved in
                                    the
                                    area of business development, market expansion, Public-Private Partnerships and
                                    championing Diversity & Inclusion (D&I).</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="row" data-aos="fade-up" data-aos-delay="100">
                <div class="col-md-4" style="padding-bottom: 30px;">
                </div>
                <div class="col-md-4" style="padding-bottom: 30px;">
                    <a href="https://www.linkedin.com/in/bharat-ongso-33a40a53/">
                        <div class="card">
                            <div class="card-body shadow col-our-partners">
                                <div class="text-center">
                                    <img src="{{ asset('assets/tunnel_terbaru/assets/img/team/profile-pak-bharat-1642403340.png') }}"
                                        alt="" width="200px">
                                </div>
                                <h4 class="card-title" style="font-family: 'Avenir LT Std'">Bharat Ongso</h4>
                                <p class="card-text" style="font-family: 'Avenir LT Std'">Bharat is a Founding
                                    Partner at Tunnelerate. With 20+ years in the business of software technology, his
                                    company now works with 1000+ IT Professionals across Indonesia. As a believer and
                                    supporter of tech advancement, he has vast experience in venture-building
                                    particularly in tech-focused startups.</p>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-md-4" style="padding-bottom: 30px;">
                    <a href="https://www.linkedin.com/in/rriswant/">
                        <div class="card">
                            <div class="card-body shadow col-our-partners">
                                <div class="text-center">
                                    <img src="{{ asset('assets/tunnel_terbaru/assets/img/team/profile-pak-riswanto-1642403490.png') }}"
                                        alt="" width="200px">
                                </div>
                                <h4 style="font-family: 'Avenir LT Std'" class="card-title">Riswanto</h4>
                                <p style="font-family: 'Avenir LT Std'" class="card-text">Riswanto is a
                                    Founding
                                    Partner at Tunnelerate with 10+ years of tech entrepreneurship journey in the
                                    area
                                    of mobile and IoT solutions. Co-Founder of Eateroo and TSM Technologies, he is a
                                    former Sillicon Valley engineer and regional product marketing leader at Intel.
                                    Has
                                    been investing and advising startups in Indonesia for the last few years.</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>


    <section>
        <div class="container" data-aos="fade-up">
            <div class="row" data-aos="fade-up" data-aos-delay="100">
                <h4 style="font-family: 'Avenir LT Std'">Team united by interest, driven by purpose</h4>
                <hr>
                <a href="https://www.linkedin.com/company/tunnelerate/jobs/" style="font-family: 'Avenir LT Std'">Join
                    our
                    team <i class="bi bi-arrow-right" style="color: orange;"></i></a>
                <br><br>
            </div>
            <div class="row" data-aos="fade-up" data-aos-delay="100">
                <div class="col-md-4" style="padding-bottom: 30px;">
                    <a href="https://www.linkedin.com/in/muhammadfaisalfahmi/">
                        <div class="card">
                            <div class="card-body shadow col-team-united">
                                <div class="text-center">
                                    <img src="{{ asset('assets/tunnel_terbaru/assets/img/team/profile-faisal-1642403963.png') }}"
                                        alt="" width="200px">
                                </div>
                                <h4 style="font-family: 'Avenir LT Std'" class="card-title">Faisal Fahmi</h4>
                                <p style="font-family: 'Avenir LT Std'" class="card-text">Faisal is an Investment
                                    Manager at Tunnelerate. Previously from Pegasus Tech Ventures, he has a deep
                                    interest in investing in emerging technology companies and getting to know the team
                                    of the startups. He has established and maintained good relationships with various
                                    investment firms in the region</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4" style="padding-bottom: 30px;">
                    <a href="https://www.linkedin.com/in/jatin-raisinghani-b82258147/">
                        <div class="card">
                            <div class="card-body shadow col-team-united">
                                <div class="text-center">
                                    <img src="{{ asset('assets/tunnel_terbaru/assets/img/team/profile-jatin-1642404097.png') }}"
                                        alt="" width="200px">
                                </div>
                                <h4 style="font-family: 'Avenir LT Std'" class="card-title">Jatin Raisinghani</h4>
                                <p style="font-family: 'Avenir LT Std'" class="card-text">Jatin is a Strategic
                                    Partnership Manager at Tunnelerate. Experienced Product Manager with interest in
                                    connecting the dots between business and technology, currently leading the team to
                                    expand Tunnelerate partnership network.</p>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-md-4" style="padding-bottom: 30px;">
                    <a href="https://www.linkedin.com/in/christle-hillary-1b62b6101/">
                        <div class="card">
                            <div class="card-body shadow col-team-united">
                                <div class="text-center">
                                    <img src="{{ asset('assets/tunnel_terbaru/assets/img/team/profile-christle-1642655873.png') }}"
                                        alt="" width="200px">
                                </div>
                                <h4 style="font-family: 'Avenir LT Std'" class="card-title">Christle Hillary</h4>
                                <p style="font-family: 'Avenir LT Std'" class="card-text">Christle is the Office
                                    Services Manager at Tunnelerate. She carries her experience in office management to
                                    support Tunnelerate overall operations to support fellow founders.</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="row" data-aos="fade-up" data-aos-delay="100">

                <div><br></div>
                <div class="col-md-4" style="padding-bottom: 50px;">
                    <a href="https://www.linkedin.com/in/afrianto-syahputra-931727111/">
                        <div class="card">
                            <div class="card-body shadow col-team-united">
                                <div class="text-center">
                                    <img src="{{ asset('assets/tunnel_terbaru/assets/img/team/profile-putra-1642405815.png') }}"
                                        alt="" width="200px">
                                </div>
                                <h4 style="font-family: 'Avenir LT Std'" class="card-title">Afriyanto Syahputra
                                </h4>
                                <p style="font-family: 'Avenir LT Std'" class="card-text">Putra is a Content
                                    Creative at Tunnelerate. He draws on his design and multimedia expertise to help
                                    Tunnelerate strategize on overall content management for founders and its growing
                                    network of stakeholders.</p>
                            </div>
                        </div>
                    </a>
                </div>


                <div class="col-md-4" style="padding-bottom: 50px;">
                    <a href="https://www.linkedin.com/in/mhrctrfrnz/">
                        <div class="card">
                            <div class="card-body shadow col-team-united">
                                <div class="text-center">
                                    <img src="{{ asset('assets/tunnel_terbaru/assets/img/team/profile-catur-1642405894.png') }}"
                                        alt="" width="200px">
                                </div>
                                <h4 style="font-family: 'Avenir LT Std'" class="card-title">Mahar Catur Ferniza
                                </h4>
                                <p style="font-family: 'Avenir LT Std'" class="card-text">Catur is a Full Stack
                                    Engineer at Tunnelerate. Brings in his tech background to the team, he is fully
                                    responsible for ensuring the overall management of Tunnelerate tech networks.</p>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-md-4">
                    <a href="https://www.linkedin.com/in/rkalbuadi/">
                        <div class="card">
                            <div class="card-body shadow col-team-united">
                                <div class="text-center">
                                    <img src="{{ asset('assets/tunnel_terbaru/assets/img/team/profile-rifan.png') }}"
                                        alt="" width="200px">
                                </div>
                                <h4 style="font-family: 'Avenir LT Std'" class="card-title">Rifan Kalbuadi</h4>
                                <p style="font-family: 'Avenir LT Std'" class="card-text">Rifan is a program and
                                    partnership associate at Tunnelerate. He is responsible to execute the overall
                                    program & partnership strategy, collaborate with partners and team members to design
                                    programs based on founders needs and partnership packages based on targeted
                                    industries/sectors, drive external approaches and make direct contact to the
                                    designated parties.</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>

        </div>

    </section>



</main><!-- End #main -->
@includeIf('website.footer')
@endsection
{{--
<!-- ======= Footer ======= --> --}}
{{-- <footer id="footer"> --}}

    {{-- <div class="footer-top"> --}}
        {{-- <div class="container"> --}}
            {{-- <div class="row"> --}}


                {{-- <div class="col-lg-4 col-md-6"> --}}
                    {{-- <h4>Stay up to date on all <br> the latest Tunnelerate <br> news</h4> --}}
                    {{-- <form action="" method="post"> --}}
                        {{-- <input type="text" name="nama" class="form-control" placeholder="Nama"><br> --}}
                        {{-- <input type="email" name="email" class="form-control" placeholder="Email"> --}}
                        {{-- </form> --}}
                    {{-- <br> --}}
                    {{-- <button type="submit" class="btn btn-light btn-sm">Subscribe Now <i class="bi bi-arrow-right"
                            style="color: orange;"></i></button> --}}
                    {{-- </div> --}}


                {{-- <div class="col-lg-2 col-md-6 footer-links"> --}}
                    {{-- <h4>Discover</h4> --}}
                    {{-- <ul> --}}
                        {{-- <li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li> --}}
                        {{-- <li><i class="bx bx-chevron-right"></i> <a href="#">Our Program</a></li> --}}
                        {{-- <li><i class="bx bx-chevron-right"></i> <a href="#">Partner with us</a></li> --}}
                        {{-- <li><i class="bx bx-chevron-right"></i> <a href="#">Investors</a></li> --}}
                        {{-- <li><i class="bx bx-chevron-right"></i> <a href="#">Apply</a></li> --}}
                        {{-- </ul> --}}
                    {{-- </div> --}}

                {{-- <div class="col-lg-3 col-md-6 footer-links"> --}}
                    {{-- <h4>Social Media</h4> --}}
                    {{-- <ul> --}}
                        {{-- <li><i class="bx bx-chevron-right"></i> <a href="#">Instagram</a></li> --}}
                        {{-- <li><i class="bx bx-chevron-right"></i> <a href="#">Linkedin</a></li> --}}
                        {{-- <li><i class="bx bx-chevron-right"></i> <a href="#">Spotify</a></li> --}}
                        {{-- <li><i class="bx bx-chevron-right"></i> <a href="#">Youtube</a></li> --}}
                        {{-- </ul> --}}
                    {{-- </div> --}}

                {{-- <div class="col-lg-3 col-md-6 footer-contact"> --}}
                    {{-- <h3><img src="assets/img/logo-tunnelerate.png" alt="" width="100px"></h3> --}}
                    {{-- <p> --}}
                        {{-- South Quarter Building Complex <br> --}}
                        {{-- Jalan RA Kartini Ksb 8, Cilandak, Jakarta.<br> --}}
                        {{-- Selatan <br> --}}
                        {{-- +6221 5098 2659<br> --}}
                        {{-- </p> --}}
                    {{-- </div> --}}

                {{-- </div> --}}
            {{-- </div> --}}
        {{-- </div> --}}

    {{-- <div class="container d-md-flex py-4"> --}}

        {{-- <div class="me-md-auto text-center text-md-start"> --}}
            {{-- <div class="copyright"> --}}
                {{-- &copy; Copyright <strong><span>Tunnelerate</span></strong>. All Rights Reserved --}}
                {{-- </div> --}}
            {{-- <div class="credits"> --}}
                {{--
                <!-- All the links in the footer should remain intact. --> --}}
                {{--
                <!-- You can delete the links only if you purchased the pro version. --> --}}
                {{--
                <!-- Licensing information: https://bootstrapmade.com/license/ --> --}}
                {{--
                <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/presento-bootstrap-corporate-template/ -->
                --}}

                {{--
            </div> --}}
            {{-- </div> --}}
        {{-- <div class="social-links text-center text-md-end pt-3 pt-md-0"> --}}
            {{-- <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a> --}}
            {{-- <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a> --}}
            {{-- <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a> --}}
            {{-- <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a> --}}
            {{-- <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a> --}}
            {{-- </div> --}}
        {{-- </div> --}}
    {{-- </footer><!-- End Footer --> --}}

{{-- <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
        class="bi bi-arrow-up-short"></i></a> --}}

{{--
<!-- Vendor JS Files --> --}}
{{-- <script src="{{asset('assets/tunnel_terbaru/assets/vendor/purecounter/purecounter.js') }}">
</script> --}}
{{-- <script src="{{asset('assets/tunnel_terbaru/assets/vendor/aos/aos.js') }}">
</script> --}}
{{-- <script src="{{asset('assets/tunnel_terbaru/assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}">
</script> --}}
{{-- <script src="{{asset('assets/tunnel_terbaru/assets/vendor/glightbox/js/glightbox.min.js') }}">
</script> --}}
{{-- <script src="{{asset('assets/tunnel_terbaru/assets/vendor/isotope-layout/isotope.pkgd.min.js') }}">
</script> --}}
{{-- <script src="{{asset('assets/tunnel_terbaru/assets/vendor/swiper/swiper-bundle.min.js') }}">
</script> --}}
{{-- <script src="{{asset('assets/tunnel_terbaru/assets/vendor/php-email-form/validate.js') }}">
</script> --}}

{{--
<!-- Template Main JS File --> --}}
{{-- <script src="{{asset('assets/tunnel_terbaru/assets/js/main.js') }}">
</script> --}}
