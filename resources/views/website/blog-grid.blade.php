<!-- merox blog area start -->
<div id="merox-blog-area" class="merox-blog-area mt-100">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                @if(count($teamcontent) > 0)
                    @foreach($teamcontent as $itemcontent)
                        <div class="mt-60 mb-60 text-center">
                            <h1>Tunnelerate Blog</h1>
                            <hr style="color:#fff;">
                        </div>
                    @endforeach
                @endif

            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">

        @if( count($blog) > 0)

            @foreach($blog as $item)
                <!-- single blog post -->
                    <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12">
                        <div class="single-blog-wraper mb-30  wow fadeInUp">
                            <div class="blog-img"><img style="border-radius: 5px" src="{{ asset( $item->dblog_image ) }}"
                                                       alt="{{ $item->dblog_title }}" /></div>
                            <div class="single-blog-content col-our-blog">
                                <div class="blog-date-n">
                                    {{ Carbon\Carbon::parse($item->created_at)->Format('d M, Y') }}
                                </div>
                                <h4><a href="{{ url('/') }}/blogdetails/{{ $item->id }}/{{ Str::slug($item->dblog_title) }}"
                                       class="post-title">{{ $item->dblog_title }}</a></h4>
                                <p style=" font-family:'Avenir LT Std';">{{ $item->dblog_paragraph }}</p>
                            </div>
                        </div>
                    </div>
                    <!-- single blog post -->
                @endforeach
            @endif
            <div class="col-xl-12">
                <div class="merox-pagination mt-20">
                    {!! $blog->links() !!}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- merox blog area end -->
