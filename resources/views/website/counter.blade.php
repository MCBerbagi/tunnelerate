<div id="merox-counter-area" class="merox-counter-area mb-70 wow fadeInUp">
    <div class="container">
        <div class="row">

            @if(count($counter) > 0)
                @foreach( $counter as $item )

                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                        <div class="about-single-counter text-center">
                            <img src="{{ asset( $item->dc_icon ) }}" alt="{{ $item->dc_title }}" />
                            <h2 class="counter">{{ $item->dc_number }}</h2>
                            <span>{{ $item->dc_title }}</span>
                        </div>
                    </div>

                @endforeach
            @endif
        </div>
    </div>
</div>
