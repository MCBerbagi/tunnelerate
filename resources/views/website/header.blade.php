<header id="header" class="fixed-top d-flex align-items-center">
    <div class="container d-flex align-items-center">
        <h1 class="logo me-auto"><a href="{{ route('index') }}">

                <img src="{{asset('assets/tunnel_terbaru/assets/img/Logo_Tunnelerate_white.png')}}" alt=""></a></h1>

        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html" class="logo me-auto"><img src="assets/img/logo.png" alt=""></a>-->

        <nav id="navbar" class="navbar order-last order-lg-0">
            <ul>
                <li><a class="nav-link scrollto active"
                        href="{{ route('index') }}">Home</a></li>
                <li><a class="nav-link scrollto"  href="{{ route('about') }}">About
                        Us</a></li>
                <li><a class="nav-link scrollto"
                        href="{{ route('team') }}">Support</a></li>
                <li><a class="nav-link scrollto"  href="{{ route('acc') }}">Our
                        Programs</a></li>
                <li><a class="nav-link scrollto"
                        href="{{ route('partner') }}">Partner With Us</a></li>
                <li><a class="nav-link scrollto"
                        href="{{ route('invest') }}">Investors</a></li>

            </ul>
            <i class="bi bi-list mobile-nav-toggle"></i>
        </nav><!-- .navbar -->

        <a href="{{ route('apply') }}"  class="get-started-btn scrollto">Apply
            Here</a>
    </div>
</header><!-- End Header -->
