<div id="merox-testimonial-area" class="merox-testimonial-area mt-100 mb-100 wow fadeInUp">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                @if(count($testimonialcontent) > 0)
                    @foreach($testimonialcontent as $itemcontent)
                        <div class="section-title">
                            <h4>{{ $itemcontent->dtmcon_sec_title }}</h4>
                            <h3>{{ $itemcontent->dtmcon_sec_title_2 }}</h3>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="testimonial-wraper  owl-carousel owl-theme ">

                    @if(count($testimonial) > 0)
                        @foreach( $testimonial as $item )

                            <div class="single-testimonial">
                                <div class="testimonial-img">
                                    <img src="{{ asset( $item->dts_image ) }}" class="img-fluid"
                                        alt="testimonial-img">
                                </div>
                                <div class="testimonial-content">
                                    <p>{{ $item->dts_paragraph }}</p>

                                    <div class="title-desig">
                                        <h3>{{ $item->dts_title }}</h3>
                                        <h4>{{ $item->dts_desig }}</h4>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
