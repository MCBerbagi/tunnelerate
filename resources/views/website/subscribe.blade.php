<div id="merox-subscribe-area" class="merox-subscribe-area pt-100 pb-100" style="background-image:
@if(!empty($subscribecontent->dsubscribeco_image))
	url({{ asset($subscribecontent->dsubscribeco_image) }})
@endif">
    <div class=" container">
    <div class="row">
        <div class="col-xl-5 col-lg-5 wow fadeInLeft">
            <div class="subscribe-title">

                @if(!empty($subscribecontent->dsubscribeco_title))
                    <h3>{!! $subscribecontent->dsubscribeco_title !!}</h3>

                @endif
            </div>
        </div>
        <div class="col-xl-6 col-lg-6 offset-xl-1  wow fadeInRight">
            <div class="subscribe-form-box">
                <form id="merox-subscribe-form" class="subscribe-form">
                    <input name="fs_email" type="email" placeholder="Type Your Email" id="subscribe-email">
                    <button type="submit" class="btn btn-type-2" id="sub-submit">
                        @if(!empty($subscribecontent->dsubscribeco_btn_title))
                            {{ $subscribecontent->dsubscribeco_btn_title }}
                        @endif
                        <i class="fa fa-long-arrow-right"></i>
                    </button>
                </form>
            </div>
            <p class="text-white form-message float-left mt-15" id="merox-email-error"></p>
            <p class="text-white form-message float-left mt-15" id="subscribe-success-message"> </p>
        </div>
    </div>
</div>
</div>
