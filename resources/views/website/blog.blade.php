<div id="merox-blog-area" class="merox-blog-area mt-100 mb-70">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                @if(count($blogcontent) > 0)
                    @foreach( $blogcontent as $itemcontent )

                        <div class="section-title">
                            <h4>{{ $itemcontent->dtmcon_sec_title }}</h4>
                            <h3>{{ $itemcontent->dtmcon_sec_title_2 }}</h3>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">

            @if(count($blog) > 0)
                @foreach( $blog as $item )
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                        <div class="single-blog-wraper mb-30  wow fadeInUp">
                            <div class="blog-img"><img src="{{ asset( $item->dblog_image ) }}"
                                    alt="{{ $item->dblog_title }}" /></div>
                            <div class="single-blog-content">
                                <div class="blog-date">
                                    {{ Carbon\Carbon::parse($item->created_at)->Format('d M, Y') }}
                                </div>
                                <h4><a href="{{ url('/') }}/blogdetails/{{ $item->id }}/{{ Str::slug($item->dblog_title) }}"
                                        class="post-title">{{ $item->dblog_title }}</a></h4>
                                <p>{{ $item->dblog_paragraph }}</p>
                            </div>
                        </div>
                    </div>
                @endforeach

            @endif
        </div>
    </div>
</div>
