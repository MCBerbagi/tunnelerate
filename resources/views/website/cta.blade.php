<div id="merox-cta-area" class="merox-cta-area pt-100 pb-100 text-center" style="background-image:
@if(!empty($cta->dcta_big_image))
	url({{ asset($cta->dcta_big_image) }})
@endif ">
    <div class="container">
        <div class="row">
            <div class="col-xl-10 mx-auto col-lg-10 col-md-12">

                <div class="cta-content">
                    @if(!empty($cta->dcta_title)) <p>{!! $cta->dcta_title !!}</p>@endif

                @if(!empty($cta->dcta_title_2)) <h2 class="wow fadeInUp">{{ $cta->dcta_title_2 }}</h2> @endif

                    <div class="cta-btn">

                        <a href="@if (!empty($cta->dcta_url)){{ $cta->dcta_url }}@else # @endif "
                            class="btn btn-type-5 wow fadeInUp" data-wow-delay=".4s">
                            @if(!empty($cta->dcta_btn))
                                {{ $cta->dcta_btn }}
                            @endif
                            <i class="fa fa-long-arrow-right"></i>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
