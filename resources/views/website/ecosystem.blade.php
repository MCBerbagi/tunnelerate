<div id="merox-client-slider" class="merox-client-slider pb-70">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                @if(count($teamcontent) > 0)
                    @foreach($teamcontent as $itemcontent)
                        <div class="section-title">
                            <h4>Tunnelerate</h4>
                            <h3>Our Partner</h3>
                        </div>
                    @endforeach
                @endif

            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xl-10 col-lg-10 col-md-12 mx-auto">
                <div class="all-client-slider owl-carousel owl-theme">
                    @if(count($Ecosystem) > 0)
                        @foreach( $Ecosystem as $item )
                            <!-- single client start -->
                            <a href="{{ $item->dcli_link }}">
                                <div class="single-client">
                                    <img src="{{ asset( $item->dcli_image ) }}" alt="client-image"
                                        class="img-fluid" style="width: 150px"/>
                                </div>
                            </a>
                            <!-- single client end -->
                        @endforeach

                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
