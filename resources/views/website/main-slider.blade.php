@if(count($mainSlider) > 0)
    <div id="merox-slider-area" class="merox-slider-area">
        <div class="merox-main-slider owl-carousel owl-theme">
            @foreach($mainSlider as $item)
                <div class="merox-single-slider" style="background-image: url({{ asset($item->dms_image) }})">
                    <div class="slid-bg-1"></div>
                    <div class="merox-single-table">
                        <div class="merox-single-tablecell">
                            <div class="container">
                                <div class="row">
                                    <div class="col-xl-8 col-lg-8 col-md-12">
                                        <div class="merox-slider-area-content">
                                            <h4>{{ $item->dms_title }}</h4>
                                            <h2>{!! $item->dms_title2 !!}</h2>
                                            <p>{!! $item->dms_paragraph !!}</p>
                                            <div class="slider-area-btn">
                                                <a href="{{ $item->dms_url1 }}"
                                                    class="btn btn-type-2">{{ $item->dms_btn1 }} <i
                                                        class="fa fa-long-arrow-right"></i> </a>
                                                <a href="{{ $item->dms_url2 }}"
                                                    class="btn btn-type-3">{{ $item->dms_btn2 }}</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endif
