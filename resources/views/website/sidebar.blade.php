<div class="col-xl-4 col-lg-4">
    <!-- search wedget -->
    <div class="single-sid-wdg sm-mt-30">
        <form action="{{ route('blog.search') }}" method="GET" class="wdg-search-form">
            <input name="search" type="text" placeholder="{{ __('messages.type to search here...') }}">
            <button class="submit-btn" type="submit"><i class="fa fa-search"></i></button>
        </form>
    </div>
    <!-- latest post wedget -->
    <div class="single-sid-wdg">
        <div class="widget_recent_entries">
            <h3>{{ __('messages.Latest post') }}</h3>

            @if(count($blog_relat_sidebar) > 0)

                @foreach($blog_relat_sidebar as $blog_relat_item)
                    <div class="single-wdg-post">
                        <div class="wdg-post-img">
                            <a
                                href="{{ url('/') }}/blogdetails/{{ $blog_relat_item->id }}/{{ Str::slug($blog_relat_item->dblog_title) }}">
                                <img src="{{ asset( $blog_relat_item->dblog_image) }}"
                                    alt="{{ $blog_relat_item->dblog_title }}" class="img-fluid">
                            </a>
                        </div>
                        <div class="wdg-post-content">

                            <h5><a
                                    href="{{ url('/') }}/blogdetails/{{ $blog_relat_item->id }}/{{ Str::slug($blog_relat_item->dblog_title) }}">{{ $blog_relat_item->dblog_title }}</a>
                            </h5>
                            <span>{{ Carbon\Carbon::parse($blog_relat_item->created_at)->Format('d M, Y') }}</span>
                        </div>

                    </div>
                @endforeach

            @endif

        </div>
    </div>

</div>
